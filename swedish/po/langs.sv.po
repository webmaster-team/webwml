msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2014-05-18 18:08+0100\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.4\n"

#: ../../english/template/debian/language_names.wml:8
msgid "Arabic"
msgstr "arabiska"

#: ../../english/template/debian/language_names.wml:9
msgid "Armenian"
msgstr "armeniska"

#: ../../english/template/debian/language_names.wml:10
msgid "Finnish"
msgstr "finska"

#: ../../english/template/debian/language_names.wml:11
msgid "Croatian"
msgstr "kroatiska"

#: ../../english/template/debian/language_names.wml:12
msgid "Danish"
msgstr "danska"

#: ../../english/template/debian/language_names.wml:13
msgid "Dutch"
msgstr "nederländska"

#: ../../english/template/debian/language_names.wml:14
msgid "English"
msgstr "engelska"

#: ../../english/template/debian/language_names.wml:15
msgid "French"
msgstr "franska"

#: ../../english/template/debian/language_names.wml:16
msgid "Galician"
msgstr "galiciska"

#: ../../english/template/debian/language_names.wml:17
msgid "German"
msgstr "tyska"

#: ../../english/template/debian/language_names.wml:18
msgid "Italian"
msgstr "italienska"

#: ../../english/template/debian/language_names.wml:19
msgid "Japanese"
msgstr "japanska"

#: ../../english/template/debian/language_names.wml:20
msgid "Korean"
msgstr "koreanska"

#: ../../english/template/debian/language_names.wml:21
msgid "Spanish"
msgstr "spanska"

#: ../../english/template/debian/language_names.wml:22
msgid "Portuguese"
msgstr "portugisiska"

#: ../../english/template/debian/language_names.wml:23
msgid "Portuguese (Brazilian)"
msgstr "portugisiska (Brasilien)"

#: ../../english/template/debian/language_names.wml:24
msgid "Chinese"
msgstr "kinesiska"

#: ../../english/template/debian/language_names.wml:25
msgid "Chinese (China)"
msgstr "kinesiska (Kina)"

#: ../../english/template/debian/language_names.wml:26
msgid "Chinese (Hong Kong)"
msgstr "kinesiska (Hongkong)"

#: ../../english/template/debian/language_names.wml:27
msgid "Chinese (Taiwan)"
msgstr "kinesiska (Taiwan)"

#: ../../english/template/debian/language_names.wml:28
msgid "Chinese (Traditional)"
msgstr "kinesiska (traditionell)"

#: ../../english/template/debian/language_names.wml:29
msgid "Chinese (Simplified)"
msgstr "kinesiska (förenklad)"

#: ../../english/template/debian/language_names.wml:30
msgid "Swedish"
msgstr "svenska"

#: ../../english/template/debian/language_names.wml:31
msgid "Polish"
msgstr "polska"

#: ../../english/template/debian/language_names.wml:32
msgid "Norwegian"
msgstr "norska"

#: ../../english/template/debian/language_names.wml:33
msgid "Turkish"
msgstr "turkiska"

#: ../../english/template/debian/language_names.wml:34
msgid "Russian"
msgstr "ryska"

#: ../../english/template/debian/language_names.wml:35
msgid "Czech"
msgstr "tjeckiska"

#: ../../english/template/debian/language_names.wml:36
msgid "Esperanto"
msgstr "esperanto"

#: ../../english/template/debian/language_names.wml:37
msgid "Hungarian"
msgstr "ungerska"

#: ../../english/template/debian/language_names.wml:38
msgid "Romanian"
msgstr "rumänska"

#: ../../english/template/debian/language_names.wml:39
msgid "Slovak"
msgstr "slovakiska"

#: ../../english/template/debian/language_names.wml:40
msgid "Greek"
msgstr "grekiska"

#: ../../english/template/debian/language_names.wml:41
msgid "Catalan"
msgstr "katalanska"

#: ../../english/template/debian/language_names.wml:42
msgid "Indonesian"
msgstr "indonesiska"

#: ../../english/template/debian/language_names.wml:43
msgid "Lithuanian"
msgstr "litauiska"

#: ../../english/template/debian/language_names.wml:44
msgid "Slovene"
msgstr "slovenska"

#: ../../english/template/debian/language_names.wml:45
msgid "Bulgarian"
msgstr "bulgariska"

#: ../../english/template/debian/language_names.wml:46
msgid "Tamil"
msgstr "tamil"

#. for now, the following are only needed if you intend to translate intl/l10n
#: ../../english/template/debian/language_names.wml:48
msgid "Afrikaans"
msgstr "afrikaans"

#: ../../english/template/debian/language_names.wml:49
msgid "Albanian"
msgstr "albanska"

#: ../../english/template/debian/language_names.wml:50
msgid "Asturian"
msgstr "asturiska"

#: ../../english/template/debian/language_names.wml:51
msgid "Amharic"
msgstr "amhariska"

#: ../../english/template/debian/language_names.wml:52
msgid "Azerbaijani"
msgstr "azerbajdzjanska"

#: ../../english/template/debian/language_names.wml:53
msgid "Basque"
msgstr "baskiska"

#: ../../english/template/debian/language_names.wml:54
msgid "Belarusian"
msgstr "vitryska"

#: ../../english/template/debian/language_names.wml:55
msgid "Bengali"
msgstr "bengali"

#: ../../english/template/debian/language_names.wml:56
msgid "Bosnian"
msgstr "bosniska"

#: ../../english/template/debian/language_names.wml:57
msgid "Breton"
msgstr "bretonska"

#: ../../english/template/debian/language_names.wml:58
msgid "Cornish"
msgstr "corniska"

#: ../../english/template/debian/language_names.wml:59
msgid "Estonian"
msgstr "estniska"

#: ../../english/template/debian/language_names.wml:60
msgid "Faeroese"
msgstr "färöiska"

#: ../../english/template/debian/language_names.wml:61
msgid "Gaelic (Scots)"
msgstr "skotska"

#: ../../english/template/debian/language_names.wml:62
msgid "Georgian"
msgstr "georgiska"

#: ../../english/template/debian/language_names.wml:63
msgid "Hebrew"
msgstr "hebreiska"

#: ../../english/template/debian/language_names.wml:64
msgid "Hindi"
msgstr "hindi"

#: ../../english/template/debian/language_names.wml:65
msgid "Icelandic"
msgstr "isländska"

#: ../../english/template/debian/language_names.wml:66
msgid "Interlingua"
msgstr "interlingua"

#: ../../english/template/debian/language_names.wml:67
msgid "Irish"
msgstr "irländska"

#: ../../english/template/debian/language_names.wml:68
msgid "Kalaallisut"
msgstr "grönländska"

#: ../../english/template/debian/language_names.wml:69
msgid "Kannada"
msgstr "kanaresiska"

#: ../../english/template/debian/language_names.wml:70
msgid "Kurdish"
msgstr "kurdiska"

#: ../../english/template/debian/language_names.wml:71
msgid "Latvian"
msgstr "lettiska"

#: ../../english/template/debian/language_names.wml:72
msgid "Macedonian"
msgstr "makedoniska"

#: ../../english/template/debian/language_names.wml:73
msgid "Malay"
msgstr "malajiska"

#: ../../english/template/debian/language_names.wml:74
msgid "Malayalam"
msgstr "malayalam"

#: ../../english/template/debian/language_names.wml:75
msgid "Maltese"
msgstr "maltesiska"

#: ../../english/template/debian/language_names.wml:76
msgid "Manx"
msgstr "manx"

#: ../../english/template/debian/language_names.wml:77
msgid "Maori"
msgstr "maori"

#: ../../english/template/debian/language_names.wml:78
msgid "Mongolian"
msgstr "mongoliska"

#: ../../english/template/debian/language_names.wml:80
msgid "Norwegian Bokm&aring;l"
msgstr "bokmål (norska)"

#: ../../english/template/debian/language_names.wml:82
msgid "Norwegian Nynorsk"
msgstr "nynorska"

#: ../../english/template/debian/language_names.wml:83
msgid "Occitan (post 1500)"
msgstr "occitanska (efter 1500)"

#: ../../english/template/debian/language_names.wml:84
msgid "Persian"
msgstr "persiska"

#: ../../english/template/debian/language_names.wml:85
msgid "Serbian"
msgstr "serbiska"

#: ../../english/template/debian/language_names.wml:86
msgid "Slovenian"
msgstr "slovenska"

#: ../../english/template/debian/language_names.wml:87
msgid "Tajik"
msgstr "tadjikiska"

#: ../../english/template/debian/language_names.wml:88
msgid "Thai"
msgstr "thailändska"

#: ../../english/template/debian/language_names.wml:89
msgid "Tonga"
msgstr "tonga"

#: ../../english/template/debian/language_names.wml:90
msgid "Twi"
msgstr "twi"

#: ../../english/template/debian/language_names.wml:91
msgid "Ukrainian"
msgstr "ukrainska"

#: ../../english/template/debian/language_names.wml:92
msgid "Vietnamese"
msgstr "vietnamesiska"

#: ../../english/template/debian/language_names.wml:93
msgid "Welsh"
msgstr "walesiska"

#: ../../english/template/debian/language_names.wml:94
msgid "Xhosa"
msgstr "xhosa"

#: ../../english/template/debian/language_names.wml:95
msgid "Yiddish"
msgstr "jiddisch"

#: ../../english/template/debian/language_names.wml:96
msgid "Zulu"
msgstr "zulu"

#~ msgid "Gallegan"
#~ msgstr "galiciska"
