#use wml::debian::template title="Debian 全球鏡像站" MAINPAGE="true"
#use wml::debian::translation-check translation="34422e94132789c2bcd0f6d41dc8680b49c7f3ab" maintainer="Kanru Chen"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Debian 国家/地区镜像站</a></li>
  <li><a href="#complete-list">完整镜像站列表</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian 正透過數百個伺服器（<a href="https://www.debian.org/mirror/">鏡像</a>）在網路上[CN:分发:][HKTW:散佈:]。如果您计划<a href="../download">下载</a> Debian，请优先使用離您較近的伺服器。这样您的下载速度可能会更快，並且也能減輕我们的中央伺服器的負擔。</p>
</aside>

<p class="centerblock">
  许多国家或地区都有 Debian 的镜像站，对于其中一些国家或地区，我们
  添加了类似 <code>ftp.&lt;国家&gt;.debian.org</code> 的别名。
  这个别名通常指向一个能够进行周期性且快速的同步，并且含有 Debian 支持的
  所有架构的镜像站。Debian 仓库总是位于
  该服务器的 <code>/debian</code> 目录，且可通过 HTTP 访问。
</p>

<p class="centerblock">
  其他镜像站可能会限制
  它們[CN:鏡像:][HKTW:映射:]的内容（因为空间有限）。
  仅仅因为一个镜像站不是该国家或地区
  的 <code>ftp.&lt;国家&gt;.debian.org</code> 并不意味着它
  比 <code>ftp.&lt;国家&gt;.debian.org</code> 镜像站更慢或内容更旧。
  事实上， 您几乎总是应当优先选择包含您所使用的硬件架构
  且离您更近（所以更快）的镜像站，而非离您更远的镜像站。
</p>

<p>使用離您最近的站台以取得最快的下載速度，不管它是不是国家/地区镜像站别名。
名為
<a href="https://packages.debian.org/stable/net/netselect-apt">
<code>netselect-apt</code></a> 的程式可以用來決定哪個站台有最小的延遲；使用下載程式
如
<a href="https://packages.debian.org/stable/web/wget">
<code>wget</code></a> 或是
<a href="https://packages.debian.org/stable/net/rsync">
<code>rsync</code></a> 來決定哪些站台有最大的輸出頻寬。
注意地理上的接近可能不是決定哪個站台最適合您的最重要因素。</p>

<p>
如果您的机器经常移动，使用基于全球
<abbr title="内容分发网络">CDN</abbr> 的<q>镜像</q>效果可能更好。
Debian 项目将域名 <code>deb.debian.org</code> 用作此种目的，您可以
在您的 <code>sources.list</code> 中使用它——访问此服务的\
<a href="http://deb.debian.org/">网站</a>以获得详细说明。
</p>

<h2 id="per-country">Debian 国家/地区镜像站</h2>

<table border="0" class="center">
<tr>
  <th>國家/地區</th>
  <th>站台</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 id="complete-list">完整镜像站列表</h2>

<table border="0" class="center">
<tr>
  <th>主機名稱</th>
  <th>HTTP</th>
  <th>硬體架構</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
