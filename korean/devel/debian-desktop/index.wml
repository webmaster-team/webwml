#use wml::debian::template title="데스크톱 데비안" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="a1eaa8012e6459917b80adb073e753fab27bfdb5"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#philosophy">우리의 철학</a></li>
<li><a href="#help">도움을 주는 법</a></li>
<li><a href="#join">참여하기</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 데비안 데스크톱은 가정 및
회사 워크스테이션에서 사용할 수 있는 최고의 운영 체제를 만드려는 자원봉사자
모임입니다. 우리 모토는: <q>동작하는 소프트웨어</q>. 우리 목표는: 데비안, GNU,
리눅스를 주류 세계로 들여 오는 것입니다.</p>
</aside>

<h2><a id="philosophy">우리의 철학</a></h2>

<p>
우리는 여러가지 <a href="https://wiki.debian.org/DesktopEnvironment">데스크톱
환경</a>이 있다는 것을 알고 있으며 그 데스크톱 환경의 사용을 지원할 것입니다.
이는 데비안에서 잘 작동하도록 만드는 것이 그 중 하나입니다. 우리 목표는
초보자가 사용하기 쉬우면서도, 고급 사용자와 전문가는 원하는대로 조정할 수 있는
그래픽 인터페이스를 만드는 것입니다.
</p>

<p>
우리는 소프트웨어가 가장 일반적인 데스크톱 사용을 위해 구성되도록 노력할
것입니다. 예를 들어, 설치 중에 생성된 일반 사용자 계정에는 sudo를 통해 오디오
및 비디오 재생, 인쇄 및 시스템 관리 권한이 있어야 합니다. 또한 <a
href="https://wiki.debian.org/debconf">debconf</a> (데비안 구성 관리 시스템)
질문을 최소한으로 하려합니다. 설치하는 동안 어려운 기술적 세부 사항을 제시할
필요가 없습니다. 대신, 우리는 debconf 질문이 사용자에게 의미가 있는지
확인하려고 노력할 것입니다. 초보자는 해당 질문의 의미를 이해하지 못할 수도
있습니다. 그러나 전문가는 설치가 완료된 후 데스크톱 환경을 구성하게 되어 매우
기쁠 것입니다.
</p>

<h2><a id="help">도움을 주는 법</a></h2>

<p>

우리는 일을 진행하는 의욕적인 분들을 찾고 있습니다. 패치를 제출하거나 패키지를
만드는데 데비안 개발자(DD)가 되지 않아도 됩니다. 핵심 데비안 데스크톱 팀이
여러분의 작업을 통합되게 할 것입니다. 도움이 될 수 있는 몇 가지 사항은 다음과
같습니다:
</p>

<ul>

  <li>다음 릴리스에 대한 기본 데스크톱 환경을 (또는 다른 데스크톱을)
  테스트하십시오. <a href="$(DEVEL)/debian-installer/">testing 이미지</a> 중
  하나를 구해 <a href="https://lists.debian.org/debian-desktop/">debian-desktop 
  메일링 리스트</a>에 피드백을 보내십시오.</li>

  <li><a href="https://wiki.debian.org/DebianInstaller/Team">데비안 인스톨러
  팀</a>에 가입하여 <a href="$(DEVEL)/debian-installer/">데비안 설치
  프로그램</a>을 개선하는 일을 도와 주십시오. GTK+ 프론트엔드에 여러분이
  필요합니다.</li>

  <li><a href="https://wiki.debian.org/Teams/DebianGnome">데비안 GNOME 팀</a>,
  <a href="https://qt-kde-team.pages.debian.net/">데비안 Qt/KDE Maintainers
  and 데비안 KDE Extras 팀</a>, 또는 <a href="https://salsa.debian.org/xfce-team/">
  데비안 Xfce 그룹</a>에서 패키징, 버그 수정, 문서, 테스트 등으로 도울 수 있습니다.</li>

  <li>질문의 우선 순위를 낮추거나 패키지에서 불필요한 질문을 없애서
  <a href="https://packages.debian.org/debconf">debconf</a>를 향상시키는 걸 도우십시오.
  필요한 debconf 질문을 이해하기 쉽게 만드십시오.</li>

  <li>디자이너 기술이 있나요? 그러면 <a
  href="https://wiki.debian.org/DebianDesktop/Artwork">Debian Desktop
  Artwork</a>에서 일해 보십시오.</li>
</ul>

<h2><a id="join">참여하기</a></h2>

<aside class="light">
  <span class="fa fa-users fa-4x"></span>
</aside>

<ul>
  <li><strong>위키:</strong> <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a> 위키 (몇몇 글은 오래됨).</li>
  <li><strong>메일링 리스트:</strong> <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a> 메일링 리스트에서 토론하세요.</li>
  <li><strong>IRC 채널:</strong> IRC에서 우리와 채팅하세요.
  <a href="http://oftc.net/">OFTC IRC</a>(irc.debian.org)에서 #debian-desktop 채널에 참여하세요.</li>
</ul>
