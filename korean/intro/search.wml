#use wml::debian::template title="데비안 검색 엔진 쓰는 방법" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>데비안 프로젝트는 자체 검색 엔진을 <a
href="https://search.debian.org/">https://search.debian.org/</a>에서 제공합니다.
그것을 어떻게 쓰는지 그리고 불 연산자를 써서 간단한 검색에서 시작해서 더 복잡한 검색을 하는 팁이 있습니다.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>단순 검색</h3>
      <p>엔진을 쓰는 가장 단순한 방법은 한 단어를 검색 필드에 넣고 [Enter]를 누르는 겁니다.
다른 방법은 <em>Search</em> 버튼을 누르세요.
검색 엔진은 단어를 포함하는 모든 페이지를 돌려줄 겁니다.
대부분 검색에서 이건 좋은 결과를 줄 겁니다.</p>
      <p>대신, 하나 보다 많은 단어를 검색할 수 있습니다. 
다시, 여러분이 입력한 모든 단어가 들어있는 데비안 웹사이트 페이지를 볼 수 있습니다.
구문을 검색하려면, 따옴표(") 안에 단어를 넣으세요.
검색 엔진은 대소문자 구별 안 함을 주의하세요. 그래서        
<code>gcc</code>는 "gcc" 는 물론 "GCC"에도 매치됩니다.
      <p>검색 필드 아래에서 페이지당 표시할 결과 수를 지정할 수 있습니다. 
다른 언어를 선택할 수도 있습니다. 데비안 웹 사이트 검색은 거의 40개의 다른 언어를 지원합니다.
</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>불 검색</h3>
      <p>단순 검색이 불충분하다면, 불 검색 연산자를 쓸 수 있습니다.
<em>AND</em>, <em>OR</em> 및 <em>NOT</em>을 쓰거나 셋 모두 조합할 수 있습니다.
모든 연산자에 대문자를 써서, 검색 엔진이 인지하도록 하세요.</p>
      <ul>
        <li><b>AND</b>는 두 식을 묶어서 두 단어 모두 들어 있는 페이지를 돌려줍니다. 
예를 들어, <code>gcc AND patch</code>는  "gcc" 그리고 "patch" 둘 다 있는 페이지를. 
이 경우에 <code>gcc patch</code> 검색할 때와 같은 결과를 얻을 수 있지만, 명시적인 <code>AND</code>는 다른 연산자와 묶을 때 쓸모 있습니다.</li>
        <li><b>OR</b>는 한 단어만 있어도 돌려줍니다. <code>gcc OR patch</code>는 "gcc" 또는 "patch"가 들어간 것을 찾아줍니다.</li>
        <li><b>NOT</b>은 결과에서 검색 단어를 제외할 때 쓰입니다. 예를 들어, <code>gcc NOT patch</code>는 "gcc"가 들어가고 "patch"는 안 들어간 것을 찾습니다. <code>gcc AND NOT patch</code>는 같은 결과를 주지만, <code>NOT patch</code>를 찾는 것은 지원 안 됩니다.</li>
        <li><b>(...)</b>는 그룹 식에 쓸 수 있습니다. 예를 들어, <code>(gcc OR make) NOT patch</code>는 gcc" 또는 "make", 그러나 "patch"는 들어가지 않은 것.</li>
      </ul>
    </div>
  </div>
</div>
