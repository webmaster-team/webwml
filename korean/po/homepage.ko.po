# Sangdo Jun <sebuls@gmail.com>, 2020
# Changwoo Ryu <cwryu@debian.org>, 2025.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2025-01-11 15:35+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "범용 운영체제"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "데브컨프가 진행 중입니다!"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "데브컨프 로고"

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr "DC22 단체 사진"

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr "데브컨프22 단체 사진"

#: ../../english/index.def:26
msgid "DC23 Group Photo"
msgstr "DC23 단체 사진"

#: ../../english/index.def:29
msgid "DebConf23 Group Photo"
msgstr "데브컨프22 단체 사진"

#: ../../english/index.def:33
msgid "DC24 Group Photo"
msgstr "DC24 단체 사진"

#: ../../english/index.def:36
msgid "DebConf24 Group Photo"
msgstr "데브컨프24 단체 사진"

#: ../../english/index.def:40
msgid "MiniDebConf Berlin 2024"
msgstr "미니 데브컨프 베를린 2024"

#: ../../english/index.def:43
msgid "Group photo of the MiniDebConf Berlin 2024"
msgstr "미니 데브컨프 베를린 2024 단체 사진"

#: ../../english/index.def:47
msgid "MiniDebConf Brasília 2023"
msgstr "미니 데브컨프 브라질리아 2023"

#: ../../english/index.def:50
msgid "Group photo of the MiniDebConf Brasília 2023"
msgstr "미니 데브컨프 브라질리아 2023 단체 사진"

#: ../../english/index.def:54
msgid "Mini DebConf Regensburg 2021"
msgstr "미니 데브컨프 레겐부르크 2021"

#: ../../english/index.def:57
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "미니 데브컨프 레겐부르크 2021 단체 사진"

#: ../../english/index.def:61
msgid "Screenshot Calamares Installer"
msgstr "스크린샷 칼라마리 인스톨러"

#: ../../english/index.def:64
msgid "Screenshot from the Calamares installer"
msgstr "칼라마리 인스톨러의 스크린샷"

#: ../../english/index.def:68 ../../english/index.def:71
msgid "Debian is like a Swiss Army Knife"
msgstr "데비안은 스위스 군용 칼 같습니다"

#: ../../english/index.def:75
msgid "People have fun with Debian"
msgstr "사람들이 데비안과 함께 재미있게 지냅니다"

#: ../../english/index.def:78
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "신주 DebConf18에서 즐기는 데비안 사람들"

#~ msgid "Debian Reunion Hamburg 2023"
#~ msgstr "데비안 리유나이언 함부르크 2023"

#~ msgid "Group photo of the Debian Reunion Hamburg 2023"
#~ msgstr "데비안 리유나이언 함부르크 2023 단체 사진"
