msgid ""
msgstr ""
"Project-Id-Version: stats.po\n"
"PO-Revision-Date: 2011-03-20 06:09+0900\n"
"Last-Translator: Osamu Aoki <osamu@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian ウェブサイト翻訳状況"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "翻訳するページ数 %d"

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "翻訳するバイト数 %d"

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "翻訳する文の数 %d"

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "翻訳のバージョン番号が変です"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "翻訳が古すぎです"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "原版がこの翻訳より新しくなっています"

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "原版がなくなっています"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "ヒットカウント無し"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "ヒット"

#: ../../stattrans.pl:600 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:605
msgid "Translation summary for"
msgstr "翻訳状況:"

#: ../../stattrans.pl:608 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "未翻訳"

#: ../../stattrans.pl:608 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "未更新"

#: ../../stattrans.pl:608
msgid "Translated"
msgstr "翻訳済"

#: ../../stattrans.pl:608 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "最新"

#: ../../stattrans.pl:609 ../../stattrans.pl:610 ../../stattrans.pl:611
#: ../../stattrans.pl:612
msgid "files"
msgstr "ファイル"

#: ../../stattrans.pl:615 ../../stattrans.pl:616 ../../stattrans.pl:617
#: ../../stattrans.pl:618
msgid "bytes"
msgstr "バイト"

#: ../../stattrans.pl:625
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"注: このページのリストは人気順に並んでいます。ページ名にマウスカーソルを持っ"
"て行くとヒット数を確認できます。"

#: ../../stattrans.pl:631
msgid "Outdated translations"
msgstr "古くなっている翻訳"

#: ../../stattrans.pl:633 ../../stattrans.pl:686
msgid "File"
msgstr "ファイル"

#: ../../stattrans.pl:635
msgid "Diff"
msgstr "差分"

#: ../../stattrans.pl:637
msgid "Comment"
msgstr "状態"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:640
msgid "Log"
msgstr "ログ"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "翻訳版"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "担当者"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "状態"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "翻訳者"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "日時"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "未翻訳の一般ページ"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "未翻訳の一般ページ"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "未翻訳のニュース項目"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "未翻訳のニュース項目"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "未翻訳のコンサルタント/ユーザページ"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "未翻訳のコンサルタント/ユーザページ"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "未翻訳の国際化ページ"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "未翻訳の国際化ページ"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "翻訳済ページ (最新)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "翻訳済テンプレート (PO ファイル)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "PO 翻訳状況"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Fuzzy"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "未翻訳"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "合計"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "合計:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "翻訳済ページ"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "ページ単位で集計した翻訳状況"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "言語"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "翻訳版"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "翻訳済ページ (サイズ)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "ページサイズから見た翻訳状況"

#~ msgid "Click to fetch diffstat data"
#~ msgstr "クリックすると diffstat データを取得します"

#~ msgid "Colored diff"
#~ msgstr "差分(色分け)"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "差分(色分け)"

#~ msgid "Created with"
#~ msgstr "Created with"

#~ msgid "Diffstat"
#~ msgstr "Diffstat"

#~ msgid "Origin"
#~ msgstr "原版"

#~ msgid "Unified diff"
#~ msgstr "差分(統合)"
