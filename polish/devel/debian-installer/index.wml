#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="7d94266e1b97023d2c8b58cc9a6ecb77eb9e079e"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Wiadomości</h1>

<p><:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Starsze wiadomości</a>
</p>

<h1>Instalacja przy pomocy Instalatora Debiana</h1>
 
<p>
<if-stable-release release="bullseye">
<strong>Oficjalne nośniki instalacyjne oraz informacje dotyczące Debiana 
<current_release_bullseye></strong> są dostępne na 
<a href="$(HOME)/releases/bullseye/debian-installer">stronie wydania bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>Oficjalne nośniki instalacyjne oraz informacje dotyczące Debiana
<current_release_bookworm></strong> są dostępne na
<a href="$(HOME)/releases/bookworm/debian-installer">stronie wydania bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Wszystkie poniższe obrazy są dla wersji instalatora przygotowywanej dla
następnej wersji Debiana, i domyślnie powodują instalację testowej wersji
Debiana (<q><current_testing_name></q>).
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">

<p>
<strong>Aby zainstalować wersję testową Debiana</strong>, zalecamy użyć
<strong>codziennych kompilacji instalatora</strong>. Są dostępne następujące obrazy
codziennych kompilacji:
</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">

<p>
<strong>Aby zainstalować wersję testową Debiana</strong>, zalecamy użyć
wydania instalatora <strong><humanversion /></strong>, po sprawdzeniu jego
<a href="errata">erraty</a>. Są dostępne następujące obrazy
<humanversion />:
</p>

<h2>Wydanie oficjalne</h2>

<div class="line">
<div class="item col50">
<strong>obrazy CD netinst (instalacja z sieci, ok. 150-450 MB)</strong>
<netinst-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<full-dvd-images />
</div>

</div>


<div class="line">
<div class="item col50">
<strong>zestawy CD (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>zestawy DVD (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>zestawy Blu-ray (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>inne obrazy (netboot, na urządzenia USB itp.)</strong>
<other-images />
</div>
</div>

<p>
Lub zainstaluj <strong>bieżącą migawkę tygodniową Debian testing</strong>,
która używa tej samej wersji instalatora co ostatnie wydanie:
</p>

<h2>Aktualne migawki tygodniowe</h2>

<div class="line">
<div class="item col50">
<strong>CD</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>DVD</strong>
<devel-full-dvd-images />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>CD (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>DVD (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Blu-ray (przez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

<p>
Jeśli chciałbyś użyć najnowszej wersji instalatora, na przykład aby pomóc nam
testować przyszłe wydanie, lub masz problemy ze sprzętem, albo inne kłopoty,
spróbuj jednej z <strong>codziennych kompilacji</strong>, która zawiera
najnowsze dostępne wersje komponentów instalatora.
</p>

</if-testing-installer>

<h2>Aktualne migawki codzienne</h2>

<div class="line">
<div class="item col50">
<strong>obrazy płyt netinst (zasadniczo 150-280 MB)<!-- i businesscard (ok. 20-50 MB)--></strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>obrazy netinst <!-- i businesscard --> (poprzez <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>obrazy netinst multi-arch</strong>
<devel-multi-arch-cd />
</div>

<div class="item col50 lastcol">
<strong>inne obrazy (netboot, USB stick, itd.)</strong>
<devel-other-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml

<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w
<a href="https://d-i.debian.org/doc/installation-guide/en.amd64/ch06s04.html">podręczniku instalacji (w języku angielskim)</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/daily-builds/sid_d-i/current/">nieoficjalne
obrazy zawierające firmware - codzienne kompilacje</a>
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/">nieoficjalne
obrazy zawierające firmware - cotygodniowe kompilacje</a>

 </p>
</div>

<hr />

<p>
<strong>Uwagi</strong>
</p>
<ul>
#	<li>Przed pobraniem obrazu dziennej kompilacji zalecamy sprawdzić
#	<a href="https://wiki.debian.org/DebianInstaller/Today">znane problemy</a>.</li>
	<li>Dana architektura może (tymczasowo) nie być uwzględniona w liście
	codziennie budowanych obrazów, jeśli nie ma dla niej (konsekwentnie) dostępnych
	codziennych kompilacji.</li>
	<li>Pliki sum kontrolnych dla obrazów płyt (<tt>SHA256SUMS</tt>, <tt>SHA512SUMS</tt>
	i innych) są dostępne w tym samym katalogu co one.</li>	
	<li>Do pobierania pełnych obrazów płyt CD i DVD zalecamy użycie
	jigdo.</li>
	<li>Tylko wybrane obrazy z zestawów CD i DVD są dostępne w postaci plików
	ISO dostępnych do bezpośredniego pobrania. Większość użytkowników nie
	potrzebuje całego oprogramowania zawartego na tych dyskach, więc aby
	zaoszczędzić miejsce na serwerach pełne obrazy są dostępne jedynie
	przez jigdo.</li>
	<li>Obraz <em>netinst CD</em> multi-arch obsługuje architektury
	i386/amd64; instalacja jest zbliżona do
	instalacji pojedynczej architektury z obrazu netinst.</li>
</ul>

<p>
<strong>Po użyciu Instalatora Debiana</strong> prosimy o wysłanie nam
<a href="https://d-i.debian.org/manual/en.amd64/ch05s04.html#submit-bug">raportu
poinstalacyjnego</a>, nawet jeśli nie było żadnych problemów.
</p>

<h1>Dokumentacja</h1>

<p>
<strong>Jeśli czytasz tylko jeden dokument</strong> przed instalacją, przeczytaj nasze
<a href="https://d-i.debian.org/manual/en.amd64/apa.html">Installation
Howto</a>, krótki przegląd procesu instalacji. Inne przydatne dokumenty to:
</p>

<ul>
<li>Przewodnik po instalacji:
#    <a href="$(HOME)/releases/stable/installmanual">wersja dla
#       obecnego wydania</a> &mdash;
    <a href="$(HOME)/releases/testing/installmanual">wersja deweloperska (testing)</a> &mdash;
    <a href="https://d-i.debian.org/manual/">najnowsza wersja (Git)</a>
<br />
szczegółowe instrukcje dotyczące instalacji</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ Instalatora Debiana</a>
i <a href="$(HOME)/CD/faq/">FAQ Debian-CD</a><br />
częste pytania i odpowiedzi</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
dokumentacja prowadzona przez społeczność</li>
</ul>

<h1>Kontakt</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">Lista dyskusyjna
debian-boot</a> jest głównym forum dyskusji i pracy nad Instalatorem
Debiana.
</p>

<p>
Istnieje też kanał IRC #debian-boot na <tt>irc.debian.org</tt>. Jest
on używany głównie przez deweloperów, ale czasem też dla wsparcia.
Jeśli nie otrzymasz odpowiedzi, spróbuj listy dyskusyjnej.
</p>
