#use wml::debian::cdimage title="Pobieranie obrazów płyt Debiana przez HTTP/FTP" BARETITLE=true
#use wml::debian::translation-check translation="f4fe84f1063f34ac4bfb75eb963e6fa71ba0e642"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<div class="tip">
<p><strong>Prosimy nie pobierać obrazów płyt za pomocą przeglądarki
www, tak jak pobiera się inne pliki!</strong> Jeśli pobieranie zostanie
przerwane, większość przeglądarek nie poradzi sobie ze wznowieniem od miejsca,
w którym nastąpiło przerwanie.</p>
</div>

<p>Zamiast tego prosimy używać narzędzi, które pozwalają na wznawianie - opisywane 
zazwyczaj jako <q>menedżer pobierania</q>. Istnieje wiele wtyczek do przeglądarek, które 
na to pozwalają. Pod Linuxem/Unixem możesz użyć programów 
<a href="http://aria2.sourceforge.net/">aria2</a>,
<a href="http://dfast.sourceforge.net/">wxDownload Fast</a> lub
(jako polecenie linii komend)
<q><tt>wget&nbsp;-c&nbsp;</tt><em>URL</em></q>, czy też
<q><tt>curl&nbsp;-C&nbsp;-&nbsp;-L&nbsp;-O&nbsp;</tt><em>URL</em></q>.

Więcej opcji jest opisanych na <a
href="https://en.wikipedia.org/wiki/Comparison_of_download_managers">porównanie
menedżerów pobierania</a>.</p>

<p>Są dostępne następujące obrazy płyt Debiana:</p>

<ul>	
  <li><a href="#stable">Oficjalne obrazy płyt CD/DVD wydania
	  <q>stabilnego</q></a></li>
  <li><a href="#firmware"><b>Nieoficjalne</b> obrazy płyt CD/DVD wydania <q>stabilnego</q>
  z dołączonym <b>niewolnym</b> oprogramowaniem firmware.</a></li>
  <li><a href="https://cdimage.debian.org/cdimage/weekly-builds/">Oficjalne
	obrazy płyt CD/DVD wydania <q>testowego</q> (<em>odnawiane co
	tydzień</em>)</a></li>

<comment>
  <li>Nieoficjalne obrazy płyt CD/DVD dystrybucji <q>testowej</q> i
  <q>niestabilnej</q> rozprowadzane przez fsn://HU &mdash;
  <a href="#unofficial">patrz niżej</a></li>
</comment>

</ul>

<p>Zobacz też:</p>
<ul>
  <li>Pełną <a href="#mirrors">listę mirrorów <tt>debian-cd/</tt></a></li>
  <li>Obrazy do <q>Instalacji sieciowej</q> (150-300&nbsp;MB) 
      są opisane na stronie <a href="../netinst/">instalacji sieciowej</a></li>
  <li>Obrazy <q>netinst</q> wydania testowego, zarówno
      obrazy dzienne jak i migawki, znajdują się na stronie <a
      href="$(DEVEL)/debian-installer/">Debian-Installer</a>.</li>
</ul>

<hr />

<h2><a name="stable">Oficjalne obrazy płyt CD/DVD wydania <q>stabilnego</q></a></h2>

<p>Możliwe jest zainstalowanie Debiana bez dostępu do internetu przy użyciu
płyt CD (każda po 700&nbsp;MB) lub DVD (po 4,7&nbsp;GB). Pobierz pierwszy
obraz płyty, zapisz go na nośniku za pomocą nagrywarki (lub na pamięci USB 
dla architektury i386 i amd64) i z takiego nośnika wystartuj
komputer.</p>

<p><strong>Pierwsza</strong> płyta zawiera wszystkie pliki konieczne do
instalacji standardowego systemu Debiana.<br />
Aby zapobiec pobieraniu z internetu zbyt wielu danych, prosimy
<strong>nie</strong> ściągać innych obrazów płyt przed zapoznaniem się
z ich zawartością i określeniem przydatności pakietów.</p>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>

<p>Następujące linki prowadzą do obrazów, z których każdy ma do 700&nbsp;MB,
czyli odpowiednich do nagrania zwykłej płyty CD-R(W):</p>

<stable-full-cd-images />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>

<p>Następujące linki prowadzą do obrazów, z których każdy ma do 4,7&nbsp;GB,
czyli odpowiednich do nagrania zwykłej płyty DVD-R/DVD+R (i podobnych):</p>

<stable-full-dvd-images />
</div><div class="clear"></div>
</div>

<p>Prosimy zajrzeć do dokumentacji przed instalacją.
<strong>Jeśli czytasz tylko jeden dokument</strong>, przeczytaj nasze
<a href="$(HOME)/releases/stable/i386/apa">Installation Howto</a>, krótki
przegląd procesu instalacyjnego. Inne użyteczne dokumenty to:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installation Guide</a>,
    szczegółowy opis instalacji</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Dokumentacja Debian-Installer</a>,
    która zawiera FAQ z pytaniami i odpowiedziami</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata do
    Debian-Installer</a>, lista znanych problemów występujących w instalatorze.</li>
</ul>

<hr />

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<h2><a name="firmware">Nieoficjalne obrazy płyt CD/DVD z dołączonym niewolnym oprogramowaniem firmware</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Jeżeli jakikolwiek sprzęt w systemie <strong>wymaga załadowania niewolnego
oprogramowania firmware</strong> wraz ze sterownikiem, można użyć jednego z
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archiwów zawierających pakiety z firmware</a> lub pobrać
<strong>nieoficjalny</strong> obraz zawierający <strong>niewolny</strong> firmware.
Instrukcje, jak użyć tych archiwów i podstawowe
informacje na temat ładowania firmware podczas instalacji
są zamieszczone w <a href="../../releases/stable/amd64/ch06s04">Podręczniku Instalacji</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">nieoficjalne
obrazy instalacyjne dla wersji <q>stabilnej</q> z zawartym firmwarem</a>
</p>
</div>

<hr />

<h2><a name="mirrors">Zarejestrowane mirrory archiwum <q>debian-cd</q></a></h2>

<p>Zwracamy uwagę, że <strong>niektóre mirrory nie są zaktualizowane</strong>
&mdash; przed pobraniem sprawdź, czy numer wersji obrazu jest identyczny
<a href="../#latest">z tą listą</a>!
Oprócz tego wiadomo, że wiele stron nie udostępnia wszyskich obrazów
(zwłaszcza DVD), ze względu na ich rozmiar.</p>

<p><strong>W razie wątpliwości użyj <a href="https://cdimage.debian.org/debian-cd/">głównego
serwera obrazów płyt</a> w Szwecji</strong> lub 
<a href="http://debian-cd.debian.net/">eksperymentalnego automatycznego 
wybierania serwera lustrzanego</a>, który automatycznie przekieruje Cię 
na najbliższy znany serwer z aktualną wersją.</p>

<p>Jeśli jesteś zainteresowany oferowaniem obrazów płyt Debiana na swoim
serwerze, przeczytaj <a href="../mirroring/">instrukcję dotyczącą konfiguracji
mirrora obrazów CD</a>.</p>

#use wml::debian::countries
#include "$(ENGLISHDIR)/CD/http-ftp/cdimage_mirrors.list"
