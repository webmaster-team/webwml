# From: John S4 Hosting <john@s-4.host>
# Submission email: https://lists.debian.org/debian-www/2021/07/msg00031.html
#use wml::debian::translation-check translation="3a5997bd52b54098502c054c9094e7ff267ed51a" maintainer="Jean-Paul Guillonneau"

<define-tag pagetitle>S4 Hosting, Lituanie</define-tag>
<define-tag webpage>https://s-4.host</define-tag>

#use wml::debian::users

<p>
Nous sommes une entreprise d’hébergement éthique et durable et, jusqu’à récemment,
la plupart de nos serveurs web fonctionnaient sous CentOS, en parallèle avec
un amalgame d’autres distributions pour les serveurs de
courriels ou de sauvegardes.
</p>

<p>
Durant l’année dernière, comme notre entreprise a quelque peu grandi et que le
modèle de publication de CentOS a changé, nous avons recherché activement
une nouvelle solution et planifié de regrouper tous nos serveurs sous une
unique distribution Linux. Nous avons fait beaucoup de recherches et construit
de nombreux serveurs de tests, et nous sommes arrivés à la conclusion que Debian est
finalement le meilleur choix pour nous. Nous migrons donc tous sous des
serveurs basés sur Debian.
</p>

<p>
Les logiciels au code source ouvert ont été au centre de notre éthos depuis le
début, mais c’est une chose à laquelle nous attachons de plus en plus
d’importance. Lorsque les projets au code source ouvert sont dirigés
par des fondations ou des communautés, sans être dépendants ou possédés par des
entreprises commerciales, c'est ce qu'il y a de mieux. Et de ce point de vue rien ne vaut
Debian.
</p>

<p>
Puisque nous hébergeons des sites de clients et des serveurs de courriels en
production, la sécurité et la stabilité sont aussi extrêmement importantes pour
nous et, de nouveau, nous n’avons rien trouvé qui rivalise avec Debian quant à la
stabilité.
</p>
