#use wml::debian::translation-check translation="9d41ab1625a3bbe9bf95b782d91e91b766a3f664" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.5</define-tag>
<define-tag release_date>2024-02-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la cinquième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apktool "Écriture évitée de fichiers arbitraires avec des noms de ressources malveillants [CVE-2024-21633]">
<correction atril "Correction d'un plantage lors de l'ouverture de certains fichiers epub ; correction du chargement de l'index de certains documents epub ; ajout d'une solution de repli pour les fichiers epub mal formés dans check_mime_type ; utilisation de libarchive à la place d'une commande externe pour l'extraction de documents [CVE-2023-51698]">
<correction base-files "Mise à jour pour cette version">
<correction caja "Correction d'artefacts de rendu du bureau après une modification de la résolution de l'affichage ; correction de l'utilisation du format de date <q>informal</q>">
<correction calibre "Correction de <q>Entrée HTML : pas d'ajout de ressources qui se trouvent en dehors de la hiérarchie de répertoires dont la racine est le répertoire parent du fichier d'entrée HTML par défaut</q> [CVE-2023-46303]">
<correction compton "Retrait de la recommandation de picom">
<correction cryptsetup "cryptsetup-initramfs : ajout de la prise en charge des modules du noyau compressés ; cryptsetup-suspend-wrapper : pas d'émission d'erreur en l'absence du répertoire /lib/systemd/system-sleep ; add_modules() : modification de la logique de suppression de suffixe pour correspondre à initramfs-tools">
<correction debian-edu-artwork "Fourniture d'un thème graphique basé sur Emerald pour Debian Edu 12">
<correction debian-edu-config "Nouvelle version amont">
<correction debian-edu-doc "Mise à jour de la documentation incluse et des traductions">
<correction debian-edu-fai "Nouvelle version amont">
<correction debian-edu-install "Nouvelle version amont ; correction du fichier sources.list de sécurité">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-18 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-ports-archive-keyring "Ajout de la clé de signature automatique de l'archive des portages de Debian (2025)">
<correction dpdk "Nouvelle version amont stable">
<correction dropbear "Correction de <q>l'attaque Terrapin</q> [CVE-2023-48795]">
<correction engrampa "Correction de plusieurs fuites de mémoire ; correction de la fonction <q>save as</q> de l'archive">
<correction espeak-ng "Correction de problèmes de dépassement de tampon [CVE-2023-49990 CVE-2023-49992 CVE-2023-49993], d'un problème de dépassement de tampon par le bas [CVE-2023-49991], d'un problème d'exception de virgule flottante [CVE-2023-49994]">
<correction filezilla "Exploitation de l'attaque <q>Terrapin</q> évitée [CVE-2023-48795]">
<correction fish "Gestion sûre des caractères Unicode non imprimables quand ils sont passés comme substitution de commande [CVE-2023-49284]">
<correction fssync "Désactivation de tests peu fiables">
<correction gnutls28 "Correction d'un échec d'assertion lors de la vérification d'une chaîne de certificats avec un cycle de signatures croisées [CVE-2024-0567] ; correction d'un problème d'attaque temporelle par canal auxiliaire [CVE-2024-0553]">
<correction indent "Correction d'un problème d’un soupassement de lecture de tampon [CVE-2024-0911]">
<correction isl "Correction de l'utilisation sur les vieux processeurs">
<correction jtreg7 "Nouveau parquet source pour prendre en charge les constructions d'openjdk-17">
<correction libdatetime-timezone-perl "Mise à jour des données de zone horaire incluses">
<correction libde265 "Correction de problèmes de dépassement de tampon [CVE-2023-49465 CVE-2023-49467 CVE-2023-49468]">
<correction libfirefox-marionette-perl "Correction de compatibilité avec les versions récentes de firefox-esr">
<correction libmateweather "Correction de l'URL d'aviationweather.gov">
<correction libspreadsheet-parsexlsx-perl "Correction d'une possible <q>memory bomb</q> [CVE-2024-22368] ; correction d'un problème d'entité externe XML [CVE-2024-23525]">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 18">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 18">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 18">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 18">
<correction localslackirc "Envoi des en-têtes d'autorisation et de cookie au websocket">
<correction mariadb "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2023-22084]">
<correction mate-screensaver "Correction de fuites de mémoire">
<correction mate-settings-daemon "Correction de fuites de mémoire ; assouplissement des limites de haute résolution (<q>High DPI</q>) ; correction de la gestion de multiples événements de rfkill">
<correction mate-utils "Correction de diverses fuites de mémoire">
<correction monitoring-plugins "Correction du greffon check_http quand <q>--no-body</q> est utilisé et la réponse amont est <q>chunked</q>">
<correction needrestart "Correction de régression de vérification du microcode sur les processeurs AMD">
<correction netplan.io "Correction de autopkgtests avec les versions récentes de systemd">
<correction nextcloud-desktop "Correction de <q>échec de synchronisation de fichiers avec des caractères spéciaux tels que « : »</q> ; correction des notifications d'authentification à deux facteurs">
<correction node-yarnpkg "Correction de l'utilisation avec Commander 8">
<correction onionprobe "Correction de l'initialisation de Tor lors de l'utilisation de mots de passe hachés">
<correction pipewire "Utilisation de malloc_trim() quand l'instruction est disponible pour libérer la mémoire">
<correction pluma "Correction de problèmes de fuite de mémoire ; correction de double activation des extensions">
<correction postfix "Nouvelle version amont stable ; traitement d'un problème de dissimulation d'adresse SMTP [CVE-2023-51764]">
<correction proftpd-dfsg "Implémentation d'une correction pour <q>l'attaque Terrapin</q> [CVE-2023-48795] ; correction d'un problème de lecture hors limites [CVE-2023-51713]">
<correction proftpd-mod-proxy "Implémentation d'une correction pour <q>l'attaque Terrapin</q> [CVE-2023-48795]">
<correction pypdf "Correction d'un problème de boucle infinie [CVE-2023-36464]">
<correction pypdf2 "Correction d'un problème de boucle infinie [CVE-2023-36464]">
<correction pypy3 "Évitement de l’erreur d'assertion de RPython dans le JIT si des plages de valeurs entières ne se recouvrent pas dans une boucle">
<correction qemu "Nouvelle version amont stable ; virtio-net : copie correcte de l'en-tête vnet lors du vidage de tampon TX [CVE-2023-6693] ; correction d'un problème de déréférencement de pointeur NULL [CVE-2023-6683] ; suppression du correctif provoquant des régressions dans la fonction <q>suspend/resume</q>">
<correction rpm "Activation du dorsal BerkeleyDB en lecture seule">
<correction rss-glx "Installation des économiseurs d'écran dans /usr/libexec/xscreensaver ; appel de GLFinish() avant celui de glXSwapBuffers()">
<correction spip "Correction de deux problèmes de script intersite">
<correction swupdate "Évitement de l’acquisition de privilèges de superutilisateur au moyen d'un mode de socket inapproprié">
<correction systemd "Nouvelle version amont stable ; correction d'un problème d'absence de vérification dans systemd-resolved [CVE-2023-7008]">
<correction tar "Correction de vérification de limites dans le décodeur base 256 [CVE-2022-48303], gestion des préfixes d'en-tête étendus [CVE-2023-39804]">
<correction tinyxml "Correction d'un problème d'assertion [CVE-2023-34194]">
<correction tzdata "Nouvelle version amont stable">
<correction usb.ids "Mise à jour de la liste des données incluses">
<correction usbutils "Correction de usb-devices qui n'affiche pas tous les périphériques">
<correction usrmerge "Nettoyage des répertoires bi-architectures quand ils ne sont pas nécessaires ; pas de nouvelle exécution de convert-etc-shells sur les systèmes convertis ; gestion des /lib/modules montés sur les systèmes Xen ; amélioration de rapport d'erreur ; ajout d'une version pour Conflicts avec libc-bin, dhcpcd, libparted1.8-10 et lustre-utils">
<correction wolfssl "Correction d'un problème de sécurité quand le client n'envoie jamais d'extension PSK ou KSE [CVE-2023-3724]">
<correction xen "Nouvelle version amont stable ; corrections de sécurité [CVE-2023-46837 CVE-2023-46839 CVE-2023-46840]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5572 roundcube>
<dsa 2023 5573 chromium>
<dsa 2023 5574 libreoffice>
<dsa 2023 5576 xorg-server>
<dsa 2023 5577 chromium>
<dsa 2023 5578 ghostscript>
<dsa 2023 5579 freeimage>
<dsa 2023 5581 firefox-esr>
<dsa 2023 5582 thunderbird>
<dsa 2023 5583 gst-plugins-bad1.0>
<dsa 2023 5584 bluez>
<dsa 2023 5585 chromium>
<dsa 2023 5586 openssh>
<dsa 2023 5587 curl>
<dsa 2023 5588 putty>
<dsa 2023 5589 node-undici>
<dsa 2023 5590 haproxy>
<dsa 2023 5591 libssh>
<dsa 2023 5592 libspreadsheet-parseexcel-perl>
<dsa 2024 5593 linux-signed-amd64>
<dsa 2024 5593 linux-signed-arm64>
<dsa 2024 5593 linux-signed-i386>
<dsa 2024 5593 linux>
<dsa 2024 5595 chromium>
<dsa 2024 5597 exim4>
<dsa 2024 5598 chromium>
<dsa 2024 5599 phpseclib>
<dsa 2024 5600 php-phpseclib>
<dsa 2024 5601 php-phpseclib3>
<dsa 2024 5602 chromium>
<dsa 2024 5603 xorg-server>
<dsa 2024 5605 thunderbird>
<dsa 2024 5606 firefox-esr>
<dsa 2024 5607 chromium>
<dsa 2024 5608 gst-plugins-bad1.0>
<dsa 2024 5609 slurm-wlm>
<dsa 2024 5610 redis>
<dsa 2024 5611 glibc>
<dsa 2024 5612 chromium>
<dsa 2024 5613 openjdk-17>
<dsa 2024 5614 zbar>
<dsa 2024 5615 runc>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Mises à jour proposées à la distribution stable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

