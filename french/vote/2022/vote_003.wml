<define-tag pagetitle>Résolution générale : microprogrammes non libres</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::translation-check translation="9b613d7447827b46baa87229fc9235472e047a76" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Période de débat :</th>
	<td>18 août 2022</td>
	<td>7 septembre 2022</td>
      </tr>
#      <tr>
#	<th>Période de vote :</th>
#	<td>dimanche 18 septembre 2022 00:00:00 UTC</td>
#	<td>samedi 1er octobre 23:59:59 UTC</td>
#      </tr>
    </table>

    La période de débat a été prolongée de sept jours par le responsable du projet Debian.
    [<a href='https://lists.debian.org/debian-vote/2022/09/msg00037.html'>message</a>]</li>

    <vproposera />
    <p>Steve McIntyre [<email 93sam@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00001.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00194.html'>amendement</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00002.html'>message</a>]</li>
        <li>Luca Boccassi [<email bluca@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00003.html'>message</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00004.html'>message</a>]</li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00005.html'>message</a>]</li>
        <li>Sebastian Ramacher [<email sramacher@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00006.html'>message</a>]</li>
        <li>Samuel Henrique [<email samueloph@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00007.html'>message</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00008.html'>message</a>]</li>
        <li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00009.html'>message</a>]</li>
        <li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00011.html'>message</a>]</li>
        <li>Cyril Brulebois [<email kibi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00014.html'>message</a>]</li>
        <li>Iain Lane [<email laney@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00015.html'>message</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00023.html'>message</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00029.html'>message</a>]</li>
        <li>Anton Gladky [<email gladk@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00030.html'>message</a>]</li>
        <li>Moritz Mühlenhoff [<email jmm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00031.html'>message</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00034.html'>message</a>]</li>
    </ol>
    <vtexta />
#	<h3>Choix 1 : un seul installateur, incluant des microprogrammes non libres</h3>

<p>Nous inclurons les paquets de microprogrammes non libres de la section
<q>non-free-firmware</q> de l'archive Debian dans nos supports officiels (images
de l'installateur et images autonomes). Les binaires des microprogrammes inclus
seront <q>normalement</q> activés par défaut lorsque le système détermine qu'ils
sont requis, mais quand c'est possible, nous inclurons des moyens pour que
l'utilisateur les désactive à l'amorçage (option du menu de démarrage, ligne de
commande du noyau, etc.).</p>

<p>Lors de l'exécution de l'installateur ou du système autonome, nous fournirons
à l'utilisateur des informations sur les microprogrammes chargés (aussi bien les
libres que les non libres), et nous sauvegarderons aussi ces informations sur
le système cible de sorte que l'utilisateur puisse les retrouver plus tard. 
S'il se trouve que des microprogrammes sont nécessaires, le système cible sera
<b>aussi</b> configuré pour utiliser par défaut le composant
<q>non-free-firmware</q> dans le ficher sources.list d'apt.
Nos utilisateurs devraient recevoir les mises à jour de sécurité et les
correctifs importants pour les binaires des microprogrammes exactement comme
pour tous les autres logiciels installés.</p>

<p>Nous publierons ces images en tant que médias officiels de Debian, remplaçant
le jeu de médias actuel qui n'inclut pas les paquets de microprogrammes non
libres.</p>

    <vproposerb />
    <p>Gunnar Wolf [<email gwolf@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00046.html'>texte de la proposition</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00047.html'>message</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00048.html'>message</a>]</li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00053.html'>message</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00055.html'>message</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00060.html'>message</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00093.html'>message</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00120.html'>message</a>]</li>
        <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00122.html'>message</a>]</li>
        <li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00168.html'>message</a>]</li>
    </ol>
    <vtextb />
#<h3>Choix 2 : recommander un installateur contenant des microprogrammes non libres</h3>

<p>Nous inclurons les paquets de microprogrammes non libres de la section
<q>non-free-firmware</q> de l'archive Debian dans nos supports officiels (images
de l'installateur et images autonomes). Les binaires des microprogrammes inclus
seront <q>normalement</q> activés par défaut lorsque le système détermine qu'ils
sont requis, mais quand c'est possible, nous inclurons des moyens pour que
l'utilisateur les désactive à l'amorçage (option du menu de démarrage, ligne de
commande du noyau, etc.).</p>

<p>Lors de l'exécution de l'installateur ou du système autonome, nous fournirons
à l'utilisateur des informations sur les microprogrammes chargés (aussi bien les
libres que les non libres), et nous sauvegarderons aussi ces informations sur
le système cible de sorte que l'utilisateur puisse les retrouver plus tard. 
S'il se trouve que des microprogrammes sont nécessaires, le système cible sera
<b>aussi</b> configuré pour utiliser par défaut le composant
<q>non-free-firmware</q> dans le ficher sources.list d'apt.
Nos utilisateurs devraient recevoir les mises à jour de sécurité et les
correctifs importants pour les binaires des microprogrammes exactement comme
pour tous les autres logiciels installés.</p>

<p>Même si nous publions ces images comme médias officiels de Debian, elles
<b>ne remplaceront pas</b> les jeux de médias actuels qui n'incluent pas de
paquets de microprogrammes non libres, mais seront proposées à côté. Les images
qui incluent les microprogrammes non libres seront proposées de façon plus
visible de manière que les débutants pourront les trouver plus facilement ;
les images complètement libres ne seront pas dissimulées ; leurs liens seront
sur les mêmes pages du projet, mais avec une visibilité moins importante.</p>

    <vproposerc />
    <p>Bart Martens [<email bartm@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00106.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/09/msg00041.html'>amendement</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>Stefano Zacchiroli [<email zack@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00116.html'>message</a>]</li>
        <li>Laura Arjona Reina [<email larjona@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00119.html'>message</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00123.html'>message</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00128.html'>message</a>]</li>
        <li>Philip Rinn [<email rinni@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00160.html'>message</a>]</li>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00044.html'>message</a>]</li>
        <li>Paul Wise [<email pabs@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00055.html'>message</a>]</li>
        <li>Simon Josefsson [<email jas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00086.html'>message</a>]</li>
    </ol>
    <vtextc />
#<h3>Choix 3 : permettre de présenter des installateurs non libres à côté de l'installateur libre</h3>

<p>Le projet Debian est autorisé à réaliser des médias de distribution (images
de l'installateur et images autonomes) contenant des programmes non libres
provenant de l'archive Debian disponible en téléchargement en même
temps que les médias libres de manière que l'utilisateur sait, avant de
procéder au téléchargement, quels médias sont les médias libres.</p>

    <vproposerd />
    <p>Simon Josefsson [<email jas@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/08/msg00173.html'>texte de la proposition</a>]
    </p>
    <vsecondsd />
    <ol>
        <li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00068.html'>message</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00071.html'>message</a>]</li>
        <li>Hubert Chathi [<email uhoreg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00074.html'>message</a>]</li>
        <li>Guilhem Moulin [<email guilhem@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00167.html'>message</a>]</li>
        <li>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/08/msg00211.html'>message</a>]</li>
        <li>Shengjing Zhu [<email zhsj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00000.html'>message</a>]</li>
    </ol>
    <vtextd />
#<h3>Choix 4 : un installateur avec des microprogrammes non libres ne fait pas partie de Debian</h3>

<p>Nous continuerons à soutenir l'esprit du Contrat social de Debian qui dit
dans son premier paragraphe :</p>

<pre>
   Debian demeurera totalement libre.

   Le document intitulé <q>Principes du logiciel libre selon
   Debian</q> énonce les critères grâce auxquels le projet Debian
   détermine si un travail est <q>libre</q>. Nous promettons que
   le système Debian et tous ses composants seront libres selon
   ces principes et nous aiderons les personnes qui créent et utilisent
   à la fois des travaux libres et non libres sur Debian. Nous ne
   rendrons pas le système dépendant d'un composant non libre.
</pre>

<p>Par conséquent, nous n'inclurons aucun logiciel non libre dans Debian, ni 
dans l'archive principale, ni dans les images officielles de l'installateur,
les images autonomes ou pour le nuage ou autres, et par défaut nous
n'activerons rien qui provienne de <q>non-free</q> ou <q>contrib</q>.</p>

<p>Nous continuerons aussi à soutenir l'esprit du Contrat social de Debian qui
dit dans son cinquième paragraphe :</p>

<pre>
   Travaux non conformes à nos standards sur les logiciels libres

   Nous reconnaissons que certains de nos utilisateurs demandent à pouvoir
   utiliser des travaux qui ne sont pas conformes aux principes du logiciel
   libre selon Debian. Les paquets correspondant prennent place dans des
   sections nommées <q>contrib</q> (<q>contributions</q>) et <q>non-free</q>
   (<q>non libre</q>). Les paquets de ces sections ne font pas partie du
   système Debian, bien qu'ils aient été configurés afin d'être utilisés
   avec lui. Nous encourageons les fabricants de CD à lire les licences de
   ces paquets afin de déterminer s'ils peuvent les distribuer. Ainsi, bien
   que les travaux non libres ne fassent pas partie de Debian, nous prenons
   en compte leur utilisation et fournissons donc l'infrastructure
   nécessaire (à l'image de notre système de suivi des bogues et de nos
   listes de diffusion).
</pre>

<p>Ainsi nous renforçons l'interprétation que tout installateur ou image qui
contient un logiciel non libre ne fait pas partie du système Debian, mais que
nous soutenons leur utilisation et nous invitons d'autres gens à distribuer ce
genre de travail.
</p>

    <vproposere />
    <p>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00038.html'>texte de la proposition</a>]
    </p>
    <vsecondse />
    <ol>
        <li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00039.html'>message</a>]</li>
        <li>Ansgar [<email ansgar@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00040.html'>message</a>]</li>
        <li>Simon Richter [<email sjr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00045.html'>message</a>]</li>
        <li>Kunal Mehta [<email legoktm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00046.html'>message</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00047.html'>message</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00053.html'>message</a>]</li>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00071.html'>message</a>]</li>
    </ol>
    <vtexte />
#<h3>Choix 5 : modification du contrat social pour avoir des microprogrammes non libres dans l'installateur, un seul installateur</h3>

<p>Cette option du scrutin remplace le Contrat social de Debian (un document
fondateur) selon le point 4.1.5 de la constitution et il est donc nécessaire
d'avoir une majorité qualifiée de 3 contre 1.</p>

<p>Le Contrat social de Debian est remplacé par une nouvelle version qui est
identique à la version actuelle à tous égards exceptée qu'elle ajoute la
phrase suivante à la fin du point 5 :</p>

<pre>
    Les supports officiels de Debian peuvent inclure des microprogrammes qui
    autrement ne font pas partie du système Debian pour permettre l'utilisation
    de Debian avec du matériel qui nécessite ce type de microprogrammes.
</pre>

<p>Le projet Debian fait aussi la déclaration suivante dans une publication du
jour :</p>

<p>Nous inclurons les paquets de microprogrammes non libres de la section
<q>non-free-firmware</q> de l'archive Debian dans nos supports officiels (images
de l'installateur et images autonomes). Les binaires des microprogrammes inclus
seront normalement activés par défaut lorsque le système détermine qu'ils sont
requis, mais quand c'est possible, nous inclurons des moyens pour que
l'utilisateur les désactive à l'amorçage (option du menu de démarrage, ligne de
commande du noyau, etc.).</p>

<p>Lors de l'exécution de l'installateur ou du système autonome, nous fournirons
à l'utilisateur des informations sur les microprogrammes chargés (aussi bien les
libres que les non libres), et nous sauvegarderons aussi ces informations sur
le système cible de sorte que l'utilisateur puisse les retrouver plus tard. 
S'il se trouve que des microprogrammes sont nécessaires, le système cible sera
aussi configuré pour utiliser par défaut le composant <q>non-free-firmware</q>
dans le ficher sources.list d'apt. Nos utilisateurs devraient recevoir les
mises à jour de sécurité et les correctifs importants pour les binaires des
microprogrammes exactement comme pour tous les autres logiciels installés.</p>

<p>Nous publierons ces images en tant que médias officiels de Debian, remplaçant
le jeu de médias actuel qui n'inclut pas les paquets de microprogrammes non
libres.</p>

    <vproposerf />
    <p>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00166.html'>texte de la proposition</a>]
    </p>
    <vsecondsf />
    <ol>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00167.html'>message</a>]</li>
        <li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00168.html'>message</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00169.html'>message</a>]</li>
        <li>Étienne Mollier [<email emollier@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00170.html'>message</a>]</li>
        <li>Judit Foglszinger [<email urbec@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00171.html'>message</a>]</li>
        <li>David Prévot [<email taffit@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00173.html'>message</a>]</li>
        <li>Tobias Frost [<email tobi@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00174.html'>message</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00176.html'>message</a>]</li>
        <li>Didier Raboud [<email odyx@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/09/msg00180.html'>message</a>]</li>
    </ol>
    <vtextf />
<h3>Choix 6 : modification du contrat social pour avoir des microprogrammes non libres dans l'installateur, conserver deux installateurs</h3>

<p>Cette option du scrutin remplace le Contrat social de Debian (un document
fondateur) selon le point 4.1.5 de la constitution et il est donc nécessaire
d'avoir une majorité qualifiée de 3 contre 1.</p>

<p>Le Contrat social de Debian est remplacé par une nouvelle version qui est
identique à la version actuelle à tous égards exceptée qu'elle ajoute la
phrase suivante à la fin du point 5 :</p>

<pre>
    Les supports officiels de Debian peuvent inclure des microprogrammes qui
    autrement ne font pas partie du système Debian pour permettre l'utilisation
    de Debian avec du matériel qui nécessite ce type de microprogrammes.
</pre>

<p>Le projet Debian fait aussi la déclaration suivante dans une publication du
jour :</p>

<p>Nous inclurons les paquets de microprogrammes non libres de la section
<q>non-free-firmware</q> de l'archive Debian dans nos supports officiels (images
de l'installateur et images autonomes). Les binaires des microprogrammes inclus
seront normalement activés par défaut lorsque le système détermine qu'ils sont
requis, mais quand c'est possible, nous inclurons des moyens pour que
l'utilisateur les désactive à l'amorçage (option du menu de démarrage, ligne de
commande du noyau, etc.).</p>

<p>Lors de l'exécution de l'installateur ou du système autonome, nous fournirons
à l'utilisateur des informations sur les microprogrammes chargés (aussi bien les
libres que les non libres), et nous sauvegarderons aussi ces informations sur
le système cible de sorte que l'utilisateur puisse les retrouver plus tard. 
S'il se trouve que des microprogrammes sont nécessaires, le système cible sera
aussi configuré pour utiliser par défaut le composant <q>non-free-firmware</q>
dans le ficher sources.list d'apt. Nos utilisateurs devraient recevoir les
mises à jour de sécurité et les correctifs importants pour les binaires des
microprogrammes exactement comme pour tous les autres logiciels installés.</p>

<p>Nous publierons ces images en tant que médias officiels de Debian, en même
temps que le jeu de médias actuel qui n'inclut pas les paquets de microprogrammes non
libres.</p>


    <vquorum />

     <p>
        Avec la liste actuelle des <a href="vote_003_quorum.log">développeurs
         ayant voté</a>, nous avons :
     </p>
    <pre>
#include 'vote_003_quorum.txt'
    </pre>
#include 'vote_003_quorum.src'


    <vstatistics />
    <p>
	Pour cette résolution générale, comme d'habitude,
#        <a href="https://vote.debian.org/~secretary/gr_non_free_firmware/">des statistiques</a>
         des <a href="suppl_003_stats">statistiques</a>
         sur les bulletins et les accusés de réception sont rassemblées
         périodiquement durant la période du scrutin.
         De plus, la liste des <a href="vote_003_voters.txt">votants</a>
         sera enregistrée. La <a href="vote_003_tally.txt">feuille
         de compte</a> pourra être aussi consultée.
         </p>

    <vmajorityreq />
    <p>
      Les propositions 5 et 6 ont besoin d’une majorité qualifiée à 3 contre 1
    </p>
#include 'vote_003_majority.src'

    <voutcome />
#include 'vote_003_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Secrétaire du projet Debian</a>
      </address>

