#use wml::debian::cdimage title="Mídias de instalação para USB, CD, DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e"

<div class="tip">
<p>Se você deseja instalar o Debian e tem uma conexão com a
internet no computador no qual será instalado, recomendamos a mídia de <a
href="netinst/">instalação pela rede</a> que tem um arquivo menor.</p> </div>

<div class="tip">
<p>Nas arquiteturas i386 e amd64, todas as imagens para USB/CD/DVD também podem ser
<a href="https://www.debian.org/CD/faq/#write-usb">usadas em um pendrive USB</a>.</div>

<ul>

  <li><a href="http-ftp/">Baixar as imagens para USB/CD/DVD usando HTTP.</a></li>

  <li><a href="torrent-cd/">Baixar as imagens para USB/CD/DVD com BitTorrent.</a>
  O sistema par-a-par (<q>peer to peer</q>) BitTorrent possibilita que
  vários(as) usuários(as) baixem as imagens de forma cooperativa e
  simultaneamente, com demanda mínima nos nossos servidores.</li>

  <li><a href="live/">Baixar imagens live usando HTTP, FTP ou
  BitTorrent.</a>
  Imagens live são para inicializar um sistema live sem instalar.
  Você pode usar primeiro para testar o Debian e então instalar o
  conteúdo da imagem.</li>

  <li><a href="vendors/">Comprar mídias completas do Debian.</a></li>

  <li><a href="jigdo-cd/">Baixar as imagens para USB/CD/DVD com jigdo.</a>
  Apenas para usuários(as) avançados(as).
  O esquema <q>jigdo</q> permite que você escolha o mais rápido dos 300
  espelhos (<q>mirrors</q>) Debian ao redor do mundo para a sua transferência.
  Ele possui uma seleção fácil de espelhos e <q>atualização</q> de imagens
  antigas para a última versão lançada. Além disso, é a única maneira de
  baixar todas as imagens de DVD do Debian.</li>
  
  <li>Em caso de problemas, por favor verifique o <a href="faq/">FAQ sobre
  CDs/DVDs do Debian</a>.</li>
</ul>

<p>Os USB/CDs/DVDs oficiais lançados são assinados, assim você pode <a
href="verify">verificar que eles são autênticos</a>.</p>

<p>O Debian está disponível para diferentes arquiteturas de computadores
(a maioria das pessoas precisará das imagens para <q>amd64</q>, isto é, para
sistemas compatíveis com 64 bits).</p>

      <div class="cdflash" id="latest">A versão oficial mais recente
      das imagens para USB/CD/DVD da distribuição <q>estável (stable</q>):
        <strong><current-cd-release></strong>.
      </div>

<p>Informações sobre problemas de instalação conhecidos podem ser encontradas
na página de <a href="$(HOME)/releases/stable/debian-installer/">informações
de instalação</a>.<br>
</p>
