#use wml::debian::template title="Líder do projeto Debian" BARETITLE="true"
#use wml::debian::translation-check translation="9f2c3e2f64ffd99731f45de697c0b4c733e68ced"

#include "$(ENGLISHDIR)/devel/leader.data"

<p>O(A) líder do projeto Debian (DPL - Debian Project Leader)
é o(a) representante oficial do projeto Debian. Ele(a) tem duas funções
principais, uma interna e uma externa.</p>

<p>Na função externa, o(a) líder do projeto representa o projeto Debian
perante outros. Isso envolve dar palestras e apresentações sobre o
Debian e participar de exibições, além de construir bons
relacionamentos com outras organizações e companhias.</p>

<p>Internamente, o(a) líder do projeto o gerencia e define sua visão.
Ele(a) deve conversar com outros(as) desenvolvedores(as), especialmente com os(as)
delegados(as), para ver como ele(a) pode ajudar os seus trabalhos. Assim, uma
tarefa principal do(a) líder do projeto envolve coordenação e
comunicação.</p>


<h2>Nomeação</h2>

<p>O(A) líder do projeto é escolhido(a) em uma eleição na qual todos(as) os(as)
desenvolvedores(as) Debian podem votar. O mandato do(a) líder do projeto é de um ano.
Seis semanas antes do posto de liderança ficar vago, o(a) <a href="secretary">\
secretário(a) do projeto</a> inicia uma nova votação. Durante a primeira
semana, qualquer desenvolvedor(a) Debian pode se tornar um(a) candidato(a) para esse
posto se candidatando. As próximas três semanas são usadas para campanha.
Cada candidato(a) envia sua plataforma e todos(as) podem direcionar perguntas para
um(a) ou todos(as) os(as) candidatos(as). As últimas duas semanas consistem no período de
votação durante o qual os(as) desenvolvedores(as) podem dar seus votos.</p>

<p>Mais informações sobre as eleições do(a) líder estão disponíveis nas
<a href="../vote/">páginas de votação</a>.</p>


<h2>Tarefas executadas pelo(a) líder do projeto</h2>

<h3>Nomear delegados(as) ou delegar decisões ao
    <a href="tech-ctte">Comitê Técnico</a></h3>

<p>O(A) líder do projeto pode definir uma área específica de responsabilidade
e delegá-la a um(a) desenvolvedor(a) Debian.</p>

<h3>Delegar autoridade para outros(as) desenvolvedores(as)</h3>

<p>O(A) líder do projeto pode fazer declarações de apoio a pontos de
vista ou a outros(as) membros(as) do projeto.</p>

<h3>Tomar qualquer decisão que precise de ação urgente</h3>

<h3>Tomar qualquer decisão pela qual ninguém mais tenha responsabilidade</h3>

<h3>Consultando desenvolvedores(as), tomar decisões que afetam propriedades guardadas
em confiança para propósitos relacionados ao Debian</h3>

<p>O(A) líder do projeto pode tomar decisões sobre como o dinheiro do Debian
deve ser usado.</p>


<h2>Informações para contato</h2>

<p>Contate o(a) líder do projeto Debian enviando uma mensagem em inglês
para <email "leader@debian.org">.</p>

<h1>Sobre nosso(a) líder atual</h1>

<p>O(A) líder do projeto Debian atual é <current_leader>.</p>

<h2>Líderes passados</h2>

<p>Uma lista completa dos(as) líderes passados pode ser encontrada na
<a href="$(DOC)/manuals/project-history/leaders">história do projeto
Debian</a>.</p>
