#use wml::debian::template title="Mantendo atualizadas as traduções do site web"
#use wml::debian::translation-check translation="8f2dd37edbf6287df4029e3f234798efbcce2862"

<p>Como as páginas web não são estáticas, é uma boa ideia controlar qual
versão do original uma determinada tradução se refere, e usar esta informação
para checar quais páginas foram alteradas desde a última tradução. Esta
informação deve ser adicionada ao início do documento (abaixo de quaisquer
outros cabeçalhos "use") da seguinte forma:

<pre>
\#use wml::debian::translation-check translation="git_commit_hash"
</pre>

<p>onde <var>git_commit_hash</var> é o hash do commit do git que se refere
ao commit do arquivo original (em inglês) que está sendo traduzido pelo arquivo
em questão. Você pode ver os detalhes deste commit específico
usando a ferramenta <code>git show</code>: <code>git show
&lt;git_commit_hash&gt;</code> . Se você usar o script <kbd>copypage.pl</kbd>
no diretório webwml, a linha <code>translation-check</code> será adicionada
automaticamente na primeira versão de sua página traduzida, apontando para a
versão do arquivo original.</p>

<p>Algumas traduções podem não ser atualizadas por bastante tempo, não
acompanhando a atualização do arquivo com o idioma original (inglês). Devido
à negociação de conteúdo, o(a) leitor(a) da página traduzida pode não estar
ciente e perder informações importantes que foram introduzidas em versões novas
do original. O modelo <code>translation-check</code> contém código para
verificar se sua tradução está desatualizada e produz uma mensagem apropriada
alertando o(a) usuário(a) sobre isso.</p>

<p>Também há alguns parâmetros adicionais que você pode usar na linha
<code>translation-check</code>:

<dl>
 <dt><code>original="<var>idioma</var>"</code>
 <dd>onde <var>idioma</var> é o nome do idioma para o qual você está traduzindo,
 caso este não seja o inglês.
 O nome deve corresponder ao subdiretório do topo da hierarquia no VCS e
 ao nome no modelo <code>languages.wml</code>.

 <dt><code>mindelta="<var>número</var>"</code>
 <dd>define a diferença máxima entre as revisões do git para que a tradução
 seja considerada <strong>antiga</strong>. O valor padrão é <var>1</var>.
 Para páginas menos importantes, configure-o para <var>2</var>,
 o que significa que duas alterações precisam ser feitas antes
 que a tradução seja considerada antiga.

 <dt><code>maxdelta="<var>número</var>"</code>
 <dd>define a diferença máxima entre as revisões do git para que a tradução
 seja considerada <strong>desatualizada</strong>. O valor padrão é <var>5</var>.
 Para páginas muito importantes, configure-o para um número menor. O valor
 <var>1</var> significa que cada alteração marcará a tradução como
 desatualizada.
</dl>

<p>O acompanhamento do histórico das traduções permite que nós tenhamos
<a href="stats/">estatísticas de tradução</a>, um relatório de todas as
traduções desatualizadas (com links úteis para as diferenças entre os arquivos),
bem como uma lista de páginas que ainda não foram traduzidas. Este recurso foi
pensado para ajudar os(as) tradutores(as) e para atrair novas pessoas para a
tradução.
</p>

<p>
Para evitar a apresentação de informações muito desatualizadas aos(às)
nossos(as) usuários(as), traduções que não tenham sido atualizadas seis meses
após a página original ter sido alterada serão removidas automaticamente.
Veja a
<a href="https://www.debian.org/devel/website/stats/">lista de traduções
desatualizadas</a> para descobrir quais páginas estão correndo perigo
de serem removidas.
</p>

<p>Adicionalmente, o script <kbd>check_trans.pl</kbd> está disponível no
diretório webwml/, cuja função é mostrar um relatório com as páginas que
precisam de atualizações:

<pre>
check_trans.pl <var>idioma</var>
</pre>

<p>onde <var>idioma</var> é o nome do diretório que contém suas traduções, por
exemplo, "swedish".

<p>Páginas que não possuem traduções serão exibidas como
"<code>Missing <var>nome_do_arquivo</var></code>" (Ausente), e páginas que
não estão atualizadas em relação à página original serão exibidas como
"<code>NeedToUpdate <var>nome_do_arquivo</var> to version <var>x.y</var></code>"
(NecessárioAtualizar...para versão...).

<p>Se você quiser ver quais são exatamente as alterações, você pode
adicionar a opção de linha de comando <kbd>-d</kbd> ao
comando acima.</p>

<p>Se você quiser ignorar os avisos de arquivos ausentes (por exemplo,
notícias antigas), você pode criar um arquivo chamado <code>.transignore</code>
no diretório onde você quer evitar os avisos, listando cada arquivo que não
será traduzido, com um nome por linha.

<p>
Um script similar para manter o controle das traduções das descrições das
listas de discussão também está disponível.
Por favor, leia os comentários no script <code>check_desc_trans.pl</code> para
a documentação.
</p>
