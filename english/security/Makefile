# If this makefile is not generic enough to support a translation,
# please contact debian-www.

WMLBASE=..
CUR_DIR=security
SUBS=oval
# TODO: Check that 'oval' works now that RT #160 (rt.debian.org) has been closed

GETTEXTFILES += security.mo

include $(WMLBASE)/Make.lang

ifndef SUBLANG
INDEXPAGE  := index.$(LANGUAGE).html
else
INDEXPAGE  := $(sort $(foreach i,$(SUBLANG),$(subst index,index.$(LANGUAGE)-$(i),index.html)))
endif

DSADATA       := $(ENGLISHSRCDIR)/$(CUR_DIR)/data/dsa.data $(ENGLISHSRCDIR)/$(CUR_DIR)/data/dsa.fixes
DSARDF     := dsa.rdf
DSALONGRDF := dsa-long.rdf
DESTDSARDF     := $(HTMLDIR)/$(DSARDF)
DESTDSALONGRDF := $(HTMLDIR)/$(DSALONGRDF)

$(DSARDF): $(DSADATA)
ifeq "$(LANGUAGE)" "en"
	./mk-dsa-dla-list -f DSA 30
	mv index.rdf $@
endif

$(DSALONGRDF): $(DSADATA)
ifeq "$(LANGUAGE)" "en"
	./mk-dsa-dla-list -F DSA 30
	mv index.rdf $@
endif

ifeq "$(LANGUAGE)" "en"
install:: $(DESTDSARDF) $(DESTDSALONGRDF)

$(DESTDSARDF): $(HTMLDIR)/%: %
	@test -d $(HTMLDIR) || mkdir -m g+w -p $(HTMLDIR)
	install -m 664 -p $< $(HTMLDIR)
$(DESTDSALONGRDF): $(HTMLDIR)/%: %
	@test -d $(HTMLDIR) || mkdir -m g+w -p $(HTMLDIR)
	install -m 664 -p $< $(HTMLDIR)
endif

$(ENGLISHDIR)/$(CUR_DIR)/data/dsa.data: force
	cd $(ENGLISHDIR)/$(CUR_DIR) && make -C data dsa.data

$(ENGLISHDIR)/$(CUR_DIR)/dsa.list: $(ENGLISHDIR)/$(CUR_DIR)/data/dsa.data
ifeq "$(LANGUAGE)" "en"
	cd $(ENGLISHDIR)/security && ./mk-dsa-dla-list DSA 350 > "$@"
endif

index.$(LANGUAGE).html: index.wml $(ENGLISHDIR)/$(CUR_DIR)/dsa.list $(ENGLISHDIR)/$(CUR_DIR)/security-sources.inc


ifeq "$(LANGUAGE)" "zh"
	@echo -n "Processing $(<F): "
	$(shell echo $(WML) | perl -pe 's,:.zh-(..)\.html,:index.zh-$$1.html,g') \
          $(shell egrep '^-D (CUR_|CHAR)' ../.wmlrc) \
          $(<F)
	@$(GENERATE_ZH_VARIANTS) index html
else
	$(WML) $(<F)
endif

pam-auth.$(LANGUAGE).html: pam-auth.wml \
  $(ENGLISHSRCDIR)/security/pam-auth.wml

faq.$(LANGUAGE).html: faq.wml \
  $(ENGLISHSRCDIR)/security/faq.wml \
  $(ENGLISHSRCDIR)/security/faq.inc $(GETTEXTDEP)

clean::
	rm -f $(DSARDF) $(DSALONGRDF)
	rm -f dsa.list
	cd $(ENGLISHSRCDIR)/$(CUR_DIR)/data && make clean


all:: $(DSARDF) $(DSALONGRDF)

ifeq "$(LANGUAGE)" "en"

map := $(HTMLDIR)/map-dsa.txt

$(map): $(ENGLISHDIR)/$(CUR_DIR)/dsa.list
	./mk-dsa-dla-list -s DSA 90000 | perl -pane '/(DSA-)(\d+)-1/p && print "$$2 $${^POSTMATCH}";s/^DSA-//;' > $(map)

install:: $(map)

cleandest::
	rm -f $(map)

endif

force:
