#use wml::debian::template title="Debian Web Pages TODO List" BARETITLE=true
#use wml::debian::common_tags
#use wml::debian::toc

# Note to translators: there should be no need to translate this file,
# unless you're some sort of masochistic psycho :)

<toc-display/>

<toc-add-entry name="important">Fairly important items</toc-add-entry>

<p>In addition to the items mentioned below, please have a look at the list of
<a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=www.debian.org">open
bugs for the www.debian.org pseudopackage</a>. 

<h3>/sitemap</h3>

  <p>The sitemap needs to be reviewed to see if all the pages are listed there,
  and reorganize the different sections.</p>
  <p>Maybe we should emphasize some major pages by making them
  &lt;strong&gt; or &lt;em&gt;.</p>
  <p>We should agree if we want a link to the sitemap in the home page
  (and the rewritten pages that use similar template), and where (bug 
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=982344">#982344</a>).

<h3>/donations</h3>

  <p>Make a page to credit donations. This idea would need to be discussed.
  Currently significant donations are credited publishing a blog post in 
  <a href="https://bits.debian.org">the Debian blog</a>
  or via a <a href="https://www.debian.org/News">News announcement</a>.</p>

<h3>/ports/</h3>

  <p>All port specific information should be in the port pages.
  More of a long-standing wishlist that can't ever be fixed. :)

  <p>Review the ports listed as unmaintained in the port.maintainers file
  and fix them.

  <p>Import the sh port pages.

<h3>/intro/</h3>

  <p>Writing an intro to something like Debian is not an easy job. You
  really need to give some thought into how you split up all the
  (interconnected) issues involved.</p>
  
  <p>Some pages have been reviewed or rewritten recently (2021) but some 
  others would need review (mainly the "about" page, and have a look at the
  index of that section to see if it can be simplified/reorganized).</p>
  
<h3>/CD/vendors/</h3>

  <p>All the vendors web sites need to be checked to see that they actually
  contribute. They should also be checked after each major release of Debian.</p>
  
  <p>
  Please coordinate this work with the people behind cdvendors@debian.org
  </p>

<h3>/consultants/</h3>

  <p>Consultants should be pinged from time to time to see that they are still
  active and maintain the list current.</p>
  
  <p>
  Please coordinate this work with the people behind consultants@debian.org - 
  you may also want to keep an eye on the 
  <a href="https://lists.debian.org/debian-consultants/">Debian consultants mailing list</a>.
  </p>

<h3>/events/</h3>

  <p>
  Most of the events-related information is written now in wiki pages,
  it would be nice to review all this section together with the Events wiki pages,
  and update accordingly.
  Please coordinate with the <a href="https://wiki.debian.org/Teams/Events">Events team</a>
  and the <a href="https://wiki.debian.org/Teams/LocalGroups">Debian LocalGroups Team</a>
  to do this work.
  </p>
  
<h3>/doc/books</h3>

  <p>Help for maintaining the list of books of Debian is very appreciated.
  Please contact books@debian.org to see how can you help. There are some pending
  bugs: 
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=317140">#317140 Display the license for Debian books</a>,
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=363496">#363496 www.debian.org/doc/books should list books publication year</a>
  </p>
  
<h3>/vote/</h3>

  <p>Figure out if we should keep basic+votebar templates, instead of just
  one template ("votepage" or something). There is a pending bug:
  <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=364913">#364913 - 
  Vote-Pages should be build with more templates (easier translating/maintaining)</a>.
  </p>
  
<h3>/security/ and lts/security</h3>

  <p>A great amount of the Debian web pages are the security advisories.
  It would be nice to think about moving the older ones (more than five years old?),
  that won't get translations or changes anymore,
  to be static files so they are not rebuilt with every template change.
  This idea needs to be discussed together with the Security team.
  </p>

<h3>/intro/organization</h3>

  <p>Collect remaining missing representatives of Debian in other places,
  verify/maintain the current ones.</p>
  
  <p>Periodically check that the list of teams and members is current (particularly, 
  the ones that are not delegated).

<h3>/devel/website/</h3>

  <p>Code all the remaining best current practices.
  
<toc-add-entry name="cn">Content negotiation issues</toc-add-entry>

  <p>The content negotiation system has several flaws that might make some
  people give up on our site. However, we can't do much about this. Most of
  it is caused by clients sending non-RFC strings in the Accept-Language
  header, which makes Apache go bezerk and apply some of its illogical
  methods of serving smallest available files.

  <p>When given "*" in the Accept-Language header, the first available page
  will be served, and that's most likely not English, rather Arabic or
  Chinese. This is especially problematic when the user's language
  preference is a two-part language code (like "en-ca" or "nl-BE") followed
  by a "*".

  <p>Apache 1.3 sticks with the RFC here. Apache 2.0's code will imply a
  en or nl respectively, but it will be low priority so it probably won't
  help with e.g. "en-ca, fr-ca, *".

  <p>Also, when there exists a file of unknown language, i.e. an unrecognized
  foo.*.html file with no AddLanguage or LanguagePriority setting, and when
  the client sends an Accept-Language which contains only unavailable
  languages, the former file will be served.

  <p>This happened most often with the /releases/potato/i386/install page,
  because there's a Slovak variant of it and we didn't have that language
  set up in Apache because there's no web site translation in Slovak.
  We've alleviated the problem by including sk in the Apache config files,
  but as usual, changes don't propagate to all of the mirrors fast.
  
  
<toc-add-entry name="mirroring">Mirroring issues</toc-add-entry>

  <p>Sites are supposed to create the file mirror/timestamps/&lt;host&gt;.
  Jay has made scripts that check the date in this file for each mirror so
  we can know when a mirror is out of date, see mirror/timestamps/*.py.

  <p>We should reduce the number of web mirrors in Europe and increase the
  number of mirrors on other, less connected continents.

  <p>Making all mirrors pushed (from www-master if possible) is also a goal.

  <p>Making sure whether each mirror has correct Apache configuration is a
  pain, but there doesn't seem to be a way around that. Phil Hands suggested
  that the "AddLanguage" stuff is put into a wgettable file and that we make
  a Perl script that would automatically update people's Apache config
  files.

  <P>All links into the archive should allow the user to select the download
  site. The master mirror list could be used to keep the list of mirrors up
  to date (maybe even using a script). See
  <code>webwml/english/mirror/Mirrors.masterlist</code> and
  <code>webwml/english/mirror/mirror_list.pl</code> files.

  <P><em>The mirror list is maintained by the people at mirrors@debian.org.</em>

<toc-add-entry name="wrongurls">Wrong URLs</toc-add-entry>

  <p>Links to external pages have to be checked if they are still
  correct.  James Treacy has written a small Python script for this
  purpose. Frank Lichtenheld is currently
  maintaining the script, the (daily updated) results can be found at
  <url "https://www-master.debian.org/build-logs/urlcheck/" />.
  Broken links have to be removed.  This is more of a permanent task.</p>

<toc-add-entry name="misc">Miscellaneous requests</toc-add-entry>

  <p><strong>Deal with these as you wish.</strong></p>

  <P>If we had cgi.debian.org on a less used and faster host, we could have
  more dynamic content on the web pages.

  <p>Javier suggested making DDP pages account for translations,
  automatically.

  <p>Joey said:
<blockquote>
<p>I'd rather like to see a note on the security pages like:</p>

<p>
  Please note that we cannot guarantee that an intruder gets access to
  our servers since they are connected to the internet.  In such a
  case an evil third party could potential modify uploads to
  security.debian.org and modify web pages containing MD5 sums.  We
  are, however, trying our best to prohibit this.  Please be advised
  that there is no 100% security, only improvements to the current
  situation.
</p>
</blockquote>

<p>Joey should rephrase it probably. :)</p>

  <p>Should be added to /security/faq.</p>

  <p>"Vernon Balbert" &lt;vbalbert@mediaone.net&gt; suggested we make it
  clear which kernel version is used in the latest version of Debian.

  <p>Matti Airas &lt;mairas@iki.fi&gt; suggested "doing language selection
  with Apache mod_rewrite. Instead of having to explicitly go to
  https://www.debian.org/intro/about.en.html (or some mirror), I could go to
  https://www.debian.org/en/intro/about, which mod_rewrite then could replace
  with the correct url." Note that this would require additional hacks in
  order not to break relative links.</p>


  <p>Chris Tillman suggested:</p>

<blockquote>
<p>There is often confusion about which machines
Debian supports and which we don't, yet. There is a bewildering array of
machines, not to mention network cards, displays, mice, video cards, sound
cards, port types, and so forth which an individual system might
mix-and-match. Many of the Ports pages are out of date.

<p>Debian supports a *lot* of systems. I think it would make sense to start
trying to list which ones, specifically. Also, it would be really nice to
know which ones aren't supported, both for new would-be users and for
developers as a todo list.

<p>I think the easiest way to achieve this, would be to provide a web page
where people could enter in their system components, preferably chosen from
a list of known components with an 'other' capability. Then, they could also
enter a thumbs-up or thumbs-down, on Debian on that architecture. Also any
specific problems.

<p>Then after submission of the user's hardware specs, the system can show a
(dated) list of previous user's experiences with those components.

<p>We should also need to sprinkle pointers to this page into the install
documentation, FAQs, probably even put a link on the front Debian page.
</blockquote>

<toc-add-entry name="packages">packages.debian.org</toc-add-entry>

  <p>You can find a current <a
  href="https://salsa.debian.org/webmaster-team/packages/blob/master/TODO">TODO
  list</a> in the <a
  href="https://salsa.debian.org/webmaster-team/packages/">Git
  repository</a>.</p>

  <p><i>The scripts are currently maintained by Frank 'djpig' Lichtenheld and
  Martin 'Joey' Schulze.</i></p>

<toc-add-entry name="bugs">Bug reports</toc-add-entry>

<p><a href="https://bugs.debian.org/www.debian.org">The list of our bug
reports</a>.

<hr>

<p>Please submit anything else to
<a href="mailto:debian-www@lists.debian.org">our mailing list</a>.
