#use wml::debian::template title="آشنایی با دبیان" MAINPAGE="true" FOOTERMAP="true" 
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="Seyed mohammad ali Hosseinifard"
<a id=community></a>
<h2>دبیان جامعه‌ای از افراد است</h2>
<p>هزاران داوطلب در سراسر جهان برای اولویت‌بندی نرم‌افزار آزاد
  و نیازهای کاربران با یکدیگر همکاری می‌کنند.</p>

<ul>
  <li>
    <a href="people">افراد:</a>
    ما که هستیم و چه می‌کنیم
  </li>
  <li>
    <a href="philosophy">فلسفه ما:</a>
    چرا و چگونه این کار را انجام می‌دهیم
  </li>
  <li>
    <a href="../devel/join/">مشارکت کنید:</a>
    شما هم می‌توانید بخشی از ما باشید!
  </li>
  <li>
    <a href="help">چگونه می‌توانید به دبیان کمک کنید؟</a>
  </li>
  <li>
    <a href="../social_contract">قرارداد اجتماعی:</a>
    دستور کار اخلاقی ما
  </li>
  <li>
    <a href="diversity">بیانیه تنوع</a>
  </li>
  <li>
    <a href="../code_of_conduct">مرام‌نامه</a>
  </li>
  <li>
    <a href="../partners/">همکاران:</a>
    شرکت‌ها و سازمان‌هایی که به طور مداوم از پروژه دبیان حمایت می‌کنند.
  </li>
  <li>
    <a href="../donations">کمک‌های مالی</a>
  </li>
  <li>
    <a href="../legal/">اطلاعات حقوقی</a>
  </li>
  <li>
    <a href="../legal/privacy">حریم خصوصی داده‌ها</a>
  </li>
  <li>
    <a href="../contact">تماس با ما</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>دبیان یک سیستم‌عامل آزاد است</h2>
<p>ما کار خود را با لینوکس آغاز کردیم و هزاران برنامه کاربردی را برای
  پاسخگویی به نیازهای کاربران به آن افزودیم.</p>

<ul>
  <li>
    <a href="../distrib">بارگیری:</a>
    انواع بیشتری از تصاویر دبیان
  </li>
  <li>
  <a href="why_debian">چرا دبیان</a>
  </li>
  <li>
    <a href="../support">پشتیبانی:</a>
    کمک بگیرید
  </li>
  <li>
    <a href="../security">امنیت:</a>
    آخرین به‌روزرسانی <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages">بسته‌های نرم‌افزاری:</a>
    فهرست طولانی نرم‌افزارهای ما را جستجو کرده و مرور کنید
  </li>
  <li>
    <a href="../doc">مستندات</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">ویکی دبیان</a>
  </li>
  <li>
    <a href="../Bugs">گزارشات اشکال</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">فهرست‌های پستی</a>
  </li>
  <li>
    <a href="../blends">ترکیبات منحصر به فرد:</a>
    بسته‌های فرعی برای نیازهای خاص
  </li>
  <li>
    <a href="../devel">کُنج توسعه‌دهندگان:</a>
    اطلاعاتی که در درجه اول مورد توجه توسعه‌دهندگان دبیان می‌باشد
  </li>
  <li>
    <a href="../ports">پورت‌ها/معماری‌ها:</a>
    معماری‌های پردازنده‌ای که ما پشتیبانی می‌کنیم
  </li>
  <li>
    <a href="search">اطلاعاتی درمورد نحوه استفاده از موتور جستجوی دبیان</a>.
  </li>
  <li>
    <a href="cn">اطلاعاتی درمورد صفحات چند زبانه</a>.
  </li>
</ul>
