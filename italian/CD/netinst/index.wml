#use wml::debian::cdimage title="Installazione via rete da USB o CD minimale" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="678a572e92767d98d57f67822e00905782bf9637" maintainer="Luca Monducci"

<div class="tip">
<p>Sulle architetture i386 e amd64, tutte le immagini USB/CD/DVD possono
essere <a href="https://www.debian.org/CD/faq/#write-usb">utilizzate anche
su una chiavetta USB</a>.</p>
</div>

<p>Un CD per l'<q>installazione via rete</q> o <q>netinst</q> è un
semplice CD che permette di installare l'intero sistema operativo.
Questo CD contiene la minima quantità di software che permette di
installare il sistema di base e scaricare i pacchetti rimanenti da
Internet.</p>

<p><strong>Quali tipi di connessione alla rete sono supportati durante
l'installazione?</strong>
È ovvio che per l'installazione via rete è necessaria la disponibilit&agrave;
di un collegamento a Internet. Sono supportate diverse modalità di
collegamento tra le quali Ethernet e WLAN
(con qualche limitazione).</p>

<p>Sono disponibili per il download le seguenti immagini avviabili
di CD minimali:</p>

<ul>
  <li>Immagini ufficiali <q>netinst</q> per il rilascio <q>stable</q>,
  <a href="#netinst-stable">vedere sotto</a>.</li>

  <li>Immagini per il rilascio <q>testing</q> vedere la
  <a href="$(DEVEL)/debian-installer/">pagina dell'installatore
  Debian</a>.</li>
</ul>


<h2 id="netinst-stable">Immagini ufficiali <q>netinst</q> per il
rilascio <q>stable</q></h2>

<p>Questa immagine contiene
l'installatore e una ridotta selezione di pacchetti che permettono
l'installazione di un sistema (molto) minimale.</p>

<div class="line">
<div class="item col50">
<p><strong>Immagine del CD netinst</strong></p>
  <stable-netinst-images />
</div>
<div class="item col50 lastcol">
<p><strong>
Immagine del CD netinst (via <a href="$(HOME)/CD/torrent-cd">bittorrent</a>)
</strong></p>
  <stable-netinst-torrent />
</div>
<div class="clear"></div>
</div>

<p>Le informazioni su questi file e su come usarli sono contenute
nelle <a href="../faq/">FAQ</a>.</p>

<p>Dopo aver scaricato le immagini, si consiglia di leggere le
<a href="$(HOME)/releases/stable/installmanual">istruzioni dettagliate
di installazione</a>.</p>
