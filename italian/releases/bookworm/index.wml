#use wml::debian::template title="Informazioni sul rilascio di Debian &ldquo;bookworm&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="77c9da4860132027cb6546223ff0e9b84071a5b7" maintainer="Luca Monducci"


<p>Debian GNU/Linux <current_release_bookworm> è stata rilasciata il
<a href="$(HOME)/News/<current_release_newsurl_bookworm/>">
<current_release_date_bookworm></a>.
<ifneq "12.0" "<current_release>"
	"Il rilascio iniziale di Debian 12.0 fu fatto il <:=spokendate('2023-06-10'):>."
/>
Questo rilascio contiene importanti cambiamenti descritti
nel <a href="$(HOME)/News/2023/20230610">comunicato stampa</a> e
nelle <a href="releasenotes">Note di rilascio</a>.</p>

# <p><strong>Debian 12 è stata sostituita da
# <a href="../bookworm/">Debian 13 (<q>trixie</q>)</a>.
# </strong></p>

### This paragraph is orientative, please review before publishing!
# <p><strong>Nonostante bookworm benefici anche del Supporto a Lungo Termine
# (LTS Long Term Support) fino al 30 giugno 2028. Tale supporto è limitato alle
# architetture i386, amd64, armel, armhf e arm64; tutte le altre architetture
# non hanno supporto. Per ulteriori informazioni fare riferimento alla <a
# href="https://wiki.debian.org/LTS">sezione LTS del Wiki Debian</a>.
# </strong></p>

<p>
Il ciclo di vita di Debian 12 è di cinque anni: i primi 3 anni con supporto
Debian completo, fino al <:=spokendate('2026-06-10'):>, seguiti da altri 2
anni con supporto LTS, fino al <:=spokendate('2028-06-30'):>. L'insieme delle
architetture supportate si riduce durante il periodo LTS. Per ulteriori
informazioni, consultare la pagina web delle <a href="$(HOME)/security/">informazioni
sulla sicurezza</a> e la <a href="https://wiki.debian.org/LTS">sezione LTS
del Wiki Debian</a>.
</p>

<p>Per ottenere e installare Debian, si veda la pagina
con le <a href="debian-installer/">informazioni sull'installazione</a> e
la <a href="installmanual">Guida all'installazione</a>. Per aggiornare
da un precedente rilascio di Debian, consultare le
<a href="releasenotes">Note di rilascio</a>.</p>

### Activate the following when LTS period starts.
#<p>Architetture gestite nel periodo Long Term Support:</p>
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Architetture supportate al momento del rilascio iniziale di bookworm:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Nonostante la nostra volontà, questo rilascio potrebbe avere problemi,
anche se è chiamato <em>stable</em>. Esiste un <a href="errata">elenco
dei principali problemi conosciuti</a>, ed è possibile <a
href="../reportingbugs">segnalare altri problemi</a>.</p>

<p>Infine, ma non meno importante, è presente un elenco di
<a href="credits">persone da ringraziare</a> per aver permesso questo
rilascio.</p>
