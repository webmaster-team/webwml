<define-tag pagetitle>Rilascio di Debian Installer Trixie Alpha 1</define-tag>
<define-tag release_date>2024-12-31</define-tag>
#use wml::debian::translation-check translation="7f67ce7f56845c84ba736aeea2464ff14d3b0649" maintainer="Giuseppe Sacco"
#use wml::debian::news

<p>
Il <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> Debian Installer
è felice di annunciare il primo rilascio alpha della procedura di installazione
per Debian 13 <q>Trixie</q>.
</p>


<h2>Premessa</h2>

Cyril Brulebois vorrebbe ringraziare in special modo Holger Wansing che,
oltre a coordinare le traduzioni,
ha portato avanti un importante lavoro di selezione di proposte di
modifica nei vari componenti della procedura di installazione.


<h2>Modifiche importanti per questo rilascio</h2>

<p>
Durante questo ciclo di rilascio sono state fatte molte modifiche e
questo annuncio non ha lo scopo di essere esaustivo. Invece viene
presentata una descrizione di alto livello per i cambiamenti maggiori.
</p>

<p>
Questi sono i maggiori aggiornamenti sul fronte del supporto hardware:
</p>

<ul>
  <li>Non produciamo più l'installatore per le architetture armel e i386
      anche se queste sono ancora parte dell'archivio.</li>
  <li>L'architettura mipsel è stata rimossa dall'archivio l'anno scorso.</li>
  <li>L'architettura riscv64 è nuova fiammante!</li>
</ul>

<p>
Anche se le schermate di avvio vanno ancora aggiornate, il tema Ceratopsian
di Elise Couper sta facendo il debutto nell'installatore.
</p>

<p>
Le schermate per l'impostazione dell'utente (che gestiscono la creazione
dell'utente root e del primo utente) sono state riviste come meritavano
da tempo.
</p>

<p>
Molte miglorie e soluzioni di problemi sono state apportate a componenti
relativi al partizionamento. Questo include varie auristiche per il
partizionamento automatico (ad esempio, la scelta della dimensione
dello swap), alcune delle quali specifiche per dischi di dimensione
ridotta, ecc.
</p>


<h2>Stato delle localizzazioni</h2>

<ul>
  <li>In questo rilascio sono supportate 78 lingue.</li>
  <li>La traduzione è completa per 18 di esse.</li>
</ul>


<h2>Problemi conosciuti di questo rilascio</h2>

<p>
Vedere l'<a href="$(DEVEL)/debian-installer/errata">errata</a> per
dettagli e l'elenco completo dei problemi conosciuti.
</p>


<h2>Feedback per questo rilascio</h2>

<p>
Ci serve aiuto per trovare problemi e migliorare ulteriormente l'installatore,
quindi collaudatelo. I CD dell'installatore, altri supporti e qualunque altra
cosa possa servire è disponibile sul nostro <a href="$(DEVEL)/debian-installer">sito web</a>.
</p>


<h2>Ringraziamenti</h2>

<p>
Il team Debian Installer ringrazia tutti quelli che hanno contribuito a
questo rilascio.
</p>
