#use wml::debian::template title="Informazioni sulle votazioni Debian" BARETITLE="true" NOHEADER="true" NOHOMELINK="true"
#use wml::debian::votebar
#use wml::debian::translation-check translation="a595ef278c9dec8c13fa8f350492b98ec20af3d0"

<h1 class="title">Informazioni sulle votazioni Debian</h1>

<p>
Il Progetto Debian ha un sistema di tracciamento delle votazioni (DEbian
VOTe EnginE [<a
href="https://vote.debian.org/~secretary/devotee.git/">devotee</a>]) che
fornisce lo stato delle risoluzioni generali in corso e i risultati delle
votazioni precedenti.
</p>

<p>
Lo stato delle Risoluzioni Generali in corso include la proposta e
l'elenco dei sostenitori, tutte le date importanti e le condizioni
necessarie per l'approvazione. Naturalmente, lo stato includerà anche
uno dei seguenti elementi:
</p>

<ul>
<li>Proposta - In attesa dei sostenitori.</li>
<li>Discussione - In un periodo di discussione di almeno due settimane.</li>
<li>Votazione - Il processo di votazione è in corso.</li>
<li>Chiuso - La votazione è terminata.</li>
</ul>

<p>
I risultati delle votazioni precedenti (risoluzioni chiuse) comprendono
l'esito e l'elenco di tutte le persone che hanno votato e il loro voto.
È disponibile anche il testo di ogni voto inviato, a meno che non si
tratti di una voto segreto.
</p>

<p>
Debian utilizza il <a
href="https://en.wikipedia.org/wiki/Condorcet_method">metodo Condorcet</a>
per le elezioni dei leader di progetto (l'articolo di wikipedia a cui si
fa riferimento è piuttosto informativo). Semplicisticamente, il metodo di
Condorcet può essere enunciato come segue
</p>

<blockquote>
<p>
Consideriamo tutti i possibili confronti a due tra candidati. Il vincitore
di Condorcet, se esiste, è il candidato che può battere ogni altro
candidato in un confronto a due con quel candidato.
</p>
</blockquote>

<p>
Il problema è che in elezioni complesse può esistere una relazione circolare
in cui A batte B, B batte C e C batte A. La differenza più importante tra
le varianti di Condorcet è proprio la modalità utilizza per risolvere il
pareggio. Per maggiori dettagli, si veda la voce <a
href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">Cloneproof
Schwartz Sequential Dropping</a>. La variante di Debian è descritta <a
href="$(HOME)/devel/constitution">nella Costituzione</a>, in particolare
nel § A.5.
</p>

<p>
Per maggiori informazioni su come leggere la matrice dei risultati, che
viene pubblicata come risultato delle votazioni, si può consultare questo
<a href="https://en.wikipedia.org/wiki/Schwartz_method">esempio</a>.
</p>
