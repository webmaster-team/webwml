#use wml::debian::translation-check translation="ca2375b4ef2eebfd1c7fb93df901295ca6ea6586" maintainer="Giuseppe Sacco"
# Status: published
# $Id$
# $Rev$
<define-tag pagetitle>Il supporto a lungo termine di Debian 10 arriva a fine ciclo</define-tag>
<define-tag release_date>2024-06-15</define-tag>
#use wml::debian::news

<p>
Il gruppo di Debian del supporto a lungo termine (Long Term Support, LTS)
annuncia che il supporto per Debian 10 <q>buster</q> arriverà a fine ciclo
il 30 giugno 2024, dopo quasi cinque anni dal rilascio iniziale del 6
luglio 2019.
</p>

<p>
Con l'inizio di luglio Debian non fornirà ulteriori aggiornamenti per la
sicurezza di Debian 10. Una parte dei pacchetti di <q>buster</q> sarà
supportata da parti esterne. Informazioni dettagliate possono essere
trovate su
<a href="https://wiki.debian.org/LTS/Extended">LTS Esteso</a>.
</p>

<p>
Il gruppo Debian LTS comincierà a prepararsi per la transizione di Debian 11
<q>bullseye</q>, l'attuale versione oldstable. Grazie agli sforzi combinati di
vari gruppi tra i quali quello della sicurezza, quello dei rilasci e quello
LTS, il ciclo di Debian 11 sarà anch'esso di cinque anni.
Perché il ciclo di vita dei rilasci di Debian sia più facile da seguire, i
vari gruppi Debian coinvolti hanno raggiunto un accordo sulla seguente
pianificazione: tre anni di normale supporto più due di supporto a lungo termine.
Il gruppo LTS prenderà le redini del supporto dai gruppi di sicurezza e
del rilascio il 14 agosto 2024, tre anni dopo l'iniziale rilascio del 14
agosto 2021. Il rilascio minore finale di <q>bullseye</q> sarà pubblicato
subito dopo che l'ultimo bollettiso di sicurezza (DSA) di Debian 11 sarà
reso pubblico.
</p>

<p>
Debian 11 riceverà il supporto a lungo termine fino al 31 agosto 2026.
Le architetture supportate rimangono amd64, i386, arm64 e armhf.
</p>

<p>
Per ulteriori informazioni sull'utilizzo di <q>bullseye</q> con LTS e per
l'aggiornamento da <q>buster</q> con LTS, fare riferimento a
<a href="https://wiki.debian.org/LTS/Using">LTS/Utilizzo</a>.
</p>

<p>
Debian e il suo gruppo LTS vogliono ringraziare tutti gli utenti, sviluppatori,
sponsor che hanno contribuito assieme ad altri gruppi Debian a rendere possibile
l'estensione dei precedenti rilasci.
</p>

<p>
Se si utilizza Debian LTS si può considerare di
<a href="https://wiki.debian.org/LTS/Development">unirsi al gruppo</a>,
fornire patch, effettuare il test o
<a href="https://wiki.debian.org/LTS/Funding">finanziare lo sforzo</a>.
</p>


<h2>Su Debian</h2>

<p>
Il Progetto Debian è stato fondato nel 1993 con il fine di essere un
progetto di una comunità veramente libera. Da lì, il progetto è cresciuto
fino a diventare uno dei più vasti e influenti progetti open source.
Migliaia di volontari da tutto il mondo collaborano al fine di creare e di
mantenere il software Debian. Disponibile in 70 lingue e con una vasta gamma
di architetture supportate, Debian si definisce il
<q>sistema operativo universale</q>.
</p>

<h2>Come contattarci</h2>

<p>Per maggiori informazioni visitare le pagine web su
<a href="$(HOME)/">https://www.debian.org/</a> o mandare un email a
&lt;press@debian.org&gt;.</p>

<h2>Iscrizione / Annullamento</h2>
<p>
Per <a href="https://lists.debian.org/debian-announce/">iscriversi o annullare l'iscrizione</a>
dalla lista di messaggi Debian Announcements.
</p>
