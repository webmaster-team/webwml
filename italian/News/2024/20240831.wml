#use wml::debian::translation-check translation="f93b89c74962ae015feaa58f013cef8f7ec08dd9" maintainer="Ceppo"
<define-tag pagetitle>Aggiornato Debian 12: rilasciato 12.7</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Il progetto Debian è lieto di annunciare il settimo aggiornamento della sua
distribuzione stable Debian <release> (nome in codice <q><codename></q>).
Questa point release aggiunge principalmente correzioni per problemi di
sicurezza, insieme ad alcune modifiche per problemi gravi. Gli avvisi di
sicurezza sono già stati pubblicati separatamente e si rinvia ad essi, se
disponibili.
</p>

<p>
Si noti che la point release non costituisce una nuova versione di Debian
<release>, semplicemente aggiorna alcuni dei pacchetti inclusi. Non è
necessario buttare i vecchi supporti di <q><codename></q>. Dopo
l'installazione, i pacchetti possono essere aggiornati alla versione attuale
utilizzando un mirror aggiornato di Debian.
</p>

<p>
Coloro che installano frequentemente gli aggiornamenti da security.debian.org
non dovranno aggiornare molti pacchetti e la maggior parte di tali
aggiornamenti è inclusa nella point release.
</p>

<p>
Le nuove immagini d'installazione saranno presto disponibili nelle posizioni
abituali.
</p>

<p>
L'aggiornamento di un'installazione esistente a questa revisione può essere
effettuato puntando il sistema di gestione dei pacchetti a uno dei molti mirror
HTTP di Debian. Una lista completa dei mirror è disponibile qui:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Secure Boot e altri sistemi operativi</h2>

<p>
Gli utenti che avviano altri sistemi operativi sullo stesso hardware e che
hanno attivato Secure Boot devono tenere conto che shim 15.8 (incluso in Debian
<revision>) revoca le firme delle vecchie versioni di shim nel firmware UEFI.
Questo potrebbe rendere impossibile avviare altri sistemi operativi che usano
versioni di shim precedenti la 15.8.
</p>

<p>
Gli utenti colpiti possono disabilitare temporaneamente Secure Boot prima di
aggiornare gli altri sistemi operativi.
</p>


<h2>Correzioni di bug varie</h2>

<p>Questo aggiornamento stable aggiunge alcune correzioni importanti ai
pacchetti seguenti:
</p>

<table border=0>
<tr><th>Pacchetto</th>               <th>Motivo</th></tr>
<correction amd64-microcode "Nuovo rilascio originale; correzioni di sicurezza [CVE-2023-31315]; correzioni al firmware SEV [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "Nuovo rilascio stable originale; correzione di un problema di rivelazione delle chiavi [CVE-2023-4237]">
<correction ansible-core "Nuovo rilascio stable originale; correzione di un problema di rivelazione di informazioni [CVE-2024-0690]; correzione di un problema di iniezione dei modelli [CVE-2023-5764]; correzione di un problema di path traversal [CVE-2023-5115]">
<correction apache2 "Nuovo rilascio stable originale; correzione di un problema di rivelazione dei contenuti [CVE-2024-40725]">
<correction base-files "Aggiornamento per la point release">
<correction cacti "Correzione di problemi di esecuzione di codice remoto [CVE-2024-25641 CVE-2024-31459], di cross-site scripting [CVE-2024-29894 CVE-2024-31443 CVE-2024-31444], di iniezione SQL [CVE-2024-31445 CVE-2024-31458 CVE-2024-31460], di conversione di tipo [CVE-2024-34340]; correzione del fallimento di autopkgtest">
<correction calamares-settings-debian "Correzione di un problema di permessi nel lanciatore di Xfce">
<correction calibre "Correzione di problemi di esecuzione di codice remoto [CVE-2024-6782], di cross site scripting [CVE-2024-7008], di iniezione SQL [CVE-2024-7009]">
<correction choose-mirror "Aggiornamento della lista dei mirror disponibili">
<correction cockpit "Correzione di un problema di diniego del servizio [CVE-2024-6126]">
<correction cups "Correzione di problemi di gestione di socket di dominio [CVE-2024-35235]">
<correction curl "Correzione di un problema di sovraccarico dell'analizzatore di date ASN.1 [CVE-2024-7264]">
<correction cyrus-imapd "Correzione di una regressione introdotta CVE-2024-34055">
<correction dcm2niix "Correzione di un potenziale problema di esecuzione di codice [CVE-2024-27629]">
<correction debian-installer "Incremento dell'ABI del kernel Linux a 6.1.0-25; ricompilazione da proposed-updates">
<correction debian-installer-netboot-images "Ricompilazione da proposed-updates">
<correction dmitry "Correzioni di sicurezza [CVE-2024-31837 CVE-2020-14931 CVE-2017-7938]">
<correction dropbear "Correzione del comportamento <q>noremotetcp</q> dei pacchetti keepalive in combinazione con la restrizione <q>no-port-forwarding</q> di authorized_keys(5)">
<correction gettext.js "Correzione di un problema di falsificazione delle richieste dal lato server [CVE-2024-43370]">
<correction glibc "Correzione della liberazione di memoria non inizializzata in libc_freeres_fn(); correzione di vari problemi di prestazioni e possibili crash">
<correction glogic "Dipendenza da Gtk 3.0 e PangoCairo 1.0">
<correction graphviz "Correzione di una scala errata">
<correction gtk+2.0 "Non cercare moduli nella directory di lavoro attuale [CVE-2024-6655]">
<correction gtk+3.0 "Non cercare moduli nella directory di lavoro attuale [CVE-2024-6655]">
<correction imagemagick "Correzione di un errore di segmentazione; correzione di una correzione incompleta per CVE-2023-34151">
<correction initramfs-tools "hook_functions: correzione di copy_file con un'origine che include un collegamento simbolico a una directory; hook-functions: copy_file: normalizzazione del nome del file di destinazione; installazione del modulo hid-multitouch module per tastiere Surface Pro 4; aggiunta del modulo hyper-keyboard, necessario per inserire la password di LUKS in Hyper-V; auto_add_modules: aggiunta di onboard_usb_hub e onboard_usb_dev">
<correction intel-microcode "Nuovo rilascio originale; correzioni di sicurezza [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction ipmitool "Aggiunta del file mancante enterprise-numbers.txt">
<correction libapache2-mod-auth-openidc "Prevenzione del crash quando l'intestazione Forwarded non è presente ma OIDCXForwardedHeaders è configurato per essa">
<correction libnvme "Correzione di un buffer overflow durante la scansione di dispositivi che non supportano la lettura sub-4k">
<correction libvirt "birsh: reso domif-setlink funzionale più di una volta; qemu: domain: correzione della logica nell'esecuzion del taint del dominio; correzione di problemi di diniego del servizio [CVE-2023-3750 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction linux "Nuovo rilascio originale; incremento dell'ABI a 25">
<correction linux-signed-amd64 "Nuovo rilascio originale; incremento dell'ABI a 25">
<correction linux-signed-arm64 "Nuovo rilascio originale; incremento dell'ABI a 25">
<correction linux-signed-i386 "Nuovo rilascio originale; incremento dell'ABI a 25">
<correction newlib "Correzione di un buffer overflow [CVE-2021-3420]">
<correction numpy "Conflitto con python-numpy">
<correction openssl "Nuovo rilascio stable originale; correzione di problemi di diniego del servizio [CVE-2024-2511 CVE-2024-4603]; correzione di un problema use-after-free [CVE-2024-4741]">
<correction poe.app "Rese modificabili le celle di commento; correzione del disegno dell'interfaccia quando una NSActionCell nelle preferenze è azionata per modificarne lo stato">
<correction putty "Correzione della generazione debole di nonce ECDSA che consentiva il recupero della chiave privata [CVE-2024-31497]">
<correction qemu "Nuovo rilascio stable originale; correzione di un problema di diniego del servizio [CVE-2024-4467]">
<correction riemann-c-client "Prevenzione di payload malformati nelle operazioni di invio/ricezione in GnuTLS">
<correction rustc-web "Nuovo rilascio stable originale, per supportare la compilazione di nuove versioni di chromium firefox-esr">
<correction shim "Nuovo rilascio originale">
<correction shim-helpers-amd64-signed "Ricompilazione con shim 15.8.1">
<correction shim-helpers-arm64-signed "Ricompilazione con shim 15.8.1">
<correction shim-helpers-i386-signed "Ricompilazione con shim 15.8.1">
<correction shim-signed "Nuovo rilascio stable originale">
<correction systemd "Nuovo rilascio stable originale; aggiornamento di hwdb">
<correction usb.ids "Aggiornamento dell'elenco dei dati inclusi">
<correction xmedcon "Correzione di un buffer overflow [CVE-2024-29421]">
</table>


<h2>Aggiornamenti di sicurezza</h2>

<p>
Questa revisione aggiunge i seguenti aggiornamenti di sicurezza al rilascio
stable. Il Security Team ha già rilasciato un avviso per ciascuno di questi
aggiornamenti:
</p>

<table border=0>
<tr><th>ID avviso</th>  <th>Pacchetto</th></tr>
<dsa 2024 5617 chromium>
<dsa 2024 5629 chromium>
<dsa 2024 5634 chromium>
<dsa 2024 5636 chromium>
<dsa 2024 5639 chromium>
<dsa 2024 5648 chromium>
<dsa 2024 5654 chromium>
<dsa 2024 5656 chromium>
<dsa 2024 5668 chromium>
<dsa 2024 5675 chromium>
<dsa 2024 5676 chromium>
<dsa 2024 5683 chromium>
<dsa 2024 5687 chromium>
<dsa 2024 5689 chromium>
<dsa 2024 5694 chromium>
<dsa 2024 5696 chromium>
<dsa 2024 5697 chromium>
<dsa 2024 5701 chromium>
<dsa 2024 5710 chromium>
<dsa 2024 5716 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5720 chromium>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5724 openssh>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5731 linux-signed-amd64>
<dsa 2024 5731 linux-signed-arm64>
<dsa 2024 5731 linux-signed-i386>
<dsa 2024 5731 linux>
<dsa 2024 5732 chromium>
<dsa 2024 5734 bind9>
<dsa 2024 5735 chromium>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5741 chromium>
<dsa 2024 5743 roundcube>
<dsa 2024 5745 postgresql-15>
<dsa 2024 5748 ffmpeg>
<dsa 2024 5749 bubblewrap>
<dsa 2024 5749 flatpak>
<dsa 2024 5750 python-asyncssh>
<dsa 2024 5751 squid>
<dsa 2024 5752 dovecot>
<dsa 2024 5753 aom>
<dsa 2024 5754 cinder>
<dsa 2024 5755 glance>
<dsa 2024 5756 nova>
<dsa 2024 5757 chromium>
</table>


<h2>Pacchetti rimossi</h2>

<p>
I pacchetti seguenti sono stati rimossi per circostanze al di fuori del nostro
controllo:
</p>

<table border=0>
<tr><th>Pacchetto</th>               <th>Motivo</th></tr>
<correction bcachefs-tools "Difettoso; obsoleto">

</table>

<h2>Installatore di Debian</h2>

<p>
L'installatore è stato aggiornato per includere le correzioni incorporate in
stable dalla point release.
</p>

<h2>URL</h2>

<p>
Elenco completo dei pacchetti che sono cambiati con questa revisione:
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribuzione stable attuale:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Aggiornamenti proposti per la distribuzione stable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informazioni sulla distribuzione stable (note di rilascio, errata ecc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Annunci di sicurezza e informazioni:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>A proposito di Debian</h2>

<p>
Il Progetto Debian è un'associazione di sviluppatori di software libero che
dedicano volontariamente il proprio tempo e il proprio sforzo per produrre il
sistema operativo interamente libero Debian.
</p>

<h2>Informazioni di contatto</h2>

<p>
Per maggiori informazioni, visitare il sito web di Debian presso <a
href="$(HOME)/">https://www.debian.org/</a>, inviare una mail a
&lt;press@debian.org&gt; o contattare il release team a
&lt;debian-release@lists.debian.org&gt;.
</p>
