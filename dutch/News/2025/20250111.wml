#use wml::debian::translation-check translation="c0534ce78eaba4104b9c0e724765ee740ea24c41"
<define-tag pagetitle>Debian 12 is bijgewerkt: 12.9 werd uitgebracht</define-tag>
<define-tag release_date>2025-01-11</define-tag>
#use wml::debian::news

<define-tag release>12</define-tag>
<define-tag codename>bookworm</define-tag>
<define-tag revision>12.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de negende update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.
</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.
</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van stable, de stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction allow-html-temp "Update met het oog op compatibiliteit met Thunderbird 128">
<correction ansible-core "Nieuwe bovenstroomse stabiele release; probleem met willekeurige code-uitvoering oplossen [CVE-2024-11079]; probleem met openbaarmaking van informatie oplossen [CVE-2024-8775]; probleem met overschrijven van bestand oplossen [CVE-2024-9902]; testfout oplossen">
<correction audiofile "Probleem van null pointer dereference oplossen [CVE-2019-13147]; probleem met lekken van informatie oplossen [CVE-2022-24599]">
<correction avahi "Problemen van denial of service oplossen [CVE-2023-38469 CVE-2023-38470 CVE-2023-38471 CVE-2023-38472 CVE-2023-38473]; browsen herstellen wanneer ongeldige services aanwezig zijn">
<correction base-files "Update voor de tussenrelease">
<correction bochs "BIOS-images bouwen voor i386 CPU's">
<correction cpuinfo "Testfouten tijdens de bouw niet fataal laten zijn">
<correction criu "Dynamisch omgaan met een libc die op het moment van uitvoeren verschilt van die bij het compileren">
<correction debian-installer "Linux kernel ABI verhogen naar 6.1.0-29; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-security-support "Lijst van pakketten die in bookworm een beperkte ondersteuning krijgen, bijwerken">
<correction debootstrap "usr-is-merged niet binnenhalen in trixie/sid">
<correction dnsmasq "Problemen met denial-of-service oplossen [CVE-2023-50387 CVE-2023-50868]; standaard maximum EDNS.0 UDP pakketgrootte instellen op 1232 [CVE-2023-28450]">
<correction eas4tbsync "Update met het oog op compatibiliteit met Thunderbird 128">
<correction espeak-ng "Oplossing voor het probleem waarbij de laatste byte van stdin-invoer wordt weggelaten">
<correction geoclue-2.0 "beaconDB gebruiken in plaats van het voormalige Mozilla Location Service">
<correction glib2.0 "Oplossing voor  een bufferoverloop bij een zeer lange gebruikersnaam als in de configuratie bepaald is om een ​​SOCKS4a-proxy te gebruiken [CVE-2024-52533]">
<correction gnuchess "Probleem met willekeurige code-uitvoering oplossen [CVE-2021-30184]">
<correction grml-rescueboot "Ondersteunde architecturen updaten van amd64/i386 naar arm64/amd64">
<correction gsl "Bufferoverloop bij het berekenen van de kwantielwaarde oplossen [CVE-2020-35357]">
<correction gst-plugins-base1.0 "Niet proberen uitgebreide header te ontleden als er niet genoeg gegevens beschikbaar zijn (id3v2) [CVE-2024-47542]">
<correction gunicorn "Smokkelen van HTTP-verzoeken voorkomen [CVE-2024-1135]">
<correction icinga2 "Omzeilen van TLS-certificaat voorkomen [CVE-2024-49369]">
<correction intel-microcode "Nieuwe bovenstroomse veiligheidsrelease [CVE-2024-21853 CVE-2024-23918 CVE-2024-24968 CVE-2024-23984]">
<correction jinja2 "HTML attribuut-injectie voorkomen [CVE-2024-22195 CVE-2024-34064]">
<correction lemonldap-ng "Oplossing voor probleem van rechtenuitbreiding wanneer adaptieve authenticatieniveaus worden gebruikt [CVE-2024-52946]; XSS in upgrade-plugin oplossen [CVE-2024-52947]">
<correction libebml "Probleem van bufferoverloop oplossen [CVE-2023-52339]">
<correction libpgjava "Probleem van SQL-injectie oplossen [CVE-2024-1597]">
<correction libsoup2.4 "Smokkelen van HTTP-verzoeken voorkomen [CVE-2024-52530]; oplossing voor bufferoverloop in soup_header_parse_param_list_strict [CVE-2024-52531]; DoS-lezen van WebSocket-clients oplossen [CVE-2024-52532]">
<correction libxstream-java "Probleem van denial of service oplossen [CVE-2024-47072]">
<correction linux "Nieuwe bovenstroomse release; ABI verhogen naar 29">
<correction linux-signed-amd64 "Nieuwe bovenstroomse release; ABI verhogen naar 29">
<correction linux-signed-arm64 "Nieuwe bovenstroomse release; ABI verhogen naar 29">
<correction linux-signed-i386 "Nieuwe bovenstroomse release; ABI verhogen naar 29">
<correction live-boot "DHCP proberen op alle aangesloten interfaces">
<correction llvm-toolchain-19 "Nieuw bronpakket om het bouwen van chromium te ondersteunen">
<correction lxc "Oplossen van probleem van null pointer dereference bij het gebruik van een gedeeld rootfs">
<correction mailmindr "Update met het oog op compatibiliteit met Thunderbird 128">
<correction nfs-utils "Verwijzingen herstellen in het geval van --enable-junction=no">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse stabiele release [CVE-2024-0126]">
<correction nvidia-open-gpu-kernel-modules "Nieuwe bovenstroomse LTS-release [CVE-2024-0126]">
<correction oar "Ontbrekende vereiste van libcgi-fast-perl toevoegen; oplossing voor het aanmaken van de oar-gebruiker op nieuwe installaties; SVG-functies met PHP 8 herstellen">
<correction opensc "Datalekprobleem oplossen [CVE-2023-5992]; probleem van gebruik na vrijgave oplossen[CVE-2024-1454]; probleem van ontbrekende initialisatie oplossen [CVE-2024-45615]; diverse problemen met APDU-bufferverwerking oplossen [CVE-2024-45616]; oplossing voor ontbrekende of onjuiste controles van door functies teruggezonden waarden [CVE-2024-45617 CVE-2024-45618]; oplossing voor problemen van <q>onjuiste verwerking van lengte van buffers of bestanden</q> [CVE-2024-45619 CVE-2024-45620]; een probleem van willekeurige code-uitvoering oplossen [CVE-2024-8443]">
<correction openssh "Steeds de interne implementatie van mkdtemp gebruiken; oplossing voor declaratie van gssapi-keyex; ssh-gssapi geautomatiseerde test toevoegen; niet de voorkeur geven aan host-gebonden publieke sleutelhandtekeningen als er geen initiële hostsleutel was; ook het sleuteluitwisselingsalgoritme sntrup761x25519-sha512 beschikbaar stellen zonder het achtervoegsel @openssh.com">
<correction pgtcl "Bibliotheek installeren in het standaard Tcl auto_path">
<correction poco "Een probleem van overloop van gehele getallen oplossen [CVE-2023-52389]">
<correction prometheus-node-exporter-collectors "Ontbrekende `apt_package_cache_timestamp_seconds`-statistieken herstellen; oplossing voor statistieken in verband met apt_upgrades_pending en apt_upgrades_held; het vinden van de tijd van de laatste uitvoering van apt update verbeteren">
<correction pypy3 "Een probleem van ontleden van e-mailadressen oplossen [CVE-2023-27043]; Een mogelijk probleem van Server Side Request Forgery oplossen [CVE-2024-11168]; oplossing voor het ontleden van het bereik van private IP-adressen [CVE-2024-4032]; oplossing voor een probleem van Denial of Service waarbij een reguliere expressie aan de oorsprong ligt [CVE-2024-6232]; probleem van kopregel-injectie oplossen [CVE-2024-6923]; probleem van denial of service oplossen [CVE-2024-7592 CVE-2024-8088]; probleem van commando-injectie oplossen [CVE-2024-9287]">
<correction python-asyncssh "Probleem van <q>rogue extension negotiation</q> oplossen [CVE-2023-46445]; probleem van <q>rogue session attack</q> oplossen [CVE-2023-46446]">
<correction python-tornado "Open redirect-probleem oplossen [CVE-2023-28370]; probleem met denial of service oplossen [CVE-2024-52804]">
<correction python-urllib3 "Mogelijke informatielekken tijdens cross-origin-omleidingen oplossen [CVE-2023-43804]; oplossing voor <q>request body not stripped after redirect from 303 status changes request method to GET</q> [CVE-2023-45803]; oplossing voor <q>Proxy-Authorization request header isn't stripped during cross-origin redirects</q> [CVE-2024-37891]">
<correction python-werkzeug "Oplossing voor denial of service als het uploaden van het bestand begint met CR of LF [CVE-2023-46136]; oplossing voor willekeurige code-uitvoering via de debugger op de machine van de ontwikkelaar [CVE-2024-34069]; oplossing voor denial of service bij de verwerking van verzoeken om meervoudige/formuliergegevens (multipart/form-data requests) [CVE-2024-49767]">
<correction python3.11 "Slecht gevormde adressen in email.parseaddr() afwijzen [CVE-2023-27043]; nieuwe regels coderen in de kopregels in de e-mailmodule [CVE-2024-6923]; kwadratische complexiteit oplossen bij het ontleden van cookies met backslashes [CVE-2024-7592]; oplossing voor het onvermogen van venv-activatiescripts om paden te citeren [CVE-2024-9287]; oplossing voor onjuiste validatie van tussen haakjes geplaatste hosts in urllib-functies [CVE-2024-11168]">
<correction qemu "Nieuwe bovenstroomse release met probleemoplossingen [CVE-2024-7409]; interne codegen helper symbolen markeren als verborgen, om een bouwfout op arm64 te verhelpen">
<correction quicktext "Update met het oog op compatibiliteit met Thunderbird 128">
<correction redis "Oplossing voor denial of service bij foutief gevormde ACL-selectors [CVE-2024-31227]; oplossing voor denial of service door ongebonden patroonherkenning [CVE-2024-31228]; overloop van de stack oplossen [CVE-202431449]">
<correction renderdoc "Overloop van gehele getallen oplossen [CVE-2023-33863 CVE-2023-33864]; probleem met symbolische koppeling-aanvalsvector oplossen [CVE-2023-33865]">
<correction ruby-doorkeeper "Overslaan van autorisatiestappen voorkomen [CVE-2023-34246]">
<correction setuptools "Probleem van code-uitvoering op afstand oplossen [CVE-2024-6345]">
<correction sqlparse "Oplossing voor een met reguliere expressie verband houdend probleem van denial of service [CVE-2023-30608]; probleem van denial of service oplossen [CVE-2024-4340]">
<correction srt "Vereisten juist instellen voor gebruikers van de -dev-pakketten">
<correction systemd "Nieuwe bovenstroomse stabiele release">
<correction tango "De property_*-tabellen bij het installeren compatibel maken met MariaDB 10.11; autopkgtest toevoegen">
<correction tbsync "Update met het oog op compatibiliteit met Thunderbird 128">
<correction texlive-bin "Gegevensverlies oplossen bij het gebruik van discretionaire taken met prioriteiten; heap bufferoverloop repareren [CVE-2024-25262]">
<correction tiff "Problemen met bufferoverloop oplossen [CVE-2023-25433 CVE-2023-26966]; probleem met gebruik na vrijgave oplossen [CVE-2023-26965]; oplossen van probleem van null pointer dereference [CVE-2023-2908]; problemen met denial of service oplossen [CVE-2023-3618 CVE-2023-52356 CVE-2024-7006]">
<correction tzdata "Nieuwe bovenstroomse release: historische gegevens voor sommige zones verbeteren; bevestigen dat er in 2024 geen schrikkelseconde is">
<correction ucf "Variabele die later wordt doorgegeven aan eval initialiseren">
<correction util-linux "Een verdere matiging voor CVE-2024-28085 aanbrengen">
<correction xsane "Een Recommends toevoegen voor zowel firefox-esr als firefox">
<correction zfs-linux "Ontbrekende symbolen toevoegen in libzfs4linux en libzpool5linux; reparatie voor vuile test van dnode [CVE-2023-49298]; oplossing voor de IPv6-adresontleding van sharenfs [CVE-2013-20001]; oplossingen in verband met NULL pointer, geheugentoewijzing, enz.">
<correction zookeeper "Reparatie voor openbaarmaking van informatie in de behandeling van persistente bewakers [CVE-2024-23944]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de
stabiele release. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2024 5801 firefox-esr>
<dsa 2024 5803 thunderbird>
<dsa 2024 5804 webkit2gtk>
<dsa 2024 5805 guix>
<dsa 2024 5806 libarchive>
<dsa 2024 5807 nss>
<dsa 2024 5808 ghostscript>
<dsa 2024 5809 symfony>
<dsa 2024 5810 chromium>
<dsa 2024 5811 mpg123>
<dsa 2024 5812 postgresql-15>
<dsa 2024 5813 symfony>
<dsa 2024 5814 thunderbird>
<dsa 2024 5815 needrestart>
<dsa 2024 5816 libmodule-scandeps-perl>
<dsa 2024 5817 chromium>
<dsa 2024 5818 linux-signed-amd64>
<dsa 2024 5818 linux-signed-arm64>
<dsa 2024 5818 linux-signed-i386>
<dsa 2024 5818 linux>
<dsa 2024 5819 php8.2>
<dsa 2024 5820 firefox-esr>
<dsa 2024 5821 thunderbird>
<dsa 2024 5822 simplesamlphp>
<dsa 2024 5823 webkit2gtk>
<dsa 2024 5824 chromium>
<dsa 2024 5825 ceph>
<dsa 2024 5826 smarty3>
<dsa 2024 5827 proftpd-dfsg>
<dsa 2024 5828 python-aiohttp>
<dsa 2024 5829 chromium>
<dsa 2024 5830 smarty4>
<dsa 2024 5831 gst-plugins-base1.0>
<dsa 2024 5832 gstreamer1.0>
<dsa 2024 5833 dpdk>
<dsa 2024 5835 webkit2gtk>
<dsa 2024 5837 fastnetmon>
<dsa 2024 5838 gst-plugins-good1.0>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd vanwege omstandigheden die we niet onder controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction criu "[armhf] Bouwen mislukt op arm64">
<correction tk-html3 "Niet langer onderhouden; veiligheidsproblemen">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze
tussenrelease in stable, de stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt; of neem contact met het release-team voor de stabiele release op &lt;debian-release@lists.debian.org&gt;.</p>


