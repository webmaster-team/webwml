#use wml::debian::translation-check translation="7e9ce6b3f7065fbbce6287410a40acb00b516392"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.8 werd uitgebracht</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de achtste update aan van oldstable,
zijn voorgaande stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het upgraden van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst met spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Oplossingen voor diverse problemen</h2>

<p>Met deze update van oldstable, de voorgaande stabiele distributie, worden een paar
belangrijke correcties aangebracht aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction adduser "Oplossing voor kwetsbaarheid voor commandoinjectie in deluser">
<correction aide "Oplossing voor probleem met de verwerking van uitgebreide attributen bij symbolische koppelingen">
<correction amd64-microcode "Bijwerken van meegeleverde microcode, inclusief een oplossing voor <q>AMD Inception</q> op AMD Zen4-processors [CVE-2023-20569]">
<correction appstream-glib "De tags &lt;em&gt; en &lt;code&gt; in metadata verwerken">
<correction asmtools "Backport naar bullseye voor toekomstige compilaties van openjdk-11">
<correction autofs "Ontbrekende mutex-ontgrendeling repareren; rpcbind niet gebruiken voor NFS4-aankoppelingen; regressie repareren die de bereikbaarheid op dual-stack-hosts bepaalt">
<correction base-files "Update voor tussenrelease 11.8">
<correction batik "Problemen met vervalsing van Server Side-verzoeken oplossen [CVE-2022-44729 CVE-2022-44730]">
<correction bmake "Instellen van een Conflict met bsdowl (&lt;&lt; 2.2.2-1.2~) tom soepele upgrades te garanderen">
<correction boxer-data "Omzetten van compatibiliteitsoplossingen voor Thunderbird naar deze uitgave">
<correction ca-certificates-java "Tijdelijke oplossing voor niet-geconfigureerde JRE tijdens nieuwe installaties">
<correction cairosvg "Gegevens verwerken: URL's in veilige modus">
<correction cargo-mozilla "Nieuwe <q>bovenstroomse</q> versie om het bouwen van nieuwere versies van firefox-esr te ondersteunen">
<correction clamav "Nieuwe bovenstroomse stabiele release; oplossing voor kwetsbaarheid voor Denial of Service via HFS+ verwerker [CVE-2023-20197]">
<correction cpio "Probleem met uitvoering van willekeurige code oplossen [CVE-2021-38185]; libarchive1 vervangen door libarchive-dev in Suggests:">
<correction cryptmount "Problemen met geheugeninitialisatie vermijden in opdrachtregelverwerker">
<correction cups "Probleem met op heap gebaseerde bufferoverloop oplossen [CVE-2023-4504 CVE-2023-32324], probleem met niet-geauthenticeerde toegang [CVE-2023-32360] en gebruik-na-vrijgave [CVE-2023-34241] oplossen">
<correction curl "Oplossen van probleem met uitvoering van code [CVE-2023-27533 CVE-2023-27534], van probleem met vrijgeven van informatie [CVE-2023-27535 CVE-2023-27536 CVE-2023-28322], van probleem met ongepast hergebruik van verbinding [CVE-2023-27538], van probleem met onjuiste certificaatvalidatie [CVE-2023-28321]">
<correction dbus "Nieuwe bovenstroomse stabiele release; probleem met denial-of-service oplossen [CVE-2023-34969]">
<correction debian-design "Opnieuw bouwen met nieuwere boxer-data">
<correction debian-installer "Linux kernel ABI verhogen naar 5.10.0-26; opnieuw bouwen tegen proposed-updates">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debian-parl "Opnieuw bouwen met nieuwere boxer-data">
<correction debian-security-support "Instellen van DEB_NEXT_VER_ID=12 aangezien dat bookworm de volgende release is; security-support-limited: gnupg1 toevoegen">
<correction distro-info-data "Toevoegen van Debian 14 <q>forky</q>; de releasedatum van Ubuntu 23.04 corrigeren; toevoegen van Ubuntu 23.10 Mantic Minotaur; de geplande releasedatum voor Debian bookworm toevoegen">
<correction dkimpy "Nieuwe bovenstroomse release met probleemoplossingen">
<correction dpdk "Nieuwe bovenstroomse stabiele release">
<correction dpkg "Ondersteuning voor CPU loong64 toevoegen; ontbrekende Version verwerken bij het formatteren van source:Upstream-Version; varbuf-geheugenlek in pkg_source_version() repareren">
<correction flameshot "Uploaden naar imgur standaard uitschakelen; repareren van de naam van het bestand d/NEWS in de vorige upload">
<correction ghostscript "Probleem met bufferoverloop oplossen [CVE-2023-38559]; proberen het opstarten van de IJS-server te beveiligen [CVE-2023-43115]">
<correction gitit "Opnieuw bouwen tegen een nieuw pandoc">
<correction grunt "Raceconditie bij kopiëren van symbolische koppelingen oplossen [CVE-2022-1537]">
<correction gss "Toevoegen van Breaks+Replaces: libgss0 (&lt;&lt; 0.1)">
<correction haskell-hakyll "Opnieuw bouwen tegen nieuw pandoc">
<correction haskell-pandoc-citeproc "Opnieuw bouwen tegen nieuw pandoc">
<correction hnswlib "Oplossing voor dubbele vrijgave in init_index wanneer het M-argument een groot geheel getal is [CVE-2023-37365]">
<correction horizon "Probleem met open omleidingen oplossen [CVE-2022-45582]">
<correction inetutils "De terugkeerwaarden controleren voor set*id()-functies, zodat mogelijke beveiligingsproblemen vermeden worden [CVE-2023-40303]">
<correction krb5 "Probleem met het vrijgeven van een niet-geïnitialiseerde pointer oplossen [CVE-2023-36054]">
<correction kscreenlocker "Authenticatiefout bij gebruik van PAM verhelpen">
<correction lacme "De statussen ready, processing en valid van CA correct veerwerken">
<correction lapack "Reparatie voor eigenvector matrix">
<correction lemonldap-ng "Open omleiding repareren wanneer OIDC RP geen omleidings-URI's heeft; probleem met vervalsing van Server Side-verzoeken oplossen [CVE-2023-44469]; open omleiding repareren als gevolg van onjuiste escape-afhandeling">
<correction libapache-mod-jk "Verwijderen van de impliciete omzettingsfunctionaliteit, die zou kunnen leiden tot onbedoelde blootstelling van de statuswerker en/of het omzeilen van beveiligingsbeperkingen [CVE-2023-41081]">
<correction libbsd "Reparatie voor oneindige lus in MD5File">
<correction libclamunrar "Nieuwe bovenstroomse stabiele release">
<correction libprelude "Python-module bruikbaar maken">
<correction libreswan "Oplossing voor probleem van denial-of-service [CVE-2023-30570]">
<correction libsignal-protocol-c "Oplossing voor probleem met overloop van gehele getallen [CVE-2022-48468]">
<correction linux "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release">
<correction logrotate "Vervanging van /dev/null door een gewoon bestand vermijden als dit wordt gebruikt voor het statusbestand">
<correction ltsp "Het gebruik van <q>mv</q> vermijden bij symbolische koppeling naar init als tijdelijke oplossing voor een probleem met overlayfs">
<correction lttng-modules "Bouwproblemen met nieuwere kernelversies oplossen">
<correction lua5.3 "Oplossing voor gebruik na vrijgave in lua_upvaluejoin (lapi.c) [CVE-2019-6706]; voor segmentatiefout in getlocal en setlocal (ldebug.c) [CVE-2020-24370]">
<correction mariadb-10.5 "Nieuwe bovenstroomse release met probleemoplossingen [CVE-2022-47015]">
<correction mujs "Beveiligingsoplossing">
<correction ncurses "Het laden van aangepaste terminfo-items in setuid/setgid-programma's niet toestaan [CVE-2023-29491]">
<correction node-css-what "Probleem van denial-of-service op basis van reguliere expressies oplossen [CVE-2022-21222 CVE-2021-33587]">
<correction node-json5 "Oplossing voor probleem van prototypepollutie [CVE-2022-46175]">
<correction node-tough-cookie "Oplossen van veiligheidsprobleem (prototypepollutie) [CVE-2023-26136]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release [CVE-2023-25515 CVE-2023-25516]; verhogen van compatibiliteit met recente kernels">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse release [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release met probleemoplossingen [CVE-2023-25515 CVE-2023-25516]">
<correction openblas "Resultaten van DGEMM verbeteren op hardware die geschikt is voor AVX512, wanneer het pakket is gebouwd op hardware van vóór AVX2">
<correction openssh "Probleem met uitvoering van externe code via een doorgestuurde agent-socket oplossen [CVE-2023-38408]">
<correction openssl "Nieuwe bovenstroomse stabiele release; oplossing voor problemen van denial of service [CVE-2023-3446 CVE-2023-3817]">
<correction org-mode "Oplossing voor kwetsbaarheid voor injectie van opdrachten [CVE-2023-28617]">
<correction pandoc "Problemen met willekeurig schrijven naar bestanden oplossen [CVE-2023-35936 CVE-2023-38745]">
<correction pev "Probleem van bufferoverloop oplossen [CVE-2021-45423]">
<correction php-guzzlehttp-psr7 "Onjuiste invoervalidatie corrigeren [CVE-2023-29197]">
<correction php-nyholm-psr7 "Probleem van onjuiste invoervalidatie corrigeren [CVE-2023-29197]">
<correction postgis "Regressie in de asvolgorde repareren">
<correction protobuf "Beveiligingsoplossingen: DoS in Java [CVE-2021-22569]; NULL pointer dereference [CVE-2021-22570]; geheugen-DoS [CVE-2022-1941]">
<correction python2.7 "Oplossingen voor problemen van <q>parameterverhulling</q> [CVE-2021-23336], URL-injectie [CVE-2022-0391], gebruik-na-vrijgave [CVE-2022-48560], XML Externe Entiteit [CVE-2022-48565]; constante-tijdvergelijkingen verbeteren in compare_digest() [CVE-2022-48566]; URL-verwerking verbeteren [CVE-2023-24329]; voorkomen dat niet-geauthenticeerde gegevens worden gelezen op een SSLSocket [CVE-2023-40217]">
<correction qemu "Oplossingen voor problemen van oneindige lus [CVE-2020-14394], NULL pointer dereference [CVE-2021-20196], overloop van gehele getallen [CVE-2021-20203], bufferoverloop [CVE-2021-3507 CVE-2023-3180], denial-of-service [CVE-2021-3930 CVE-2023-3301], gebruik-na-vrijgave [CVE-2022-0216], mogelijke stack-overloop en gebruik-na-vrijgave [CVE-2023-0330], lezen buiten de grenzen [CVE-2023-1544]">
<correction rar "Nieuwe bovenstroomse release; oplossen van probleem van mapoverschrijding [CVE-2022-30333]; probleem met het uitvoeren van willekeurige code oplossen [CVE-2023-40477]">
<correction rhonabwy "aesgcm bufferoverloop oplossen [CVE-2022-32096]">
<correction roundcube "Nieuwe bovenstroomse stabiele release; problemen met cross-site scripting oplossen[CVE-2023-43770]; Enigma: de initiële synchronisatie van privésleutels verbeteren">
<correction rust-cbindgen "Nieuwe <q>bovenstroomse</q> versie om het bouwen van nieuwere versies van firefox-esr te ondersteunen">
<correction rustc-mozilla "Nieuwe <q>bovenstroomse</q> versie om het bouwen van nieuwere versies van firefox-esr te ondersteunen">
<correction schleuder "Torvoegen van versiegebonden vereiste van ruby-activerecord">
<correction sgt-puzzles "Diverse beveiligingsproblemen bij het laden van het spel oplossen [CVE-2023-24283 CVE-2023-24284 CVE-2023-24285 CVE-2023-24287 CVE-2023-24288 CVE-2023-24291]">
<correction spip "Verscheidene beveiligingsoplossingen; beveiligingsoplossing voor uitgebreide filtering van authenticatiegegevens">
<correction spyder "Oplossing voor defecte patch in voorgaande update">
<correction systemd "Udev: reparatie voor symbolische koppelingen naar USB-apparaten in /dev/serial/by-id/; oplossing voor geheugenlek bij daemon-reload; reparatie voor het blijven hangen van de berekening van kalenderspecificaties bij het veranderen van de zomertijd als TZ=Europe/Dublin">
<correction tang "Raceconditie verhelpen bij het aanmaken/rouleren van sleutels; beperkende permissies op sleuteldirectory verzekeren [CVE-2023-1672]; tangd-rotate-keys uitvoerbaar maken">
<correction testng7 "Backporten naar oldstable voor toekomstige compilaties van openjdk-17">
<correction tinyssh "Tijdelijke oplossing voor inkomende pakketten die de maximale pakketlengte niet respecteren">
<correction unrar-nonfree "Probleem met overschrijven van bestanden oplossen [CVE-2022-48579]; oplossen van een probleem van uitvoeren van externe code [CVE-2023-40477]">
<correction xen "Nieuwe bovenstroomse stabiele release; beveiligingsproblemen oplossen [CVE-2023-20593 CVE-2023-20569 CVE-2022-40982]">
<correction yajl "Beveiligingsoplossing voor geheugenlekken; beveiligingsoplossingen: mogelijk probleem van denial-of-service met bewerkt JSON-bestand [CVE-2017-16516]; heap-geheugenbeschadiging bij het verwerken van grote (~2GB) invoer [CVE-2022-24795]; reparatie voor onvolledige patch voor CVE-2023-33460">
</table>


<h2>Beveiligingsupdates</h2>


<p>Met deze revisie worden de volgende beveiligingsupdates toegevoegd aan de release oldstable. Het beveiligingsteam heeft voor elk van deze updates
al een advies uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2023 5394 ffmpeg>
<dsa 2023 5395 nodejs>
<dsa 2023 5396 evolution>
<dsa 2023 5396 webkit2gtk>
<dsa 2023 5397 wpewebkit>
<dsa 2023 5398 chromium>
<dsa 2023 5399 odoo>
<dsa 2023 5400 firefox-esr>
<dsa 2023 5401 postgresql-13>
<dsa 2023 5402 linux-signed-amd64>
<dsa 2023 5402 linux-signed-arm64>
<dsa 2023 5402 linux-signed-i386>
<dsa 2023 5402 linux>
<dsa 2023 5403 thunderbird>
<dsa 2023 5404 chromium>
<dsa 2023 5405 libapache2-mod-auth-openidc>
<dsa 2023 5406 texlive-bin>
<dsa 2023 5407 cups-filters>
<dsa 2023 5408 libwebp>
<dsa 2023 5409 libssh>
<dsa 2023 5410 sofia-sip>
<dsa 2023 5411 gpac>
<dsa 2023 5412 libraw>
<dsa 2023 5413 sniproxy>
<dsa 2023 5414 docker-registry>
<dsa 2023 5415 libreoffice>
<dsa 2023 5416 connman>
<dsa 2023 5417 openssl>
<dsa 2023 5418 chromium>
<dsa 2023 5419 c-ares>
<dsa 2023 5420 chromium>
<dsa 2023 5421 firefox-esr>
<dsa 2023 5422 jupyter-core>
<dsa 2023 5423 thunderbird>
<dsa 2023 5424 php7.4>
<dsa 2023 5426 owslib>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5431 sofia-sip>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5438 asterisk>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5441 maradns>
<dsa 2023 5442 flask>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
<dsa 2023 5452 gpac>
<dsa 2023 5453 linux-signed-amd64>
<dsa 2023 5453 linux-signed-arm64>
<dsa 2023 5453 linux-signed-i386>
<dsa 2023 5453 linux>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5461 linux-signed-amd64>
<dsa 2023 5461 linux-signed-arm64>
<dsa 2023 5461 linux-signed-i386>
<dsa 2023 5461 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5470 python-werkzeug>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5478 openjdk-11>
<dsa 2023 5479 chromium>
<dsa 2023 5480 linux-signed-amd64>
<dsa 2023 5480 linux-signed-arm64>
<dsa 2023 5480 linux-signed-i386>
<dsa 2023 5480 linux>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5486 json-c>
<dsa 2023 5487 chromium>
<dsa 2023 5489 file>
<dsa 2023 5490 aom>
<dsa 2023 5491 chromium>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5497 libwebp>
<dsa 2023 5500 flac>
<dsa 2023 5502 xorgxrdp>
<dsa 2023 5502 xrdp>
<dsa 2023 5503 netatalk>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Verwijderde pakketten</h2>

<p>De volgende pakketten werden verwijderd wegens omstandigheden waarover wij geen controle hebben:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction atlas-cpp "onstabiele bovenstroom, ongeschikt voor Debian">
<correction ember-media "onstabiele bovenstroom, ongeschikt voor Debian">
<correction eris "onstabiele bovenstroom, ongeschikt voor Debian">
<correction libwfut "onstabiele bovenstroom, ongeschikt voor Debian">
<correction mercator "onstabiele bovenstroom, ongeschikt voor Debian">
<correction nomad "beveiligingsoplossingen zijn niet langer beschikbaar">
<correction nomad-driver-lxc "vereist nomad dat verwijderd zal worden">
<correction skstream "onstabiele bovenstroom, ongeschikt voor Debian">
<correction varconf "onstabiele bovenstroom, ongeschikt voor Debian">
<correction wfmath "onstabiele bovenstroom, ongeschikt voor Debian">

</table>

<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in oldstable, de vorige stabiele release, opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Voorgestelde updates voor de distributie oldstable:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>informatie over de distributie oldstable (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een samenwerkingsverband van ontwikkelaars van vrije
software die vrijwillig tijd en moeite steken in het produceren van het
volledig vrije besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>


