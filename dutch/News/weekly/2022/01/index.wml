#use wml::debian::projectnews::header PUBDATE="2022-07-16" SUMMARY="Welkom bij het DPN, DebConf22 begint, data voor het bevriezen van bookworm, DSA's, nieuws over Debian-releases, verkiezingen en stemmingen, doelgroepenwerking, evenementen, rapporten, medewerkers"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="ee107e946f2ad10ff55cb1d418e1185fe697f471"

# Status: [frozen]

## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<h2>Welkom bij het to the Debian projectnieuws!</h2>

<shortintro issue="eerste"/>

<p>We hopen dat u geniet van deze editie van het DPN.</p>

<h2>DebConf22 is begonnen!</h2>

<p>
Na twee online edities in 2020 en 2021, keert de jaarlijkse Debian Conferentie terug naar zijn gebruikelijke formule.: <a href="https://debconf22.debconf.org/">DebConf22</a>
wordt gehouden in Prizren, Kosovo van zondag 17 tot en met zondag 24 juli 2022.</p>

<p>
Het <a href="https://debconf22.debconf.org/schedule/">volledige programma</a> 
omvat presentaties van 45 en 20 minuten, teamvergaderingen ("BoF"), workshops, een jobbeurs en diverse andere evenementen. Videostreaming zal ook <a href="https://debconf22.debconf.org/">beschikbaar zijn op de DebConf22-website</a>.
</p>
<p>
U kunt ook de live berichtgeving van nieuws over DebConf22 volgen op <a href="https://micronews.debian.org">https://micronews.debian.org</a> of via het @debian-profiel in uw favoriete sociale netwerk.

</p>
<p>
Debian bedankt de talrijke <a href="https://debconf22.debconf.org/sponsors/">sponsors</a> die DebConf22 steunen, in het bijzonder de Platinum Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://itp-prizren.com">ITP Prizren</a>
en <a href="https://google.com">Google</a>.
</p>

<h2>Bookworm bevriezingsdata (voorlopig)</h2>

<p>
De ontwikkeling van de volgende Debian-release gaat door en het Debian Releaseteam heeft een tijdlijn <a href="https://lists.debian.org/debian-devel/2022/03/msg00251.html">voorgesteld</a> voor de bevriezing ervan:
</p>

<p>
Januari 2023 (12-01-2023): Mijlpaal 1 - bevriezing voor transities en de gereedschapsset<br/>
Februari 2023 (12-02-2023): Mijlpaal 2 - zachte bevriezing<br/>
Maart 2023 (12-03-2023): Mijlpaal 3 - harde bevriezing - voor belangrijke pakketten en pakketten zonder autopkgtests<br/>
Nog aan te kondigen: Mijlpaal 4 - volledige bevriezing
</p>

<introtoc/>

<toc-display/>

<p>Lees voor ander nieuws de officiële blog van Debian,
<a href="https://bits.debian.org">Bits from Debian</a>, en volg
<a href="https://micronews.debian.org">https://micronews.debian.org</a> dat ook het @debian-profiel op verschillende sociale netwerken voedt via RSS.</p>


<toc-add-entry name="security">Belangrijke beveiligingsadviezen van Debian</toc-add-entry>

<p> Het beveiligingsteam van Debian brengt dagelijks actuele adviezen uit: 
(<a href="$(HOME)/security/2022/">Beveiligingsadviezen 2022</a>). 
Lees ze zorgvuldig en abonneer u op de <a href="https://lists.debian.org/debian-security-announce/">beveiligingsmailinglijst</a> om de beveiliging van uw systemen up-to-date te houden tegen eventuele kwetsbaarheden.
Op 1 november 2021 werd het Debian beveiligingsadvies nummer #5000 gepubliceerd. Bedankt beveiligingsteam voor de voortdurende inzet en ondersteuning al die jaren!</p>

## Pull the below data directly from $(HOME)/security/2020/. Swap out the 
## dsa-XXXX for the current advisory # , be sure to change the name as well. 

<p>Sommige recent vrijgegeven adviezen hebben betrekking op deze pakketten: 
<a href="$(HOME)/security/2022/DSA-5185-1">mat2</a>,
<a href="$(HOME)/security/2022/DSA-5184-1">xen</a>,
<a href="$(HOME)/security/2022/DSA-5183-1">wpewebkit</a>,
<a href="$(HOME)/security/2022/DSA-5182-1">webkit2gtk</a>,
<a href="$(HOME)/security/2022/DSA-5181-1">request-tracker4</a>,
<a href="$(HOME)/security/2022/DSA-5180-1">chromium</a>,
<a href="$(HOME)/security/2022/dsa-5179-1">php7.4</a>,
<a href="$(HOME)/security/2022/dsa-5178-1">intel-microcode</a>,
<a href="$(HOME)/security/2022/dsa-5177-1">ldap-account-manager</a>,
<a href="$(HOME)/security/2022/dsa-5176-1">blender</a>,
<a href="$(HOME)/security/2022/dsa-5175-1">thunderbird</a>,
<a href="$(HOME)/security/2022/dsa-5174-1">gnupg2</a>.

<p>De website van Debian <a href="https://www.debian.org/lts/security/">archiveert</a> ook de beveiligingsadviezen die werden uitgegeven door het Debian-team voor laqngetermijnondersteuning en die worden geplaatst op de <a href="https://lists.debian.org/debian-lts-announce/">mailinglijst debian-lts-announce</a>.

<toc-add-entry name="bullseye">Nieuws over Debian <q>bullseye</q> en <q>buster</q></toc-add-entry>

<p><b>Debian 11 en Debian 10 werden bijgewerkt: 11.4 en 10.12 zijn uitgebracht</b></p>

<p>Sinds de <a href="https://www.debian.org/News/2021/20210814">eerste release</a> op 14 augustus 2021, heeft het Debian-project vier updates van zijn stabiele distributie Debian 11 (codenaam <q>bullseye</q>) uitgebracht, de laatste werd <a href="https://www.debian.org/News/2022/20220709">aangekondigd</a> op 9 juli 2022.</p>

<p>Het Debian-project <a href="https://www.debian.org/News/2022/2022032602">kondigde</a> op 23 maart 2022 ook de twaalfde update aan van zijn oude stabiele distributie Debian 10 (codenaam <q>Buster</q>) als tussenrelease 10.12.</p>

<p>Deze tussenreleases voegden correcties toe voor beveiligingsproblemen, samen met enkele aanpassingen voor ernstige problemen. Beveiligingsadviezen zijn al afzonderlijk gepubliceerd en worden waar mogelijk vermeld.

Een bestaande installatie upgraden naar een van beide versies kan worden bereikt door het pakketbeheersysteem naar een van de vele HTTP-speigelservers van Debian te verwijzen..
Een volledige lijst van spiegelservers is beschikbaar op:
<a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>
</p>

<toc-add-entry name="lts">Nieuws over Debian LTS</toc-add-entry>

<p>Het Debian-team voor langetermijnondersteuning (Long Term Support - LTS) 
<a href="https://lists.debian.org/debian-lts-announce/2022/07/msg00002.html">kondigde aan</a> dat de ondersteuning voor Debian 9
<q>stretch</q> eindigde op 1 juli 2022.
Een gedeelte van de pakketten zal vanaf nu worden ondersteund door externe partijen (gedetailleerde informatie kan worden gevonden op <a href="https://wiki.debian.org/LTS/Extended">Uitgebreide LTS</a>).</p>

<p>
Het LTS-team zal in augustus de ondersteuning van Debian 10 <q>buster</q> overnemen van het beveiligingsteam aangezien de laatste tussenrelease voor <q>buster</q> in die maand zal worden uitgebracht. Debian 10 zal ook tot vijf jaar na de oorspronkelijke release langetermijnondersteuning ontvangen, een ondersteuning die zal eindigen op 30 juni 2024.</p>

<toc-add-entry name="bugs">Meer dan 1.000.000 bugs gerapporteerd</toc-add-entry>
<p>
Op 18 november 2021, om 12:06:14 UTC, bereikte Debian een mijlpaal met bug <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1000000"># 1.000.000</a>. Dank aan al onze ontwikkelaars, medewerkers en gebruikers die ons hebben geholpen deze mijlpaal te bereiken (en te verhelpen).
</p>

<toc-add-entry name="elections">Verkiezingen en stemmingen in het Debian-project</toc-add-entry>

<p>
Er zijn de afgelopen maanden in Debian verschillende beslissingen genomen via het proces van algemene resoluties en andere soorten verkiezingen, en er staan nog andere discussies en beslissingen gepland voor de volgende maanden.
Hier vindt u een korte samenvatting ervan:
</p>

<p><b>Algemene resolutie: de resolutieprocedure wijzigen</b><p>
<p>Na een periode van voorstellen en besprekingen die in 2021 begon, heeft het project officieel een aantal wijzigingen in de statuten van Debian aangenomen die de resolutieprocedure vanaf januari 2022 wijzigen.
Met deze statutenwijzigingen wordt geprobeerd een aantal vastgestelde problemen aan te pakken. Dit gebeurt door de procedure bij het Technisch Comité te scheiden van de procedure bij een algemene resolutie, aangezien deze aan verschillende behoeften beantwoorden. Er werd een maximumperiode voor de behandeling van een resolutie vastgesteld. Wanneer de inhoud van de stemming wordt gewijzigd, wordt de besprekingsperiode van de algemene resolutie automatisch verlengd. De AR-procedure wordt gewijzigd zodat alle stemopties gelijk worden behandelen. Daarnaast worden andere verbeteringen of verduidelijkingen van de procedure doorgevoerd.
</p>

<p>Alle informatie over deze stemming is te vinden op de 
<a href="https://www.debian.org/vote/2021/vote_003">webpagina over deze algemene resolutie</a> 
en de aangenomen wijzigingen werden vastgelegd in versie 1.8 van de statuten van Debian.</p>

<p><b>Algemene resolutie: geheimhouding van de stemming</b></p>
<p>Na een periode van voorstellen en besprekingen heeft het project officieel een wijziging in de statuten van Debian aangenomen om de identiteit van ontwikkelaars die een bepaalde stem uitbrengen te verbergen en verificatie mogelijk te maken.
Al de informatie over deze stemming is te vinden op de 
<a href="https://www.debian.org/vote/2022/vote_001">webpagina over deze algemene resolutie</a>.</p>

<p><b>Debian projectleiderverkiezing 2022: Jonathan Carter werd herverkozen</b></p>

<p>
Jonathan Carter
<a href="https://bits.debian.org/2022/04/dpl-elections-2022.html">werd herverkozen</a>
voor een nieuwe termijn. Hij stuurde een bericht 
<a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00006.html">Bits van de DPL</a>
in april en een nieuwe update over de taken van de projectleider staat 
<a href="https://debconf22.debconf.org/talks/5-bits-from-the-dpl/">ingeplant</a> op de eerste dag van DebConf22.</p>

<p><b>Open discussie over firmware</b></p>

<p>Steve McIntyre <a href="https://lists.debian.org/debian-devel/2022/04/msg00130.html">startte een discussie</a>
binnen het project om meningen en voorstellen te verzamelen over hoe de ondersteuning voor firmware in Debian verbeterd zou kunnen worden.
De uitwisseling van ideeën en meningen gaat nog steeds door en er is aan dit onderwerp een 
<a href="https://debconf22.debconf.org/talks/43-fixing-the-firmware-mess/">BoF-sessie in DebConf22</a> gewijd.
De voortgang naar het nemen van een beslissing kan in de toekomst gepaard gaan met een voorstel van algemeen resolutie om een duidelijk mandaat van het project te krijgen over de wijze waarop te werk moet worden gegaan.
</p>

<toc-add-entry name="outreach">Activiteiten in het kader van de doelgroepenwerking van Debian</toc-add-entry>

<p>
Debian blijft deelnemen aan de programma's Outreachy en Google Summer of Code, en we zijn verheugd aan te kondigen dat <a href="https://bits.debian.org/2022/05/welcome-outreachy-interns-2022.html">twee stagiairs</a> de integratie van de Yarn-pakketbeheerder in Debian zullen verbeteren voor de Outreachy-ronde mei 2022 - augustus 2022, en dat <a href="https://bits.debian.org/2022/05/welcome-gsoc2022-interns.html">drie stagiairs</a> de Android SDK-tools in Debian zullen verbeteren evenals de kwaliteitszorg voor de biologische en medische toepassingen binnen Debian in het kader van de editie 2022 van de Google Summer of Code.
</p>

<p>
Tijdens DebCamp van DebConf22 werd een <a href="https://lists.debian.org/debconf-announce/2022/07/msg00003.html">BootCamp</a> 
georganiseerd om nieuwkomers te verwelkomen en hen te helpen om kennis te maken met Debian en praktische ervaring op te doen met ons geliefde besturingssysteem en bij te dragen aan het project.
</p>

<p>
Als u wil helpen bij de inspanningen om Debian uit te breiden en diverser te maken, aarzel dan niet om u aan te sluiten bij het Debian Outreach-team! 
U kunt de dagelijkse werkzaamheden van de nieuwe medewerkers en de organisatie van de programma's volgen op de <a href="http://lists.debian.org/debian-outreach/">mailinglijst debian-outreach</a> en met ons chatten op ons IRC-kanaal #debian-outreach.
U kunt ook kijken of deelnemen aan andere teams met betrekking tot de uitbreiding van de gemeenschap: de <a href="https://wiki.debian.org/LocalGroups">Lokale Debian groepen</a> (gericht op het ondersteunen van initiatieven van lokale groepen met infrastructuur, extraatjes, ideeën, enz.), het <a href="https://wiki.debian.org/Welcome">Welkomstteam</a>, erop gericht om nieuwkomers te helpen hun weg te vinden bij het beginnen gebruiken van Debian of bij te dragen aan het project, en natuurlijk <a href="https://mentors.debian.net/">Debian Mentors</a> om mensen te ondersteunen die beginnen bij te dragen aan Debian op het gebied van verpakken en infrastructuur.</p>

<toc-add-entry name="events">BSP's, Evenementen, MiniDebCamps en MiniDebConfs</toc-add-entry>


<p><b>Aankomende evenementen</b></p>

<p>Volgende week dinsdag 16 augustus 2022 viert de Debian-gemeenschap de 29e verjaardag van Debian.
Sommige evenementen zijn al gepland. Kijk op de bijbehorende <a href="https://wiki.debian.org/DebianDay/2022">wiki-pagina</a> om er meer over te weten te komen of voeg uw Debian Day-feest toe aan de lijst om anderen te informeren over uw lokale viering van Debian Day.
</p>

<p>
Er komt een <a href="https://lists.debian.org/debian-events-eu/2022/06/msg00003.html">stand om Debian voor te stellen</a> die gedeeld wordt met <a href="https://blends.debian.org/edu/">Debian Edu</a> op <a href="https://www.froscon.org/">FrOSCon</a>, 20/21 augustus 2022 in Sankt Augustin, Duitsland.
</p>

<p>
Van 14 tot 16 oktober 2022 zal er een <a href="https://lists.debian.org/debian-devel-announce/2022/06/msg00002.html">Bug Squashing Party</a> (probleemoplossingsfeest) worden gehouden in Karlsruhe, georganiseerd (en genereus gesponsord door) <a href="https://www.unicon.com/">Unicon GmbH</a> in het kader van de eerste fase van de bookwormbevriezing die gepland staat voor het begin van 2023. Om de organisatie van het evenement te vergemakkelijken, wordt u vriendelijk verzocht om u aan te melden op de <a href="https://wiki.debian.org/BSP/2022/10/de/Karlsruhe">wiki-pagina</a> als u geïnteresseerd bent.
</p>

<p><b>Afgelopen evenementen</b></p>


<p>De <a href="https://wiki.debian.org/DebianEvents/de/2021/MiniDebConfRegensburg">MiniDebConf 2021 Regensburg</a>, Duitsland, vond plaats op 2-3 oktober 2021. Met meer dan 55 aanwezigen en meer dan 20 lezingen en korte presentaties was dit evenement een groot succes. Er zijn <a href="https://meetings-archive.debian.net/pub/debian-meetings/2021/MiniDebConf-Regensburg/">video-opnames</a> beschikbaar.
</p>

<p>
Debian had een <a href="https://stands.fosdem.org/stands/debian/">virtuele stand</a>
op FOSDEM 2022 dat online plaats vond op 5&ndash;6 februari 2022.
</p>

<p>
Het Debian Clojure-team hield op 13 en 14 mei 2022 een tweedaagse <a href="https://veronneau.org/clojure-team-2022-sprint-report.html">tele-sprint</a> waaraan vijf leden deelnamen om verschillende aspecten van het Clojure-ecosysteem in Debian te verbeteren.
</p>

<p>
De langverwachte <a href="https://wiki.debian.org/DebianEvents/de/2022/DebianReunionHamburg">Debian Reunion Hamburg 2022</a> werd gehouden van maandag 23 mei tot en met 30 mei 2022, te beginnen met vijf dagen coderen gevolgd door twee dagen lezingen. Mensen - meer dan zestig aanwezigen - waren blij om elkaar persoonlijk te ontmoeten, om te coderen en te kletsen en nog veel meer. Voor degenen die de livestreams hebben gemist, zijn er <a href="https://meetings-archive.debian.net/pub/debian-meetings/2022/Debian-Reunion-Hamburg/">video</a>-opnames beschikbaar.
</p>

<p>Tijdens de Debian-reünie kwamen drie leden van de Debian Perl-groep bijeen in een <a href="https://bits.debian.org/2022/07/debian-perl-sprint-2022.html">(officieuze) Debian Perl Sprint</a> om het Perl-ontwikkelingswerk voor Bookworm voort te zetten en om te werken aan QA-taken voor hun ruim 3800 pakketten.
</p>

<toc-add-entry name="reports">Rapporten</toc-add-entry>

## Team and progress reports. Consider placing reports from teams here rather 
## than inside of the internal news sections. 

## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#

<p><b>Maandelijkse LTS-rapporten van Freexian</b></p>

<p>Freexian brengt <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">maandelijks rapporten</a> uit over het werk van betaalde medewerkers die bijdragen aan de langetermijnondersteuning van Debian.
</p>

<p><b>Update over de toestand van reproduceerbare builds</b></p>

<p>Volg de <a href="https://reproducible-builds.org/blog/">Reproducible Builds-blog</a> voor wekelijkse rapporten over hun werk in de <q>buster</q>-cyclus.
</p>

<toc-add-entry name="help">Hulp gevraagd</toc-add-entry>

<p><b>Pakketten waarvoor hulp gevraagd wordt:</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2022/07/msg00111.html"
        orphaned="1261"
        rfa="180" />

<p><b>Bugs voor nieuwkomers</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian heeft een <q>newcomer</q> bug-tag, die wordt gebruikt om bugs aan te geven die geschikt zijn voor nieuwe medewerkers als instappunt om aan specifieke pakketten te werken.

Momenteel zijn er <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">199</a>
bugs beschikbaar met de tag <q>newcomer</q>.
</p>


<toc-add-entry name="code">Code, codeurs en medewerkers</toc-add-entry>
<p><b>Nieuwe pakketbeheerders sinds 18 maart 2021</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD
Wij verwelkomen Ganesh Pawar, Job Snijders, Hugo Torres de Lima, Saakshi Jain,
Ajayi Olatunji, Eberhard Beilharz, Ayoyimika Ajibade, Mingyu Wu, Imre Jonk,
harish chavre, Jan Gru, Felix C. Stegerman, Kai-Heng Feng, Brian Thompson,
Heinrich Schuchardt, Alex David, Cézar Augusto de Campos, Antoine Le Gonidec,
Fabrice Creuzot, Pavit Kaur, Mickael Asseline, Lin Qigang, Takuma Shibuya,
Daniel Duan, karthek, Tom Teichler, Marius Vlad, Dave Lambley, Dave Jones, Jan
Gruber, Erik Maciejewski, Daniel Salzman, Caleb Adepitan, Faustin Lammler, Linus
Vanas, Prateek Ganguli, Tian Qiao, Taavi Väänänen, Andrea Pappacoda, Carsten
Schoenert, Ricardo Brandao, Joshua Peisach, Filip Strömbäck, Victor Raphael
Santos Souza, Luiz Amaral, Roman Lebedev, Paolo Pisati, S. 7, Frédéric Danis,
Mark King, Ben Westover, Chris Talbot, Bartek Fabiszewski, Matteo Bini, Lance
Lin, Shlomi Fish, William 'jawn-smith' Wilson, Carlos F. Sanz, Ileana Dumitrescu,
Marcus Hardt, Victor Westerhuis, Christophe Maudoux, Agathe Porte, Martin Dosch,
Krzysztof Aleksander Pyrkosz, Hannes Matuschek, Lourisvaldo Figueredo Junior,
Thomas E. Dickey, Ruffin White, Chris MacNaughton, Ed J, Thiago Pezzo,
Matthieu Baerts, Ryan Gonzalez, Maxim W., Katharina Drexel, Josenilson Ferreira
da Silva, Felix Dörre, Fukui Daichi, Timon Engelke, Maxime Chambonnet,
Christopher Obbard, Martin Guenther, Nick Rosbrook, Daniel Grittner, Bo Yu,
Michael Ikwuegbu, Heather Ellsworth, Israel Galadima, Francois Gindraud, Tobias
Heider, Leandro Ramos, Erik Hulsmann, Sebastian Crane, Eivind Naess, Vignesh
Raman, clesia_roberto, Jack Toh, Calvin Wan, Matthias Geiger, Robert Greener,
Lena Voytek, Glen Choo, Helmar Gerloni, Vinay Keshava, Michel Alexandre Salim,
Lev Borodin, Fab Stz, Matt Barry, Travis Wrightsman, Nathan Pratta Teodosio,
Philippe Swartvagher, Dennis Filder, Robin Alexander, Christoph Hueffelmann,
David Heidelberg, Juri Grabowski, Anupa Ann Joseph, Aaron Rainbolt en Braulio
Henrique Marques Souto.

<p><b>Nieuwe medewerkers van Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD
Wij verwelkomen Markus Schade, Jakub Ružička, Evangelos Ribeiro Tzaras,
Hugh McMaster, Douglas Andrew Torrance, Marcel Fourné, Marcos Talau,
Sebastian Geiger, Clay Stan, Daniel Milde, David da Silva Polverari,
Sunday Cletus Nkwuda, Ma Aiguo, Sakirnth Nagarasa, Lukas Matthias Märdian,
Paulo Roberto Alves de Oliveira, Sergio Almeida Cipriano Junior, Julien Lamy,
Kristian Nielsen, Jeremy Paul Arnold Sowden, Jussi Tapio Pakkanen,
Marius Gripsgard, Martin Budaj, Peymaneh, Tommi Petteri Höynälänmaa, Lu YaNing,
Mathias Gibbens, Markus Blatt, Peter Blackman, Jan Mojžíš, Philip Wyett,
Thomas Ward, Fabio Fantoni, Mohammed Bilal en Guilherme de Paula Xavier Segundo.

<p><b>Nieuwe ontwikkelaars van Debian</b></p>
Wij verwelkomen Jeroen Ploemen, Mark Hindley, Scarlett Moore, Baptiste Beauplat,
Gunnar Ingemar Hjalmarsson, Stephan Lachnit, Timo Röhling, Patrick Franz,
Christian Ehrhardt, Fabio Augusto De Muzio Tobich, Taowa, Félix Sipma,
Étienne Mollier, Daniel Swarbrick, Hanno Wagner, Aloïs Micard, Sophie Brun,
Bastian Germann, Gürkan Myczko, Douglas Andrew Torrance, Mark Lee Garrett,
Francisco Vilmar Cardoso Ruviaro , Henry-Nicolas Tourneur en Nick Black.

<p><b>Medewerkers</b></p>
## Visit the link below and pull the information manually.

<p>
1064 personen en 10 teams worden momenteel vermeld op de pagina
<a href="https://contributors.debian.org/">Medewerkers van Debian Contributors</a> in 2022.
</p>

<toc-add-entry name="quicklinks">Snelle links van Debian sociale media</toc-add-entry>

<p>
Dit is een uittreksel uit de <a href="https://micronews.debian.org">micronews.debian.org</a>-feed van 2022, waaruit we de onderwerpen die al in deze DPN-uitgave zijn besproken, hebben verwijderd.
U kunt deze sectie overslaan als u al <b>micronews.debian.org</b> of het <b>@debian</b>-profiel op een sociaal netwerk (Pump.io, GNU Social, Mastodon of Twitter) volgt. De items worden onopgemaakt in aflopende volgorde op datum (recente nieuwsberichten bovenaan) weergegeven.
</p>

<ul>

<li>debuginfod.debian.net (de Debian-service die ervoor zorgt dat ontwikkelaars geen debuginfo-pakketten meer hoeven te installeren om programma's te debuggen) is weer online! 
<a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00001.html</a>
</li>

<li>In "Hoe Google is overgestapt op doorlopende Linux-releases voor Desktops", leggen Margarita Manterola en andere Debian-vrienden de overstap uit naar een doorlopend release-model voor de interne Linux-distributie van Google, gebaseerd op Debian
<a href="https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops">https://cloud.google.com/blog/topics/developers-practitioners/how-google-got-to-rolling-linux-releases-for-desktops</a>
</li>

<li>Python teamsprint tijdens #DebConf22 #DebCamp (vandaag vrijdag de 15e) <a href="https://wiki.debian.org/DebConf/22/Sprints">https://wiki.debian.org/DebConf/22/Sprints</a></li>

<li>Welkom in het Innovatie- en Trainingpark Prizren (ITP) in Kosovo (foto van Daniel Lenharo) #DebCamp #DebConf22</li>

<li>Riseup-vpn (gemakkelijke, snelle en veilige VPN-service van riseup.net, stuurt al uw internetverkeer over een versleutelde verbinding) geaccepteerd in unstable <a href="https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/">https://tracker.debian.org/news/1344547/accepted-riseup-vpn-02111ds-1-source-amd64-into-unstable-unstable/</a></li>

<li>Sprint van Debian reproduceerbare Builds tijdens #DebConf22 #DebCamp <a href="https://lists.debian.org/debconf-discuss/2022/07/msg00035.html">https://lists.debian.org/debconf-discuss/2022/07/msg00035.html</a></li>

<li>Sprint van Mobian/Debian op mobieltjes tijdens #DebConf22 #DebCamp <a href="https://lists.debian.org/debconf-discuss/2022/07/msg00034.html">https://lists.debian.org/debconf-discuss/2022/07/msg00034.html</a></li>

<li>#DebConf22 is op komst! #DebCamp gaat door op 10-16 juli 2022 <a href="https://debconf22.debconf.org/about/debcamp/">https://debconf22.debconf.org/about/debcamp/</a></li>

<li>Bits van het technisch comité <a href="https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/07/msg00000.html</a></li>

<li>Nieuwe ontwikkelaars en pakketbeheerders van Debian (maart en april 2022) <a href="https://bits.debian.org/2022/05/new-developers-2022-04.html">https://bits.debian.org/2022/05/new-developers-2022-04.html</a></li>

<li>Debian beleidsrichtlijnen 4.6.1.0 uitgebracht <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00003.html</a></li>

<li>Wat is DebCamp? Zou ik moeten deelnemen? <a href="https://debconf22.debconf.org/about/debcamp">https://debconf22.debconf.org/about/debcamp</a></li>

<li>Periode om deel te nemen aan de enquête onder Debian-ontwikkelaars verlengd tot 07-05-2022 <a href="https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/05/msg00000.html</a></li>

<li>Divers ontwikkelaarsnieuws (#57): Debianbijeenkomst Hamburg 2022, Python-pakketten: pep517 bouwhulpmiddel, Nieuwe Debian mailinglijst: debian-math <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00010.html</a></li>

<li>Freexian en het LTS-team hebben een enquête uitgebracht met vragen om zowel de Debian Projectleider als externe financieringsinstanties te helpen bij het opstellen van beleid over welk soort Debian-werk acceptabel is om betaald te worden. <a href="https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html">https://lists.debian.org/debian-devel-announce/2022/04/msg00002.html</a></li>

<li>Nieuwe ontwikkelaars en pakketbeheerders van Debian (januari en februari 2022) <a href="https://bits.debian.org/2022/03/new-developers-2022-02.html">https://bits.debian.org/2022/03/new-developers-2022-02.html</a></li>

<li>Salsa (salsa.debian.org, de Debian Gitlab-instantie) is weer online <a href="https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html">https://lists.debian.org/debian-infrastructure-announce/2022/03/msg00000.html</a> - thanks DSA and Salsa admins!</li>

<li>Salsa (salsa.debian.org, de Debian Gitlab-instantie) is momenteel niet beschikbaar. We hopen deze snel weer terug te hebben, heb alstublieft wat geduld.</li>

<li>"CI voor het Debian kernelteam" door Ben Hutchings <a href="https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html">https://www.decadent.org.uk/ben/blog/ci-for-the-debian-kernel-team.html</a></li>

<li>Perl 5.34 transitie is op komst in unstable! <a href="https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html">https://lists.debian.org/debian-devel-announce/2022/02/msg00000.html</a></li>

<li>De specifieke uitgave Debian Junior wordt opnieuw gelanceerd en verwelkomt bijdragen! Doe mee aan de inspanning om Debian het besturingssysteem te maken dat kinderen graag gebruiken <a href="https://lists.debian.org/debian-jr/2022/02/msg00000.html">https://lists.debian.org/debian-jr/2022/02/msg00000.html</a></li>

<li>Nieuwe ontwikkelaars en pakketbeheerders van Debian (november en december 2021) <a href="https://bits.debian.org/2022/01/new-developers-2021-12.html">https://bits.debian.org/2022/01/new-developers-2021-12.html</a></li>
</ul>

<toc-add-entry name="continuedpn">Wilt u DPN verder blijven lezen?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Aanmelden of afmelden</a> voor de mailinglijst Debian News.</p>

#use wml::debian::projectnews::footer editor="Het publiciteitsteam met bijdragen van Laura Arjona Reina, Jean-Pierre Giraud"
# No need for the word 'and' for the last contributor, it is added at build time.
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
