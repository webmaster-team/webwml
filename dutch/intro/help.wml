#use wml::debian::template title="Bijdragen: hoe u Debian kunt helpen" MAINPAGE="true"
#use wml::debian::translation-check translation="63b2f61a1cd8e47d08d0bb9461b658c7917a74a8"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#coding">Coderen en pakketten onderhouden</a></li>
    <li><a href="#testing">Testen en bugs oplossen</a></li>
    <li><a href="#documenting">Documentatie schrijven</a></li>
    <li><a href="#translating">Vertalen en lokaliseren</a></li>
    <li><a href="#usersupport">Andere gebruikers helpen</a></li>
    <li><a href="#events">Evenementen organiseren</a></li>
    <li><a href="#donations">Geld, hardware of bandbreedte doneren</a></li>
    <li><a href="#usedebian">Debian gebruiken</a></li>
    <li><a href="#organizations">Hoe uw organisatie Debian kan ondersteunen</a></li>
  </ul>
</div>

<p>Debian is niet enkel een besturingssysteem, het is een gemeenschap. Veel
mensen met veel verschillende vaardigheden dragen bij aan het project: onze
software, de grafische vormgeving, de wiki en andere documentatie zijn het
resultaat van een gezamenlijke inspanning van een grote groep individuen. Niet
iedereen is een ontwikkelaar, en u hoeft zeker niet te kunnen coderen om mee te
werken. Er zijn veel verschillende manieren waarop u kunt helpen Debian nog
beter te maken. Als u mee wilt doen, volgen hier enkele suggesties voor zowel
ervaren als onervaren gebruikers.</p>

<h2><a id="coding">Coderen en pakketten onderhouden</a></h2>

<aside class="light">
  <span class="fa fa-code-branch fa-5x"></span>
</aside>

<p>Misschien wilt u een volledig nieuwe applicatie schrijven, misschien wilt u
een nieuwe functie in een bestaand programma implementeren. Als u een
ontwikkelaar bent en een bijdrage wilt leveren aan Debian, kunt u ons ook
helpen om de software in Debian klaar te maken voor een vlotte installatie, we
noemen dit "verpakken". Bekijk deze lijst voor wat ideeën om aan de slag te
gaan:</p>

<ul>
  <li>Toepassingen verpakken, bijvoorbeeld die waarmee u ervaring heeft of die u waardevol acht voor Debian. Voor meer informatie over hoe u een pakketbeheerder kunt worden, gaat u naar de <a href="$(HOME)/devel/">Hoek voor ontwikkelaars van Debian</a>.</li>
  <li>Bestaande applicaties helpen onderhouden, bijvoorbeeld door bij te dragen met reparaties (patches) of bijkomende informatie aan te dragen in het <a href="https://bugs.debian.org/">bugvolgsysteem</a>. U kunt ook toetreden tot een team voor gezamenlijk pakketbeheer of deelnemen aan een softwareproject op <a href="https://salsa.debian.org/">Salsa</a> (onze eigen GitLab-realisatie).</li>
  <li>Ons helpen bij het <a href="https://security-tracker.debian.org/tracker/data/report">opsporen</a> en <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">oplossen</a> van <a href="$(HOME)/security/">beveiligingsproblemen</a> in Debian.</li>
  <li>U kunt ook helpen bij het minder kwetsbaar maken van <a href="https://wiki.debian.org/Hardening">pakketten</a>, <a href="https://wiki.debian.org/Hardening/RepoAndImages">pakketbronnen en images</a>, en <a href="https://wiki.debian.org/Hardening/Goals">andere componenten</a>.</li>
  <li>Bent u geïnteresseerd in het <a href="$(HOME)/ports/">geschikt maken</a> van Debian voor een bepaalde architectuur waarmee u vertrouwd bent? Dan kunt u een nieuwe versie (&quot;port&quot;) opstarten of bijdragen aan een bestaande. </li>
  <li>Ons helpen met het verbeteren van met Debian verband houdende <a href="https://wiki.debian.org/Services">diensten</a> of er nieuwe creëren en onderhouden die <a href="https://wiki.debian.org/Services#wishlist">gesuggereerd of aangevraagd</a> werden door de gemeenschap.</li>
</ul>

<h2><a id="testing">Testen en bugs oplossen</a></h2>

<aside class="light">
  <span class="fa fa-bug fa-5x"></span>
</aside>

<p>Net als elk ander softwareproject heeft Debian gebruikers nodig die het
besturingssysteem en zijn toepassingen testen. Een manier om bij te dragen is
door de laatste versie te installeren en aan de ontwikkelaars te rapporteren
als iets niet werkt zoals het zou moeten. We hebben ook mensen nodig om onze
installatiemedia, secure boot en het U-Boot opstartprogramma te testen op
verschillende hardware.
</p>

<ul>
  <li>U kunt ons <a href="https://bugs.debian.org/">bugvolgsysteem</a> gebruiken om problemen te melden die u vindt in Debian. Controleer voordat u dit doet of de bug niet reeds is gemeld.</li>
  <li>Ga naar het bugvolgsysteem en probeer door de bugs te bladeren die verband houden met pakketten die u gebruikt. Kijk of u meer informatie kunt verstrekken en de beschreven problemen kunt reproduceren.</li>
  <li>Test het Debian <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">installatieprogramma en de live ISO-images</a>, de <a href="https://wiki.debian.org/SecureBoot/Testing">ondersteuning voor secure boot</a>, de <a href="https://wiki.debian.org/LTS/TestSuites">LTS-updates</a> en het <a href="https://wiki.debian.org/U-boot/Status">U-Boot</a> opstartprogramma.</li>
</ul>

<h2><a id="documenting">Documentatie schrijven</a></h2>

<aside class="light">
  <span class="fa fa-file-alt fa-5x"></span>
</aside>

<p>Als u problemen ondervindt in Debian en geen code kunt schrijven om het
probleem op te lossen, is het misschien een optie voor u om aantekeningen te
maken en uw manier om de zaak op te lossen op te schrijven. Op die manier kunt
u andere gebruikers helpen die soortgelijke problemen hebben. Alle
Debian-documentatie is geschreven door leden van de gemeenschap en er zijn
verschillende manieren waarop u kunt helpen.</p>

<ul>
  <li>Meewerken met het <a href="$(HOME)/doc/ddp">Debian documentatieproject</a> en helpen met de officiële documentatie van Debian.</li>
  <li>Bijdragen aan de <a href="https://wiki.debian.org/">Debian Wiki</a></li>
</ul>


<h2><a id="translating">Vertalen en lokaliseren</a></h2>

<aside class="light">
  <span class="fa fa-language fa-5x"></span>
</aside>

<p>
Is uw moedertaal niet het Engels, maar heeft u voldoende taalvaardigheid in het
Engels om software of Debian-gerelateerde informatie zoals webpagina's,
documentatie, enz. te begrijpen en te vertalen? Waarom zou u zich niet
aansluiten bij een vertaalteam en Debian-applicaties omzetten naar uw
moedertaal? We zijn ook op zoek naar mensen die bestaande vertalingen kunnen
controleren en indien nodig bugrapporten kunnen indienen.
</p>

# Translators, link directly to your group's pages
<ul>
  <li>Alles wat te maken heeft met de internationalisering van Debian wordt besproken op de <a href="https://lists.debian.org/debian-i18n/">i18n-mailinglijst</a> en voor wat de vertaling naar het Nederlands betreft, op de mailinglijst<a href="https://lists.debian.org/debian-l10n-dutch/">debian-l10n-dutch</a>.</li>
  <li>Is uw moedertaal een taal die nog niet ondersteund wordt in Debian? Neem dan contact op via de pagina <a href="$(HOME)/international/">Debian Internationaal</a>.</li>
</ul>

<h2><a id="usersupport">Andere gebruikers helpen</a></h2>

<aside class="light">
  <span class="fas fa-handshake fa-5x"></span>
</aside>

<p>U kunt ook bijdragen aan het project door andere Debian-gebruikers te
helpen. Het project maakt gebruik van verschillende ondersteuningskanalen,
zoals bijvoorbeeld mailinglijsten in verschillende talen en IRC-kanalen. Voor
meer informatie kunt u terecht op onze pagina's in verband met
<a href="$(HOME)/support">ondersteuning</a>.</p>

# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<ul>
  <li>Het Debian-project maakt gebruik van veel verschillende <a href="https://lists.debian.org/">mailinglijsten</a>; sommige zijn voor ontwikkelaars en sommige zijn voor gebruikers. Ervaren gebruikers kunnen anderen helpen via de <a href="$(HOME)/support#mail_lists">mailinglijsten voor gebruikers</a>, zoals de mailinglijst <a href="https://lists.debian.org/debian-user/">debian-user</a> voor ondersteuning in het Engels en <a href="https://lists.debian.org/debian-user-dutch/">debian-user-dutch</a> voor gebruikersondersteuning in het Nederlands.</li>
  <li>Mensen van over de hele wereld chatten in realtime op IRC (Internet Relay Chat). Bezoek het kanaal <tt>#debian</tt> op <a href="https://www.oftc.net/">OFTC</a> om te chatten met andere Debian-gebruikers. Verkiest u in het Nederlands te chatten met andere ontwikkelaars en gebruikers van Debian, dan kunt u afstemmen op het kanaal <tt>#debian-nl</tt> op <a href="https://www.oftc.net/">OFTC</a>.</li>
</ul>

<h2><a id="events">Evenementen organiseren</a></h2>

<aside class="light">
  <span class="fas fa-calendar-check fa-5x"></span>
</aside>

<p>Naast de jaarlijkse Debian-conferentie (DebConf) zijn er elk jaar een aantal
kleinere bijeenkomsten en vergaderingen in verschillende landen. Deelnemen aan
een <a href="$(HOME)/events/">evenement</a> of er een helpen organiseren is een
prima gelegenheid om andere gebruikers en ontwikkelaars van Debian te
ontmoeten.</p>

<ul>
  <li>Meehelpen tijdens de jaarlijkse <a href="https://debconf.org/">conferentie van Debian</a>, bijvoorbeeld door het opnemen op <a href="https://video.debconf.org/">video</a> van lezingen en presentaties, door het onthaal te verzorgen van de deelnemers, door bijzondere evenementen te helpen organiseren tijdens Debconf (zoals de kaas- en wijnavond), door mee te helpen bij de opbouw en de afbraak, enz.</li>
  <li>Daarnaast zijn er verschillende <a href="https://wiki.debian.org/MiniDebConf">MiniDebConf</a>-evenementen, lokale bijeenkomsten georganiseerd door leden van het Debian-project.</li>
  <li>U kunt ook het initiatief nemen voor of deelnemen aan een <a href="https://wiki.debian.org/LocalGroups">lokale Debian-groep</a> met regelmatige bijeenkomsten of andere activiteiten.</li>
  <li>Kijk ook eens naar andere evenementen zoals <a href="https://wiki.debian.org/DebianDay">feesten op Debian Day</a>, <a href="https://wiki.debian.org/ReleaseParty" >releaseparty's</a>, <a href="https://wiki.debian.org/BSP">bugoplossingsparty's</a>, <a href="https://wiki.debian.org/Sprints">ontwikkelingssprints</a> of <a href="https://wiki.debian.org/DebianEvents">andere evenementen</a> over de hele wereld.</li>
</ul>

<h2><a id="donations">Geld, hardware of bandbreedte doneren</a></h2>

<aside class="light">
  <span class="fas fa-gift fa-5x"></span>
</aside>

<p>Alle donaties aan het Debian-project worden beheerd door onze
Debian-projectleider (DPL). Met uw steun kunnen we hardware, domeinnamen,
cryptografische certificaten, enz. aankopen. We gebruiken ook fondsen om
DebConf- en MiniDebConf-evenementen, ontwikkelingssprints, de aanwezigheid op
andere evenementen en andere dingen te sponsoren.</p>

<ul>
  <li>U kunt geld, uitrusting en diensten <a href="$(HOME)/donations">doneren</a> aan het Debian-project.</li>
  <li>We zijn voortdurend op zoek naar <a href="$(HOME)/mirror/">spiegelservers</a> over de hele wereld.</li>
  <li>Voor onze Debian-versies voor de verschillende architecturen steunen we op ons <a href="$(HOME)/devel/buildd/">autobuilder-netwerk</a>.</li>
</ul>

<h2><a id="usedebian">Debian gebruiken en erover praten</a></h2>

<aside class="light">
  <span class="fas fa-bullhorn fa-5x"></span>
</aside>

<p>Zeg het voort en vertel anderen over Debian en de Debian-gemeenschap. Beveel
het besturingssysteem aan andere gebruikers aan en laat hen zien hoe ze het
kunnen installeren. Gebruik het gewoon en geniet ervan -- dat is waarschijnlijk
de gemakkelijkste manier om iets terug te geven aan het Debian-project.</p>

<ul>
  <li>Help Debian te promoten door een lezing te geven en het aan andere gebruikers te demonstreren.</li>
  <li>Werk mee aan onze <a href="https://www.debian.org/devel/website/">website</a> en help ons om het publieke gezicht van Debian te verbeteren.</li>
  <li>Maak <a href="https://wiki.debian.org/ScreenShots">schermafdrukken</a> en <a href="https://screenshots.debian.net/upload">upload</a> deze naar <a href="https://screenshots.debian.net/">screenshots.debian.net</a> zodat onze gebruikers kunnen zien hoe software in Debian eruitziet voordat ze deze gebruiken.</li>
  <li>U kunt <a href="https://packages.debian.org/popularity-contest">popularity-contest-inzendingen</a> activeren, zodat we weten welke pakketten voor iedereen populair en het meest nuttig zijn.</li>
</ul>

<h2><a id="organizations">Hoe uw organisatie Debian kan ondersteunen</a></h2>

<aside class="light">
  <span class="fa fa-check-circle fa-5x"></span>
</aside>

<p>
Of u nu werkt bij een educatieve, commerciële, non-profit- of
overheidsorganisatie, er zijn tal van manieren om ons te ondersteunen met uw
middelen.
</p>

<ul>
  <li>Uw organisatie zou bijvoorbeeld simpelweg geld of hardware kunnen <a href="$(HOME)/donations">doneren</a>.</li>
  <li>Mogelijk wilt u onze conferenties <a href="https://www.debconf.org/sponsors/">sponsoren</a>.</li>
  <li>Uw organisatie zou producten of diensten kunnen leveren aan <a href="https://wiki.debian.org/MemberBenefits">personen die een bijdrage leveren aan Debian</a>.</li>
  <li>We zijn ook op zoek naar <a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">gratis hosting</a>. </li>
  <li>Natuurlijk wordt ook het opzetten van spiegelservers voor <a href="https://www.debian.org/mirror/ftpmirror">software</a>, de <a
href="https://www.debian.org/CD/mirroring/">installatiemedia</a> of de <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">conferentievideo's</a> sterk gewaardeerd.</li>
  <li>Misschien zou u ook kunnen overwegen om Debian <a href="https://www.debian.org/events/merchandise">koopwaar</a>, <a href="https://www.debian.org/CD/vendors/">installatiemedia</a> of <a href="https://www.debian.org/distrib/pre-installed">vooraf geïnstalleerde systemen</a> te verkopen.</li>
  <li>Indien uw organisatie <a href="https://www.debian.org/consultants/">consult</a> in verband met Debian of <a href="https://wiki.debian.org/DebianHosting">hosting</a> aanbiedt, laat ons dit dan alstublieft weten.</li>
</ul>

<p>
We zijn ook geïnteresseerd in het aangaan van <a href="https://www.debian.org/partners/">partnerships</a>. Indien u Debian kunt promoten via
<a href="https://www.debian.org/users/">een getuigenis</a>, door het te
gebruiken op de servers of desktops van uw organisatie of zelfs door uw
personeel aan te moedigen om mee te werken aan ons project, is dat fantastisch.
Misschien zou u ook kunnen overwegen om les te geven over het
Debian-besturingssysteem en de Debian-gemeenschap, om uw team te instrueren om
tijdens de werkuren bij te dragen aan Debian of ze naar één van onze
<a href="$(HOME)/events/">evenementen</a> te sturen.</p>

