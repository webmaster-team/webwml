#use wml::debian::template title="het domein debian.community"
#use wml::debian::faqs
#use wml::debian::translation-check translation="9bedd5cf88ae9171cefa3c6fa2d8d47dab1c859b"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p style="display: block; border: solid; padding: 1em; background-color: #FFF29F;">
 <i class="fa fa-unlink fa-3x"></i>
Als u deze pagina heeft bereikt via een link naar
<a href="https://debian.community">debian.community</a>, dan bestaat de pagina
die u heeft opgevraagd niet meer. Lees verder om uit te vinden waarom.
</p>

<ul class="toc">
 <li><a href="#background">Achtergrond</a></li>
 <li><a href="#statements">Vroegere verklaringen</a></li>
 <li><a href="#faq">Veelgestelde vragen</a></li>
</ul>

<h2><a id="background">Achtergrond</a></h2>

<p>
In juli 2022 stelde de World Intellectual Property Organization, de
 Wereldorganisatie voor de intellectuele eigendom, vast dat het domein
 <a href="https://debian.community/">debian.community</a> te kwader trouw was
 geregistreerd en werd gebruikt om de handelsmerken van het Debian Project te
 ontluisteren. Zij beval dat het domein aan het Project moest worden
 overgedragen.
</p>

<h2><a id="statements">Vroegere verklaringen</a></h2>

<h3><a id="statement-debian">
 Debian Project: laster door Daniel Pocock
</a></h3>

<p>
 <i>Oorspronkelijk gepubliceerd:
 <a href="$(HOME)/News/2021/20211117">17 november 2021</a></i>
</p>

<p>
Debian is op de hoogte van een aantal openbare berichten over Debian en de
leden van haar gemeenschap op een aantal websites door ene Daniel Pocock, die
beweert een ontwikkelaar van Debian te zijn.
</p>

<p>
De heer Pocock is niet verbonden met Debian. Hij is geen Debian-ontwikkelaar en
 ook geen lid van de Debian-gemeenschap. Hij was vroeger een
 Debian-ontwikkelaar, maar werd enkele jaren geleden uit het project gezet
 wegens gedrag dat schadelijk was voor de reputatie van Debian en voor de
 gemeenschap zelf. Hij is sinds 2018 geen lid meer van het Debian-project. Het
 is hem ook verboden om deel te nemen aan de Debian-gemeenschap in welke vorm
 dan ook, inclusief via technische bijdragen, deelname aan online ruimtes of
 het bijwonen van conferenties en/of evenementen. Hij heeft niet het recht of
 de status om Debian te vertegenwoordigen in welke hoedanigheid dan ook, of om
 zichzelf voor te stellen als een Debian-ontwikkelaar of als lid van de
 Debian-gemeenschap.
</p>

<p>
Sinds hij uit het project is gezet, heeft de heer Pocock als vergelding een
 aanhoudende en uitgebreide intimidatiecampagne gevoerd door een aantal
 opruiende en lasterlijke berichten op het internet te plaatsen, met name op
 een website die zich voordoet als een Debian-website. De inhoud van deze
 berichten heeft niet alleen betrekking op Debian, maar ook op een aantal van
 haar ontwikkelaars en vrijwilligers. In veel van zijn communicatie en publieke
 verklaringen blijft hij zich ten onrechte voordoen als lid van de
 Debian-gemeenschap. Raadpleeg dit artikel voor een lijst van de officiële
 communicatiekanalen van Debian. Juridische stappen worden overwogen voor onder
 meer laster, kwaadwillige leugens en intimidatie.
</p>

<p>
Debian staat eendrachtig als gemeenschap en kant zich tegen intimidatie. We
 hebben een gedragscode die richting geeft aan onze reactie op schadelijk
 gedrag in onze gemeenschap, en we zullen blijven optreden om onze gemeenschap
 en onze vrijwilligers te beschermen. Aarzel niet om contact op te nemen met
 het Debian Community-team als u zich zorgen maakt of ondersteuning nodig
 heeft. Ondertussen worden alle rechten van Debian en van haar vrijwilligers
 voorbehouden.
</p>

<h3><a id="statement-other">Verklaringen van andere projecten</a></h3>

<ul>
 <li>
  <a href="https://fsfe.org/about/legal/minutes/minutes-2019-10-12.en.pdf#page=17">
   Free Software Foundation Europe e.V.</a> (voor het eerst gepubliceerd op 12 oktober 2019)
 </li>
 <li>
  <a href="https://openlabs.cc/en/statement-we-have-been-a-target-of-disinformation-efforts-our-initial-reaction/">
   Open Labs</a> (voor het eerst gepubliceerd op 26 mei 2021)
 </li>
 <li>
  <a href="https://communityblog.fedoraproject.org/statement-on-we-make-fedora/">
   Fedora</a> (voor het eerst gepubliceerd op 31 januari 2022)
 </li>
</ul>

<h2><a id="faq">Veelgestelde vragen</a></h2>

<question>
 Waarom werd het domein overgedragen aan Debian?
</question>

<answer><p>
Debian diende een klacht in bij de WIPO dat het domein werd gebruikt om te
 kwader trouw inbreuk te maken op de handelsmerken van Debian. In juli 2022
 besloot de WIPO-jury dat de vorige houder geen rechten of belangen had
 bij de handelsmerken en deze te kwader trouw gebruikte, en droeg het domein
 over aan het Debian-project.
</p></answer>

<question>
 Welk bezwaar maakte Debian tegen de inhoud?
</question>

<answer><p>
De inhoud van de site <a href="https://debian.community/">debian.community</a>
 bezoedelde de handelsmerken van Debian door ze te associëren met ongefundeerde
 beweringen en links naar sekten, insinuaties van slavernij en misbruik van
 vrijwilligers.
</p></answer>

<question>
 Wie zat er achter de vorige website?
</question>

<answer><p>
De vorige houder van het domein was Free Software Contributors Association, een
 niet-geregistreerde vereniging in Zwitserland. In zijn klacht bij de WIPO
 beweerde Debian dat de vereniging een alter ego was van Daniel Pocock.
</p></answer>

<question>
 Kan ik het volledige WIPO-arrest lezen?
</question>

<answer><p>
 Ja, het is
 <a href="https://www.wipo.int/amc/en/domains/decisions/pdf/2022/d2022-1524.pdf">
 openbaar gearchiveerd</a>.
</p></answer>

<question>
 Vond de jury de artikelen lasterlijk?
</question>

<answer><p>
De jury mocht daarover geen oordeel vellen - dat valt buiten de bevoegdheid van
 de jury. De jury kon alleen vaststellen dat de houder het domein te kwader
 trouw had geregistreerd en gebruikt had om de handelsmerken van Debian te
 bezoedelen.
</p></answer>

<question>
 Zal Debian nog andere actie ondernemen?
</question>

<answer><p>
 Deze informatie kan niet openbaar worden gemaakt. Het Debian-project blijft de situatie opvolgen en overlegt met zijn juridisch adviseur.
</p></answer>

<h2><a id="further-info">Bijkomende informatie<a/></h2>

<ul>
 <li>
  <a href="https://lists.debian.org/debian-news/2024/msg00000.html">
   Verklaring over Daniel Pocock</a>
 <li>
  <a href="$(HOME)/intro/people">
   Het Debian-project: De mensen: Wie we zijn en wat we doen</a>
 </li>
 <li>
  <a href="$(HOME)/intro/philosophy">
   Het Debian-project: Onze filosofie: Waarom we het doen en hoe we het doen</a>
 </li>
</ul>

