#use wml::debian::translation-check translation="0566d9016413a572d83570c0605ce60d3cc9215d"
           <p class="center">
             <a style="margin-left: auto; margin-right: auto;" href="vote_001_results.dot">
               <img src="vote_001_results.png" alt="Grafische weergave van de resultaten">
               </a>
           </p>
             <p>
               In de bovenstaande grafiek duiden alle roze gekleurde knooppunten
               erop dat de optie de meerderheid niet heeft gehaald, het blauwe
               geeft de winnaar aan. Een achthoek wordt gebruikt voor de opties
               die de standaard niet haalden.  
           </p>
           <ul>
<li>Option 1 "Jonathan Carter"</li>
<li>Option 2 "Sruthi Chandran"</li>
<li>Option 3 "Geen van bovenstaande"</li>
           </ul>
            <p>
               In de volgende tabel geeft het vak[rij x][kol y] weer hoeveel
               keer optie x verkozen werd boven optie y tijdens de stemming. Een
               <a href="http://en.wikipedia.org/wiki/Schwartz_method">meer
               gedetailleerde uitleg over de vergelijkingsmatrix</a> kan helpen om
               de tabel te begrijpen. Om inzicht te krijgen in de Condorcet-methode,
               is het <a href="https://nl.wikipedia.org/wiki/Methode_Condorcet">Wikipedia-artikel</a> redelijk leerzaam.
           </p>
           <table class="vote">
             <caption class="center"><strong>De vergelijkingsmatrix</strong></caption>
	     <tr><th>&nbsp;</th><th colspan="3" class="center">Optie</th></tr>
              <tr>
                   <th>&nbsp;</th>
                   <th>    1 </th>
                   <th>    2 </th>
                   <th>    3 </th>
              </tr>
                 <tr>
                   <th>Optie 1  </th>
                   <td>&nbsp;</td>
                   <td>  312 </td>
                   <td>  421 </td>
                 </tr>
                 <tr>
                   <th>Optie 2  </th>
                   <td>  102 </td>
                   <td>&nbsp;</td>
                   <td>  341 </td>
                 </tr>
                 <tr>
                   <th>Optie 3  </th>
                   <td>   30 </td>
                   <td>   81 </td>
                   <td>&nbsp;</td>
                 </tr>
               </table>
              <p>

Bekijken we rij 2, kolom 1, dan blijkt dat Sruthi Chandran<br/>
102 keer verkozen werd boven Jonathan Carter<br/>
<br/>
Bekijken we rij 1, kolom 2, dan blijkt dat Jonathan Carter<br/>
312 keer verkozen werd boven Sruthi Chandran.<br/>
              <h3>Paarsgewijze vergelijking</h3>
              <ul>
                <li>Optie 1 verslaat optie 2 met ( 312 -  102) =  210 stemmen.</li>
                <li>Optie 1 verslaat optie 3 met ( 421 -   30) =  391 stemmen.</li>
                <li>Optie 2 verslaat optie 3 met ( 341 -   81) =  260 stemmen.</li>
              </ul>
              <h3>De Schwartz-verzameling bevat</h3>
              <ul>
                <li>Optie 1 "Jonathan Carter"</li>
              </ul>
              <h3>De winnaars</h3>
              <ul>
                <li>Optie 1 "Jonathan Carter"</li>
              </ul>
              <p>
               Debian gebruikt de Condorcet-methode bij stemmingen.
               Simpel gesteld kan de gewone Condorcet-methode als volgt worden
               uitgedrukt: <br/>
               <q>Bekijk alle mogelijke wedlopen in twee richtingen tussen
               kandidaten. De Condorcet-winnaar, als er een is, is de kandidaat
               die elke andere kandidaat kan verslaan in een wedloop
               met die kandidaat in de twee richtingen.</q>
               Het probleem is dat er bij complexe verkiezingen wel eens sprake kan
               zijn van een circulaire relatie waarin A B verslaat, B C verslaat en
               C A verslaat. De meeste variaties op Condorcet gebruiken
               verschillende manieren om deze onbesliste stand op te lossen. Zie
               <a href="https://en.wikipedia.org/wiki/Cloneproof_Schwartz_Sequential_Dropping">de methode van het kloonbestendig sequentieel uitrangeren in een Schwartz-verzameling</a>
               voor details. De variatie die Debian gebruikt wordt beschreven in de
               <a href="$(HOME)/devel/constitution">statuten</a>,
               meer bepaald in sectie A.6.
              </p>
