#use wml::debian::cdimage title="Debian USB/cd/dvd-images downloaden met BitTorrent" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

#use wml::debian::translation-check translation="67bd189030c9c649cfb9f6581aafc637c6af3b8e"

# Last Translation update by: $Author$
# Last Translation update at: $Date$

<p><a href="https://nl.wikipedia.org/wiki/BitTorrent">BitTorrent</a>
is een peer-to-peer downloadsysteem geoptimaliseerd voor grote aantallen
gebruikers. Het zet een minimale belasting op onze servers omdat
BitTorrent-clients stukken van bestanden uploaden naar anderen terwijl ze
downloaden, dus wordt de belasting gespreid over het netwerk en maakt het
ontzettend snelle downloads mogelijk.
</p>

<div class="tip">
<p>Het <strong>eerste</strong> USB/cd/dvd-image bevat alle bestanden die nodig
zijn voor de installatie van een standaard Debian systeem.<br />
</p>
</div>

<p>
U hebt een BitTorrent-client nodig om via deze methode USB/cd/dvd-images van
Debian te downloaden. Beschikbaar gereedschap in de Debian distributie:
<a href="https://packages.debian.org/aria2">aria2</a>,
<a href="https://packages.debian.org/transmission">transmission</a> of
<a href="https://packages.debian.org/ktorrent">KTorrent</a>.
Andere besturingssystemen (zoals Windows en macOS) worden ondersteund door:
<a href="https://www.qbittorrent.org/download">qBittorrent</a> en
<a href="https://www.bittorrent.com/download">BitTorrent</a>.
</p>


<h3>Officiële torrents voor de <q>stable</q> release</h3>

<div class="line">
<div class="item col50">
<p><strong>cd/USB</strong></p>
  <stable-full-cd-torrent>
</div>
<div class="item col50 lastcol">
<p><strong>dvd/USB</strong></p>
  <stable-full-dvd-torrent>
</div>
<div class="clear"></div>
</div>


<p>Bekijk ook de beschikbare documentatie voor u een installatie begint.
<strong>Als u slechts één document wilt lezen</strong> voor de installatie,
lees dan onze
<a href="$(HOME)/releases/stable/amd64/apa">Installatie Howto</a>, een kort
overzicht van het installatieproces. Andere nuttige documentatie:
</p>

<ul>
<li><a href="$(HOME)/releases/stable/installmanual">Installatiehandleiding</a>,
    bevat gedetaileerde installatie-instructies</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Documentatie over het
    Debian Installatiesysteem</a>, waaronder antwoorden op veel gestelde vragen
   (FAQ)</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Errata bij het
    Debian Installeratiesysteem</a>, een overzicht van bekende problemen in het
    installatiesysteem</li>
</ul>

#<h3>Officiële torrents voor de <q>testing</q> distributie</h3>
#
#<ul>
#  <li><strong>cd</strong>:<br>
#  <full-cd-torrent>
#  </li>
#
#  <li><strong>dvd</strong>:<br>
#  <full-dvd-torrent>
#  </li>
#</ul>

<p>
Laat indien mogelijk na afloop van de download uw BitTorrent client actief
om anderen te helpen de images sneller te downloaden!
</p>

