# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Templates webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-08-09 12:57+0200\n"
"Last-Translator: Laura Arjona Reina <larjona@debian.org>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "libre"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "no-libre"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "no redistribuible"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Libre"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "No-libre"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "No redistribuible"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Consulte la página con la <a href=\"./\">información de licencias</a> para "
"tener una visión general de los resúmenes de licencias de Debian (RLD)."

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, Versión %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s &ndash; %s: %s"

#~ msgid "DFSG"
#~ msgstr "DSLD"

#~ msgid "DFSG FAQ"
#~ msgstr "Preguntas frecuentes DSLD"

#~ msgid "DLS Index"
#~ msgstr "Índice RLD"

#~ msgid "Date published"
#~ msgstr "Fecha de publicación"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Archivo de Debian-legal"

#~ msgid "Discussion"
#~ msgstr "Discusión"

#~ msgid "Justification"
#~ msgstr "Justificación"

#~ msgid "License"
#~ msgstr "Licencia"

#~ msgid "License Information"
#~ msgstr "Información sobre licencias"

#~ msgid "License text"
#~ msgstr "Texto de la licencia"

#~ msgid "License text (translated)"
#~ msgstr "Texto de la licencia (traducido)"

#~ msgid "Original Summary"
#~ msgstr "Resumen original"

#~ msgid "Summary"
#~ msgstr "Resumen"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "Se puede encontrar el resumen original de <summary-author/> en los <a "
#~ "href=\"<summary-url/>\">archivos de la lista</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "<summary-author> preparó este resumen."

#~ msgid "Version"
#~ msgstr "Versión"
