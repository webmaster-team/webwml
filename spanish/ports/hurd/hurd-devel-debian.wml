#use wml::debian::template title="Debian GNU/Hurd --- Desarrollo" NOHEADER="yes"
#use wml::debian::translation-check translation="e42a3c19fa8c376678e6147f47b31ba3fc60e369"
#include "$(ENGLISHDIR)/ports/hurd/menu.inc"

<h1>
Debian GNU/Hurd</h1>
<h2>
Desarrollo de la distribución</h2>

<h3>
Empaquetado de software Hurd</h3>
<p>
Los paquetes específicos de Hurd se mantienen en <url "https://salsa.debian.org/hurd-team/">.
</p>

<h3>
Adaptar paquetes de Debian</h3>
<p>
Si quiere ayudar con la arquitectura GNU/Hurd, debería familiarizarse
con el sistema de empaquetado de Debian. Una vez que lo haya hecho, 
leyendo la documentación disponible y visitando el <a
href="$(HOME)/devel/">Rincón de los Desarrolladores</a> debería saber
cómo extraer los fuentes de los paquetes de Debian y compilar un
paquete Debian. He aquí un curso acelerado para los muy perezosos:</p>

<h3>
Obtener el código fuente y construir paquetes</h3>
<p>
Se puede obtener el código fuente simplemente ejecutando <code>apt source
package</code>, que también extraerá los fuentes.
</p>

<p>
Para extraer el contenido de un paquete de fuentes de Debian se necesita
el fichero
<code>package_version.dsc</code> y los ficheros listados en él. El 
directorio de compilación de Debian se construye con la orden
 <code>dpkg-source -x package_version.dsc</code></p>

<p>
La construcción de un paquete se lleva a cabo en el nuevo directorio de construcción de Debian
<code>package-version</code> con la orden 
<code>dpkg-buildpackage -B -rsudo "-mMiNombre &lt;MiCorreo&gt;"</code>.
En lugar de <code>-B</code> se puede usar
<code>-b</code> si también quiere construir las partes del paquete que 
son independientes de la arquitectura (aunque esto normalmente resulta inútil puesto que ya están
disponibles en el archivo y construirlas puede requerir dependencias
adicionales). Puede utilizar
<code>-uc</code> para evitar firmar el paquete con su clave pgp.</p>

<p>
La construcción puede necesitar que se instalen paquetes adicionales.
La manera más sencilla es ejecutar <code>apt build-dep package</code>,
que instalará todos los paquetes necesarios.
</p>

<p>
Puede ser conveniente usar pbuilder. Se puede construir con
<code>sudo pbuilder create --mirror http://deb.debian.org/debian-ports/ --debootstrapopts --keyring=/usr/share/keyrings/debian-ports-archive-keyring.gpg --debootstrapopts --extra-suites=unreleased --extrapackages debian-ports-archive-keyring</code>
y entonces se puede usar <code>pdebuild -- --binary-arch</code> que gestionará la descarga de las dependencias, etc. y colocará el resultado en <code>/var/cache/pbuilder/result</code>
</p>

<h3>
Escoja uno</h3>
<p>
¿En qué paquetes se necesita trabajar? Bien, cualquiera que aún no haya
sido adaptado, y lo necesite. Esto cambia de forma constante, de manera
que es preferible concentrarse primero en paquetes que tengan muchas
dependencias inversas, lo que puede verse en el gráfico de dependencias 
de paquetes <url "https://people.debian.org/~sthibault/graph-radial.pdf">
que se actualiza cada día, o en la lista de más solicitados
<url "https://people.debian.org/~sthibault/graph-total-top.txt"> (ésta es
la de más solicitados a largo plazo, la de más solicitados a corto plazo es
<url "https://people.debian.org/~sthibault/graph-top.txt">).
También suele ser buena idea escoger de la lista de desactualizados
<url "https://people.debian.org/~sthibault/out_of_date2.txt"> y
<url "https://people.debian.org/~sthibault/out_of_date.txt">, ya que esos 
solían funcionar, y ahora están rotos probablemente solo por un par de razones.
Puede simplemente escoger uno de los paquetes que faltan de manera aleatoria, 
o mirar los registros de autoconstrucción en la lista de correo debian-hurd-build-logs, 
o usar la lista wanna-build de 
<url "https://people.debian.org/~sthibault/failed_packages.txt"> .
Algunos problemas de construcción son más fáciles de corregir que otros. Típicamente, "undefined reference to foo", donde foo es algo como pthread_create, dlopen, cos, ... (que obviamente están disponibles en hurd-i386), que solamente muestra que el paso de configuración del paquete olvidó incluir -lpthread, -ldl, -lm, etc. en el Hurd también. Tenga en cuenta sin embargo que las funciones ALSA MIDI no está disponible.
</p>
<p>
También, compruebe si ya se ha realizado trabajo en 
<url "https://alioth.debian.org/tracker/?atid=410472&amp;group_id=30628&amp;func=browse">,
<url "https://alioth.debian.org/tracker/?atid=411594&amp;group_id=30628&amp;func=browse">,
y el BTS (<url "https://bugs.debian.org/cgi-bin/pkgreport.cgi?users=debian-hurd@lists.debian.org;tag=hurd">), y <url "https://wiki.debian.org/Debian_GNU/Hurd">,
y el estado de los paquetes en vivo en buildd.debian.org, p.ej. 
<url "https://buildd.debian.org/util-linux">.
</p>

<h4>
Paquetes que no van a ser adaptados
</h4>
<p>
Algunos de estos paquetes, o partes de ellos, podrían ser adaptables
más adelante, pero, al menos actualmente, se consideran inadaptables.
Normalmente se marcan como NotForUs en la base de datos de buildd.
</p>

<ul>
<li>
<code>base/makedev</code>, porque el Hurd viene con su propia versión de
este guión. El paquete de fuentes de Debian sólo contiene una versión
específica para Linux.</li>
<li>
<code>base/modconf</code> y <code>base/modutils</code>, porque el concepto
de módulo es específico de Linux.</li>
<li>
<code>base/netbase</code>, porque el resto de cosas que hay en él es 
muy específico del núcleo Linux. El Hurd, en su lugar, utiliza
<code>inetutils</code>.</li>
<li>
<code>base/pcmcia-cs</code>, porque este paquete es específico para
Linux.</li>
<li>
<code>base/setserial</code>, porque es específico para el núcleo de Linux.
Sin embargo, con la adaptación de los gestores de dispositivos de 
caracteres al Mach de GNU, quizá podamos utilizarlo.</li>
</ul>

<h3><a name="porting_issues">
 Generalidades de la adaptación
</a></h3>
<p>
Se puede encontrar<a href=https://www.gnu.org/software/hurd/hurd/porting/guidelines.html>Una lista de asuntos comunes</a> en el sitio web del proyecto original.
Los siguientes asuntos comunes son específicos de Debian.</p>
<p>Antes de arreglar algo, compruebe si la adaptación kfreebsd* quizá ya tiene un arreglo,
y simplemente se debe extender a hurd-i386.</p>


<ul>
<li>
<code>foo : Depende: foo-data (= 1.2.3-1) pero no va a instalarse</code>
<p>
La respuesta breve es: ha fallado la compilación del paquete <code>foo</code> en hurd-i386,
lo que debe corregirse. Vea cuál es el fallo de compilación es su página de estado
en buildd.debian.org.
</p>
<p>
Esto ocurre típicamente cuando la compilación del paquete <code>foo</code> falla en la actualidad,
pero solía ir bien antes. Use <code>apt-cache policy foo foo-data</code>
para ver que, por ejemplo, están disponibles la versión <code>1.2.3-1</code> de
<code>foo</code> y una versión más reciente de <code>foo-data</code>:
<code>2.0-1</code>. Esto se debe a que, en debian-ports, los paquetes independientes de la arquitectura
(arch:all) se comparten entre todas las arquitecturas y, por lo tanto, cuando se sube una versión más
reciente del paquete fuente <code>foo</code> (del cual se generan los paquetes
binarios <code>foo</code> y <code>foo-data</code>), se instala el paquete
más reciente arch:all <code>foo-data</code>, aunque el paquete binario hurd-i386
<code>foo</code> más reciente no compile, dando lugar a versiones
incompatibles. La corrección de esto pasa por hacer que el archivo de debian-ports utilize dak en lugar
de mini-dak, lo cual es todavía un trabajo en curso.
</p>

</li>
<li>
<code>han desaparecido del fichero de símbolos algunos símbolos o patrones</code>
<p>
Algunos paquetes mantienen una lista de los símbolos que se espera que aparezcan en
bibliotecas. Sin embargo, esta lista se obtiene habitualmente en un sistema Linux y, por lo tanto,
incluye símbolos que pueden no tener sentido en sistemas no Linux (por ejemplo, relacionados con una
funcionalidad exclusiva de Linux). Ahora bien, se pueden incluir condicionales en el
fichero <code>.symbols</code>, por ejemplo:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
 (arch=linux-any)linuxish_function@Base 1.23
</pre></td></tr></table>

</li>
<li>
<code>Dependencia con libc6 rota</code>

<p>
Algunos paquetes dependen erróneamente de <code>libc6-dev</code>. Esto
es incorrecto porque <code>libc6</code> es específica a algunas arquitecturas de
GNU/Linux. El correspondiente paquete para GNU es <code>libc0.3-dev</code>
pero otros Sistemas Operativos tendrán otras diferentes. Puede localizar el problema 
en el fichero <code>debian/control</code> en el árbol de código fuente. Soluciones típicas
son detectar el SO usando <code>dpkg-architecture</code> e insertando en el código el soname,
o mejor, usar un OR lógico. P.ej.:
<code>libc6-dev | libc6.1-dev | libc0.3-dev | libc0.1-dev | libc-dev</code>.
El paquete <code>libc-dev</code> es un paquete virtual que funciona para cualquier soname
pero tiene que ponerlo sólamente como última opción.</p></li>
<li>
<code>referencia no definida a snd_*, SND_* no declarado</code>
<p>
Algunos paquetes usan ALSA incluso en arquitecturas no-Linux. El paquete oss-libsalsa 
proporciona alguna emulación sobre OSS, pero está limitado a 1.0.5, y no se proporcionan
algunas características, como las operaciones de secuenciador.
</p>
<p>
Si el paquete lo permite, el soporte de alsa debería deshabilitarse en 
las arquitecturas <code>!linux-any</code> (p.ej. a través de una opción
<code>configure</code>), y añadir un cualificador <code>[linux-any]</code> 
al <code>Build-Depends</code> de alsa, y el añadir el opuesto a 
<code>Build-Conflicts</code>, como <code>Build-Conflicts: libasound2-dev [!linux-any]</code>.
</p>
</li>
<li>
 <code>dh_install: no se encuentra (nada que concuerde con) "foo" (se ha intentado en debian/tmp)</code>
<p>
Esto ocurre típicamente cuando el proyecto original no instaló algo porque no
reconoció el sistema operativo. A veces es algo tonto (p.ej. no sabe que
construir una biblioteca compartida en GNU/Hurd es exactamente igual que en GNU/Linux)
y eso necesita una corrección. A veces tiene sentido, de hecho (p.ej. no instalar los
archivos de servicios de systemd). En ese caso, uno puede usar dh-exec: haciendo depender la construcción de <tt>dh-exec</tt>,
hacer <tt>chmod +x</tt> en el archivo <tt>.install</tt>, y encabezar las líneas problemáticas
con p.ej. <tt>[linux-any]</tt> o <tt>[!hurd-any]</tt>.
</p>
</li>
</ul> 

<h3> <a name="debian_installer">
Modificación del instalador de Debian</a></h3>

<p>
Para generar una imagen ISO, lo más sencillo es partir de una imagen preexistente de las incluidas en <a href=hurd-cd>la página de imágenes de CD de Hurd</a>. A continuación, puede montarla y copiarla:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
mount debian-sid-hurd-i386-NETINST-1.iso /mnt
cp -a /mnt /tmp/myimage
umount /mnt
chmod -R +w /tmp/myimage
</pre></td></tr></table>

<p>
Puede montar el disco RAM inicial, y, por ejemplo, reemplazar un traductor por una versión preparada por usted:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
gunzip /tmp/myimage/initrd.gz
mount /tmp/myimage/initrd /mnt
cp ~/hurd/rumpdisk/rumpdisk /mnt/hurd/
umount /mnt
gzip /tmp/myimage/initrd
</pre></td></tr></table>

<p>
Ahora puede generar la nueva imagen ISO con grub-mkrescue:
</p>

<table><tr><td>&nbsp;</td><td class=example><pre>
rm -fr /tmp/myimage/boot/grub/i386-pc
grub-mkrescue -o /tmp/myimage.iso /tmp/myimage
</pre></td></tr></table>
