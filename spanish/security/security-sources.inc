#use wml::debian::translation-check translation="3121c231cfff9a41f6abfc6afdfeb2aa9435046e"

<ul>
<li><a href="https://security-tracker.debian.org/">Rastreador de seguridad de Debian (Debian Security Tracker)</a>
fuente de información primaria para toda la información relacionada con la seguridad, opciones de búsqueda</li>

<li>La <a href="https://security-tracker.debian.org/tracker/data/json">lista JSON</a>
  contiene descripciones de CVE, nombre del paquete, número del fallo Debian, versiones del paquete corregidas, no incluye DSA
</li>

<li>La <a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">lista DSA</a>
  contiene los DSA incluyendo fecha, números CVE relacionados, versiones del paquete corregidas</li>

<li>La <a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">lista DLA</a>
  contiene DLA incluyendo fecha, números CVE relacionados, versiones del paquete corregidas
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
Anuncios DSA</a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
Anuncios DLA</a></li>

<li><a href="oval">Archivos Oval</a></li>

<li>Buscar un DSA (las mayúsculas son importantes)<br>
por ej. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Buscar un DLA ( -1 es importante)<br>
por ej. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Buscar un CVE<br>
por ej. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>
