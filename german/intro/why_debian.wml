#use wml::debian::template title="Gründe für den Einsatz von Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Erik Pfannenstein"
#include "$(ENGLISHDIR)/releases/info"


<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#users">Debian auf dem PC</a></li>
    <li><a href="#devel">Debian in der Entwicklung</a></li>
    <li><a href="#enterprise">Debian in der Firmenumgebung</a></li>
  </ul>
</div>

<p>
Es gibt eine Vielzahl von Gründen, warum Sie sich bei der Betriebssystemauswahl
für Debian entscheiden sollten – egal, ob auf Ihrem persönlichen Computer, auf
Ihrem Entwicklungsrechner oder im Umfeld Ihres Unternehmens. Die meisten
unserer Anwenderinnen und Anwender schätzen die Stabilität und den
reibungslosen Upgrade-Prozess sowohl für Pakete als auch für die ganze
Distribution. Diejenigen, die Software und Hardware entwickeln, nutzen Debian,
weil es sehr viele Architekturen und Geräte unterstützt und eine öffentliche
Fehlerdatenbank sowie viele Entwicklungs-Werkzeuge bietet. Im geschäftlichen
Umfeld tut es sich durch LTS-Versionen und Cloud-Abbilder hervor.
</p>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Für mich ist es die perfekte Balance zwischen Benutzerfreundlichkeit und Stabilität. Ich habe über die Jahre verschiedene Distributionen verwendet, aber Debian ist die einzige, die einfach läuft. <a href="https://www.reddit.com/r/debian/comments/8r6d0o/why_debian/e0osmxx">NorhamsFinest auf Reddit</a></p>
</aside>

<h2><a id="users">Debian auf dem PC</a></h2>

<dl>
  <dt><strong>Debian ist Freie Software.</strong></dt>
  <dd>
    Debian ist aus freier und quelloffener Software zusammengestellt und wird
    jederzeit zu 100% <a href="free">frei</a> sein – frei für jeden und jede zur
    Verwendung, Anpassung und Weiterverbreitung. Dies ist unser
    Hauptversprechen an
    <a href="../users">unsere Anwenderinnen und Anwender</a>. Kostenfrei ist
    es übrigens auch.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist stabil und sicher.</strong></dt>
  <dd>
    Debian ist ein Linux-basiertes Betriebssytem für viele Geräte,
    einschließlich Laptops, Desktops und Server. Wir liefern jedes Paket mit
    einer sinnvollen Vorkonfiguration aus und versorgen es während seiner
    Lebenszeit mit regelmäßigen Sicherheits-Aktualisierungen.
  </dd>
</dl>

<dl>
  <dt><strong>Debian bietet eine umfangreiche Hardware-Unterstützung.</strong></dt>
  <dd>
    Die meiste Hardware wird vom Linux-Kernel unterstützt, was bedeutet, dass
    Debian sie auch unterstützen wird. Sollte es notwendig sein, sind auch
    proprietäre Hardware-Treiber verfügbar.
  </dd>
</dl>

<dl>
  <dt><strong>Debian bietet einen flexiblen Installer.</strong></dt>
  <dd>
    Unsere <a
    href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live-CD</a>
    eignet sich für alle, die Debian vor der Installation unverbindlich
    ausprobieren wollen. Sie enthält den Calamares-Installer, mit dem
    Debian direkt vom Live-System aus aufgesetzt werden kann. Fortgeschrittene
    verwenden den Debian-Installer, der weitere Optionen zur Feinabstimmung
    bietet und auch offen für automatisierte Werkzeuge zur Installation via
    Netzwerk ist.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-Upgrades laufen wie geschmiert.</strong></dt>
  <dd>
	Es ist ein Kinderspiel, unser Betriebssystem aktuell zu halten, egal ob
	Sie es komplett auf eine neue Version anheben oder nur ein einzelnes Paket
	aktualisieren möchten.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist die Basis für viele andere Distributionen.</strong></dt>
  <dd>
	Viele beliebte Linux-Distributionen wie Ubuntu, Knoppix, PureOS und Tails
	basieren auf Debian. Wir liefern die Werkzeuge, mit denen jeder die
	Software-Pakete aus dem Debian-Archiv nach Belieben mit eigenen Paketen
	anreichern kann.
  </dd>
</dl>

<dl>
  <dt><strong>Das Debian-Projekt ist eine Gemeinschaft.</strong></dt>
  <dd>
	Alle, die wollen, können Teil unserer Gemeinschaft sein, auch die
	Nicht-Entwickler und die Nicht-Systemadmins. Debian ist
	<a href="../devel/constitution">demokratisch organisiert</a>, was allen
	Debian-Mitgliedern die gleichen Rechte verschafft und verhindert, dass
	Debian von einer einzelnen Firma kontrolliert wird. Unsere Entwickler und
	Entwicklerinnen stammen aus über 60 verschiedenen Ländern und Debian selbst
	wird in mehr als 80 Sprachen übersetzt.
  </dd>
</dl>

<aside>
<p><span class="far fa-comment-dots fa-3x"></span>Der Grund dafür, dass Debian als Entwicklerbetriebssystem angesehen ist, ist die riesige Anzahl von Paketen und die Software-Unterstützung, die für Entwickler wichtig ist. Für fortgeschrittene Programmierer und Systemadministratoren empfiehlt es sich sehr. <a href="https://fossbytes.com/best-linux-distros-for-programming-developers/">Adarsh Verma auf Fossbytes</a></p>
</aside>

<h2><a id="devel">Debian in der Entwicklung</a></h2>

<dl>
  <dt><strong>Mehrere Hardware-Architekturen</strong></dt>
  <dd>
    Debian unterstützt eine <a href="../ports">lange Reihe</a> von
    CPU-Architekturen, einschließlich amd64, i386, mehrere Versionen von ARM
    und MIPS, POWER7, POWER8, IBM System z und RISC-V. Für Nischenarchitekturen
    ist Debian ebenfalls verfügbar.
  </dd>
</dl>

<dl>
  <dt><strong>IoT- und Embedded-Geräte</strong></dt>
  <dd>
    Debian läuft auf vielen verschiedenen Geräten wie dem Raspberry Pi,
    diversen QNAP-Varianten, Mobilgeräten, Heimroutern und einer Menge
    Einplatinencomputern.
  </dd>
</dl>

<dl>
  <dt><strong>Sehr viele Softwarepakete</strong></dt>
  <dd>
	Debian hat eine riesige Anzahl von <a
    href="$(DISTRIB)/packages">Paketen</a> (derzeit in Stable:
    <packages_in_stable> Pakete), die das <a
    href="https://manpages.debian.org/unstable/dpkg-dev/deb.5.en.html">deb-Format</a>
    verwenden.
  </dd>
</dl>

<dl>
  <dt><strong>Verschiedene Veröffentlichungen</strong></dt>
  <dd>
	Neben unserer stabilen Veröffentlichungen gibt es die Varianten
	Testing und Unstable, auf denen Sie neuere Software-Versionen installieren
	können.
  </dd>
</dl>

<dl>
  <dt><strong>Öffentliche Fehlerdatenbank</strong></dt>
  <dd>
    Unser Debian-<a href="../Bugs">Bug Tracking System</a> (BTS) ist für die
    Öffentlichkeit via Webbrowser zugänglich. Wir verstecken unsere
    Software-Fehler nicht und auch Sie können auf einfache Weise neue Fehler
    melden oder in die Diskussion einsteigen.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-Richtlinie und -Entwicklerwerkzeuge</strong></dt>
  <dd>
    Debian bietet hochqualitative Software an. In unseren 
    <a href="../doc/debian-policy/">Richtlinien</a> ist festgelegt, welche
    technischen Voraussetzungen jedes Paket erfüllen muss, um in die Distribution
    aufgenommen zu werden. Unsere Continuous-Integration-Strategie umfasst
    Autopkgtest (welches Tests an Paketen ausführt), Piuparts (zum Testen von
    Installation, Upgrade und Entfernung) und Lintian (sucht nach
    Inkonsistenzen und Fehlern in Paketen).
  </dd>
</dl> 

<aside>
<p><span class="far fa-comment-dots fa-3x"></span> Stabilität ist ein Synonym für Debian […] Sicherheit ist eines der wichtigsten Debian-Features. <a href="https://www.pontikis.net/blog/five-reasons-to-use-debian-as-a-server">Christos Pontikis auf pontikis.net</a></p>
</aside>

<h2><a id="enterprise">Debian in der Firmenumgebung</a></h2>

<dl>
  <dt><strong>Debian ist zuverlässig.</strong></dt>
  <dd>
	Debian beweist seine Zuverlässigkeit jeden Tag in tausenden von
	Real-Szenarios, die Anwender-Laptops und Super-Teilchenbeschleuniger
	genauso umfassen wie Aktienhandel und die Kraftfahrzeugindustrie. Darüber
	hinaus wird es in der Akademie, in der Wissenschaft und im öffentlichen
	Sektor geschätzt.
  </dd>
</dl>

<dl>
  <dt><strong>Debian hat viel Expertise.</strong></dt>
  <dd>
    Unsere Paketbetreuer kümmern sich nicht nur um die Paketierung für Debian
    und die Integration neuer Versionen von Programmherstellern. Sie kennen
    häufig auch die Anwendungen an sich in- und auswendig und arbeiten
    direkt im Entwicklungsprozess beim Programmhersteller mit.
  </dd>
</dl>

<dl>
  <dt><strong>Debian ist sicher.</strong></dt>
  <dd>
	Debian bietet Sicherheitsunterstützung für seine stabilen (Stable-)
	Veröffentlichungen. Viele andere Distributionen und Sicherheitsforscher
	verlassen sich auf Debians Sicherheits-Fehlerdatenbank.
  </dd>
</dl>

<dl>
  <dt><strong>Langzeitunterstützung</strong></dt>
  <dd>
    Debians kostenfreie <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS)-Version verlängert die Lebensdauer sämtlicher stabiler
    Debian-Veröffentlichungen um mindestens fünf Jahre. Die kommerzielle 
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>-Initiative
    pflegt eine begrenzte Anzahl von Paketen darüber hinaus.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-Abbilder</strong></dt>
  <dd>
	Für alle bedeutenden Cloud-Plattformen sind offizielle Cloud-Abbilder
	verfügbar. Außerdem versorgen wir Sie mit Werkzeugen und Konfiguration,
	aus denen Sie sich Ihre eigenen angepassten Cloud-Abbilder erstellen
	können. Debian läuft aber auch in virtuellen Maschinen auf dem Desktop oder
	in einem Container.
  </dd>
</dl>
