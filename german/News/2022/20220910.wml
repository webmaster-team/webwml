#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 10 aktualisiert: 10.13 veröffentlicht</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.13</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>Das Debian-Projekt freut sich, die dreizehnte (und letzte) Aktualisierung seiner 
Oldstable-Distribution Debian <release> (Codename <q><codename></q>) ankündigen zu 
dürfen. Diese Aktualisierung behebt hauptsächlich Sicherheitslücken der 
Oldstable-Veröffentlichung sowie einige ernste Probleme. Für sie sind bereits 
separate Sicherheitsankündigungen veröffentlicht worden, auf die, wenn möglich, 
verwiesen wird. 
</p>

<p>Nach dieser Zwischenveröffentlichung werden Debians Security- und Release-Teams die 
Arbeit an Aktualisierungen für Debian 10 einstellen. Wer weiterhin 
Sicherheitsunterstützung erhalten möchte, sollte auf Debian 11 umsteigen oder sich 
auf <url "https://wiki.debian.org/LTS"> informieren, welche Untergruppe an 
Architekturen und Paketen vom Long-Term-Support-Team weiterbetreut werden.</p>

<p>Bitte beachten Sie, dass diese Aktualisierung keine neue Version von Debian <release> 
darstellt, sondern nur einige der enthaltenen Pakete auffrischt. Es gibt keinen Grund, 
<q><codename></q>-Medien zu entsorgen, da deren Pakete nach der Installation mit Hilfe 
eines aktuellen Debian-Spiegelservers auf den neuesten Stand gebracht werden können. 
</p>

<p>Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht viele 
Pakete auf den neuesten Stand bringen müssen. Die meisten dieser Aktualisierungen sind 
in dieser Revision enthalten.</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen 
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Oldstable-Aktualisierung fügt den folgenden Paketen einige wichtige Korrekturen hinzu:</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction adminer "Probleme mit offener Weiterleitung und seitenübergreifendem Skripting behoben [CVE-2020-35572 CVE-2021-29625]; elasticsearch: keine Antwort ausgeben, wenn der HTTP-Code nicht 200 ist [CVE-2021-21311]; kompilierte Version und Konfigurationsdateien mitliefern">
<correction apache2 "Probleme mit Dienstblockade [CVE-2022-22719], HTTP-Abfrageschmuggel [CVE-2022-22720], Ganzzahlüberlauf [CVE-2022-22721], Schreibzugriffe außerhalb der Grenzen [CVE-2022-23943], HTTP-Anfrageschmuggel [CVE-2022-26377], Lesezugriff außerhalb der Grenzen [CVE-2022-28614 CVE-2022-28615], Dienstblockade [CVE-2022-29404], Lesezugriff außerhalb der Grenzen [CVE-2022-30556], Möglichkeit zur Umgehung der IP-basierten Authentifizierung [CVE-2022-31813] behoben">
<correction base-files "Aktualisierung für die Zwischenveröffentlichung 10.13">
<correction clamav "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2022-20770 CVE-2022-20771 CVE-2022-20785 CVE-2022-20792 CVE-2022-20796]">
<correction commons-daemon "JWM-Erkennung überarbeitet">
<correction composer "Anfälligkeit für Code-Injektion behoben [CVE-2022-24828]; GitHub-Token-Muster aktualisiert; Authorization-Kopfzeile anstelle des missbilligten access_token-Abfrageparameter benutzen">
<correction debian-installer "Neukompilierung gegen buster-proposed-updates; Linux-ABI auf 4.19.0-21 angehoben">
<correction debian-installer-netboot-images "Neukompilierung gegen buster-proposed-updates; Linux-ABI auf 4.19.0-21 angehoben">
<correction debian-security-support "Sicherheitsstatus verschiedener Pakete aktualisiert">
<correction debootstrap "Sichergestellt, dass Chroots ohne zusammengeführtes usr auch weiterhin für ältere Veröffentlichungen und buildd-Chroots erzeugt werden können">
<correction distro-info-data "Ubuntu 22.04 LTS, Jammy Jellyfish, und Ubuntu 22.10, Kinetic Kudu, hinzugefügt">
<correction dropbear "Mögliches Problem bei Benutzernamen-Auflistung behoben [CVE-2019-12953]">
<correction eboard "Speicherzugriffsfehler bei der Engine-Auswahl behoben">
<correction esorex "Test-Suite-Fehlschläge auf armhf und ppc64el behoben, die aus einer inkorrekten Verwendung von libffi herrührten">
<correction evemu "Kompilierungsfehlschläge mit neueren Kernel-Versionen behoben">
<correction feature-check "Einige Versions-Vergleiche überarbeitet">
<correction flac "Problem mit Schreibzugriff außerhalb der Grenzen behoben [CVE-2021-0561]">
<correction foxtrotgps "Kompilierungsfehlschläge mit neueren imagemagick-Versionen behoben">
<correction freeradius "Leck via Seitenkanal behoben, durch das einer in 2048 Handshakes fehlschlägt [CVE-2019-13456], außerdem Dienstblockade durch Multithread-BN_CTX-Zugriff [CVE-2019-17185] und Absturz durch nicht-thread-sichere Speicherzuweisung gelöst">
<correction freetype "Pufferüberlauf abgestellt [CVE-2022-27404]; Abstürze beseitigt [CVE-2022-27405 CVE-2022-27406]">
<correction fribidi "Probleme mit Pufferüberlauf gelöst [CVE-2022-25308 CVE-2022-25309]; Absturz unterbunden [CVE-2022-25310]">
<correction ftgl "Nicht versuchen, für latex PNG nach EPS zu konvertieren, weil EPS bei unserem imagemagick aus Sicherheitsgründen deaktiviert ist">
<correction gif2apng "Heap-basierte Pufferüberläufe beseitigt [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction gnucash "Kompilierungsfehlschlag mit aktuellem tzdata behoben">
<correction gnutls28 "Test-Suite an Verwendung in Kombination mit OpenSSL 1.1.1e oder aktueller angepasst">
<correction golang-github-docker-go-connections "Tests mit abgelaufenen Zertifikaten überspringen">
<correction golang-github-pkg-term "Kompilierung auf neueren 4.19-Kerneln überarbeitet">
<correction golang-github-russellhaering-goxmldsig "Nullzeiger-Dereferenzierungen beseitigt [CVE-2020-7711]">
<correction grub-efi-amd64-signed "Neue Veröffentlichung der Originalautoren">
<correction grub-efi-arm64-signed "Neue Veröffentlichung der Originalautoren">
<correction grub-efi-ia32-signed "Neue Veröffentlichung der Originalautoren">
<correction grub2 "Neue Veröffentlichung der Originalautoren">
<correction htmldoc "Endlosschleife [CVE-2022-24191], Probleme mit Ganzzahlüberlauf [CVE-2022-27114] und Heap-Speicherüberläufen gelöst [CVE-2022-28085]">
<correction iptables-netflow "Regression bei DKMS-Kompilierungsfehlschlägen behoben, die durch Änderungen von den Linux-Originalautoren am Kernel 4.19.191 verursacht wurde">
<correction isync "Pufferüberlauf-Probleme behoben [CVE-2021-3657]">
<correction kannel "Kompilierungsfehlschlag durch Abschalten der PostScript-Dokumentation abgestellt">
<correction krb5 "SHA256 als Pkinit CMS Digest verwenden">
<correction libapache2-mod-auth-openidc "Validierung des Nach-Abmelde-URL-Parameters beim Abmelden verbessert [CVE-2019-14857]">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libhttp-cookiejar-perl "Kompilierungsfehlschlag durch Anpassung des Ablaufdatums eines Test-Cookies behoben">
<correction libnet-freedb-perl "Vorgabe-Host vom abgeschalteten freedb.freedb.org auf gnudb.gnudb.org geändert">
<correction libnet-ssleay-perl "Test-Fehlschläge mit OpenSSL 1.1.1n behoben">
<correction librose-db-object-perl "Test-Fehlschlag nach dem 06.06.2020 behoben">
<correction libvirt-php "Speicherzugriffsfehler in libvirt_node_get_cpu_stats behoben">
<correction llvm-toolchain-13 "Neues Quellpaket, um die Kompilierung neuerer Versionen von firefox-esr und thunderbird zu unterstützen">
<correction minidlna "HTTP-Anfragen validieren, um vor DNS-Rebinding-Angriffen zu schützen [CVE-2022-26505]">
<correction mokutil "Neue Version der Originalautoren, um SBAT-Verwaltung zu ermöglichen">
<correction mutt "uudecode-Pufferüberlauf behoben [CVE-2022-1328]">
<correction node-ejs "Optionen und neue Objekte säubern [CVE-2022-29078]">
<correction node-end-of-stream "Testprogrammfehler umgangen">
<correction node-minimist "Prototype-Pollution-Problem gelöst [CVE-2021-44906]">
<correction node-node-forge "Probleme mit Signaturverifizierung behoben [CVE-2022-24771 CVE-2022-24772 CVE-2022-24773]">
<correction node-require-from-string "Test in Zusammenhang mit nodejs &gt;= 10.16 überarbeitet">
<correction nvidia-graphics-drivers "Neue Veröffentlichung der Originalautoren">
<correction nvidia-graphics-drivers-legacy-390xx "Neue Veröffentlichung der Originalautoren; Schreibzugriff außerhalb der Grenzen behoben [CVE-2022-28181 CVE-2022-28185]; Sicherheitskorrekturen [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction octavia "Client-Zertifikats-Prüfungen überarbeitet [CVE-2019-17134]; richtig erkennen, ob der Agent auf Debian läuft; Vorlage, die vrrp-Prüfskript erzeugt, korrigiert; zusätzliche Laufzeit-Abhängigkeiten hinzugefügt; zusätzliche Konfiguration direkt im Agent-Paket mitliefern">
<correction orca "Verwendung mit WebKitGTK 2.36 korrigiert">
<correction pacemaker "Beziehungsversionen aktualisiert, um Upgrades von Stretch LTS zu verbessern">
<correction pglogical "Kompilierungsfehlschlag behoben">
<correction php-guzzlehttp-psr7 "Unsaubere Auswertung von Kopfzeilen behoben [CVE-2022-24775]">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren; vom Benutzer gesetztes default_transport nicht übergehen; if-up.d: nicht mit Fehler aussteigen, wenn postfix noch keine Mails versenden kann; doppelte bounce_notice_recipient-Einträge in der postconf-Ausgabe entfernt">
<correction postgresql-common "pg_virtualenv: Temporäre Passwort-Datei schreiben, bevor chown darauf angewendet wird">
<correction postsrsd "Potenzielle Dienstblockade, wenn Postfix bestimmte lange Datenfelder wie mehrere verkettete E-Mail-Adressen versendet, behoben [CVE-2021-35525]">
<correction procmail "Nullzeigerdereferenzierung behoben">
<correction publicsuffix "Enthaltene Daten aktualisiert">
<correction python-keystoneauth1 "Tests aktualisiert, um Kompilierungsfehlschlag abzustellen">
<correction python-scrapy "Anmeldedaten nicht mit allen Anfragen mitschicken [CVE-2021-41125]; beim Weiterleiten keine Cookies domainübergreifend offenlegen [CVE-2022-0577]">
<correction python-udatetime "Ordentlich gegen libm-Bibliothek verlinken">
<correction qtbase-opensource-src "setTabOrder für Compound-Widgets überarbeitet; Expansionslimit für XML-Entitäten eingeführt [CVE-2015-9541]">
<correction ruby-activeldap "Fehlende Abhängigkeit von ruby-builder nachgetragen">
<correction ruby-hiredis "Einige unzuverlässige Tests überspringen, um Kompilierungsfehlschlag zu beheben">
<correction ruby-http-parser.rb "Kompilierungsfehlschlag bei Verwendung von http-parser, der die Korrektur von CVE-2019-15605 enthält, behoben">
<correction ruby-riddle "Verwenden von <q>LOAD DATA LOCAL INFILE</q> erlauben">
<correction sctk "<q>pdftoppm</q> anstelle von <q>convert</q> zum Konvertieren von PDF nach JPEG verwenden, weil Ersteres mit der geänderten Sicherheitspolitik von ImageMagick nicht mehr funktioniert">
<correction twisted "Fehlerhafte Validierung von URI- und HTTP-Methoden [CVE-2019-12387], fehlerhafte Zertifikatsüberprüfung in der XMPP-Unterstützung [CVE-2019-12855], HTTP/2-Dienstblockade [CVE-2019-9511 CVE-2019-9514 CVE-2019-9515], HTTP-Anfrageschmuggel [CVE-2020-10108 CVE-2020-10109 CVE-2022-24801], Informationsoffenlegung beim Verfolgen von domainübergreifenden Weiterleitungen [CVE-2022-21712], Dienstblockade während SSH-Handshake [CVE-2022-21716] behoben">
<correction tzdata "Zeitzonendaten für Iran, Chile und Palästina aktualisiert; Schaltsekunden-Liste aktualisiert">
<correction ublock-origin "Neue stabile Veröffentlichung der Originalautoren">
<correction unrar-nonfree "Verzeichnisüberschreitung abgestellt [CVE-2022-30333]">
<correction wireshark "Code-Fernausführung [CVE-2021-22191], Dienstblockade-Probleme [CVE-2021-4181 CVE-2021-4184 CVE-2021-4185 CVE-2022-0581 CVE-2022-0582 CVE-2022-0583 CVE-2022-0585 CVE-2022-0586] behoben">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>
Diese Revision fügt der Oldstable-Veröffentlichung die folgenden Sicherheitsaktualisierungen 
hinzu. Das Sicherheitsteam hat bereits für jede davon eine Ankündigung veröffentlicht:
</p>


<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2021 4836 openvswitch>
<dsa 2021 4852 openvswitch>
<dsa 2021 4906 chromium>
<dsa 2021 4911 chromium>
<dsa 2021 4917 chromium>
<dsa 2021 4981 firefox-esr>
<dsa 2022 5034 thunderbird>
<dsa 2022 5044 firefox-esr>
<dsa 2022 5045 thunderbird>
<dsa 2022 5069 firefox-esr>
<dsa 2022 5074 thunderbird>
<dsa 2022 5077 librecad>
<dsa 2022 5080 snapd>
<dsa 2022 5086 thunderbird>
<dsa 2022 5090 firefox-esr>
<dsa 2022 5094 thunderbird>
<dsa 2022 5097 firefox-esr>
<dsa 2022 5106 thunderbird>
<dsa 2022 5108 tiff>
<dsa 2022 5109 faad2>
<dsa 2022 5111 zlib>
<dsa 2022 5113 firefox-esr>
<dsa 2022 5115 webkit2gtk>
<dsa 2022 5118 thunderbird>
<dsa 2022 5119 subversion>
<dsa 2022 5122 gzip>
<dsa 2022 5123 xz-utils>
<dsa 2022 5126 ffmpeg>
<dsa 2022 5129 firefox-esr>
<dsa 2022 5131 openjdk-11>
<dsa 2022 5132 ecdsautils>
<dsa 2022 5135 postgresql-11>
<dsa 2022 5137 needrestart>
<dsa 2022 5138 waitress>
<dsa 2022 5139 openssl>
<dsa 2022 5140 openldap>
<dsa 2022 5141 thunderbird>
<dsa 2022 5142 libxml2>
<dsa 2022 5143 firefox-esr>
<dsa 2022 5144 condor>
<dsa 2022 5145 lrzip>
<dsa 2022 5147 dpkg>
<dsa 2022 5149 cups>
<dsa 2022 5150 rsyslog>
<dsa 2022 5151 smarty3>
<dsa 2022 5152 spip>
<dsa 2022 5153 trafficserver>
<dsa 2022 5154 webkit2gtk>
<dsa 2022 5156 firefox-esr>
<dsa 2022 5157 cifs-utils>
<dsa 2022 5158 thunderbird>
<dsa 2022 5159 python-bottle>
<dsa 2022 5160 ntfs-3g>
<dsa 2022 5164 exo>
<dsa 2022 5165 vlc>
<dsa 2022 5167 firejail>
<dsa 2022 5169 openssl>
<dsa 2022 5171 squid>
<dsa 2022 5172 firefox-esr>
<dsa 2022 5173 linux-latest>
<dsa 2022 5173 linux-signed-amd64>
<dsa 2022 5173 linux-signed-arm64>
<dsa 2022 5173 linux-signed-i386>
<dsa 2022 5173 linux>
<dsa 2022 5174 gnupg2>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5185 mat2>
<dsa 2022 5186 djangorestframework>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
</table>


<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb 
unserer Kontrolle liegen::</p>


<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction elog "Unbetreut; Sicherheitsprobleme">
<correction libnet-amazon-perl "Basiert auf nicht mehr vorhandenem API">
</table>

<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Änderungen enthält, die mit 
dieser Zwischenveröffentlichung in Oldstable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Oldstable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Vorgeschlagene Änderungen für die Oldstable-Distribution:</p>


<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Informationen zur Oldstable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Sicherheitsaktualisierungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier 
Software, die ihre Zeit und Mühen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a>, schicken Sie eine Mail (auf Englisch) an
&lt;press@debian.org&gt; oder kontaktieren Sie das 
Stable-Veröffentlichungs-Team (auch aufEnglisch) unter &lt;debian-release@lists.debian.org&gt;.</p>


